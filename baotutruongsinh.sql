-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 13, 2017 at 04:16 AM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `baotutruongsinh`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_advisory`
--

CREATE TABLE `tb_advisory` (
  `id` int(11) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ord` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_by` int(11) NOT NULL,
  `lang` varchar(2) NOT NULL DEFAULT 'vi',
  `full_name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `question` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_advisory`
--

INSERT INTO `tb_advisory` (`id`, `parent_id`, `name`, `ord`, `status`, `create_time`, `create_by`, `lang`, `full_name`, `phone`, `question`) VALUES
(1, 0, 'Ho ten', 0, 1, '2017-07-11 20:37:25', 0, 'vi', 'Ho ten', '01495864', NULL),
(2, 0, 'hovaten', 0, 1, '2017-07-11 20:39:30', 0, 'vi', 'hovaten', '0156495654', NULL),
(3, 0, 'ten của bạn', 0, 1, '2017-07-11 20:41:32', 0, 'vi', 'ten của bạn', '01685948594', 'tình bệnh nhân'),
(4, 0, 'ho và tê', 0, 1, '2017-07-11 20:48:22', 0, 'vi', 'ho và tê', '01672019148', 'tình trạng bệnh nhân'),
(5, 0, 'gjhjgh', 0, 1, '2017-07-11 20:49:23', 0, 'vi', 'gjhjgh', '5646556654', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_categories`
--

CREATE TABLE `tb_categories` (
  `id` int(11) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ord` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_by` int(11) NOT NULL,
  `lang` varchar(2) NOT NULL DEFAULT 'vi',
  `alias` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `home` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_categories`
--

INSERT INTO `tb_categories` (`id`, `parent_id`, `name`, `ord`, `status`, `create_time`, `create_by`, `lang`, `alias`, `image`, `home`) VALUES
(1, 0, 'Sản phẩm', 1, 1, '2017-07-11 17:04:35', 1, 'vi', 'san-pham', NULL, 0),
(2, 0, 'Đặc tính nổi trội', 2, 1, '2017-07-11 17:37:59', 1, 'vi', 'dac-tinh-noi-troi', 'images/baotutruongsinh_59650cf4a7b15_11_07_2017_19_37_56.png', 0),
(3, 0, 'Chứng nhận của nhà thuốc', 3, 1, '2017-07-11 17:05:26', 1, 'vi', 'chung-nhan-cua-nha-thuoc', NULL, 0),
(4, 0, 'Cảm nhận của khách hàng', 4, 1, '2017-07-11 17:05:43', 1, 'vi', 'cam-nhan-cua-khach-hang', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_extension`
--

CREATE TABLE `tb_extension` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `content` text,
  `status` tinyint(1) DEFAULT '0',
  `link_left` varchar(255) DEFAULT NULL,
  `img_left` varchar(255) DEFAULT NULL,
  `link_right` varchar(255) DEFAULT NULL,
  `img_right` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_extension`
--

INSERT INTO `tb_extension` (`id`, `name`, `link`, `img`, `content`, `status`, `link_left`, `img_left`, `link_right`, `img_right`) VALUES
(1, 'adv', '', '', NULL, 0, NULL, NULL, NULL, NULL),
(2, 'logo', NULL, 'images/logo-truongxuansen_5964bb0e613b2_11_07_2017_13_48_30.png', NULL, 0, NULL, NULL, NULL, NULL),
(3, 'hotline', NULL, NULL, '042.265.8666', 0, NULL, NULL, NULL, NULL),
(4, 'footer', NULL, NULL, '<h4>NHÀ THUỐC ĐÔNG Y GIA TRUYỀN VIỆT NAM</h4>\r\n\r\n<p>Nhà Thuốc đông y Gia truyền Việt Nam trú trọng phương pháp điều trị đánh vào nguồn gốc của bệnh, rất rẻ tiền mà không cần phẫu thuật.</p>\r\n\r\n<div class="address">\r\n<p><em>Hưng Thái. Ninh Giang. Hải dương</em></p>\r\n\r\n<p><em>hoinachdiengia@gmail.com</em></p>\r\n\r\n<p><em>0971 016 866</em></p>\r\n\r\n<p><em>0918 084 468</em></p>\r\n</div>\r\n', 0, NULL, NULL, NULL, NULL),
(5, 'contacts', NULL, NULL, '<p>Chúc mừng bạn! nhà thuốc đã nhận được thông tin đăng ký tư vấn của bạn. vui lòng để ý điện thoại và nghe đt của bác sỹ nhà thuốc sẽ liên lạc lại trong vòng 24h. nếu muốn tư vấn ngay gọi vào hotline gặp trực tiếp bác sỹ (8h-22h hàng ngày):</p>\r\n\r\n<p>0971 016 866</p>\r\n\r\n<p>0918 084 468</p>\r\n\r\n<p>TRÂN TRỌNG: NHÀ THUỐC ĐÔNG Y GIA TRUYỀN VIỆT NAM.</p>\r\n', 0, NULL, NULL, NULL, NULL),
(6, 'email', NULL, NULL, 'cskh.baotutruongsinh@gmail.com', 0, NULL, NULL, NULL, NULL),
(7, 'social', NULL, NULL, '{"time_show":"3","time_show_after_off":"10"}', 0, NULL, NULL, NULL, NULL),
(8, 'maps', NULL, NULL, 'Bao tử trường sinh', 0, NULL, NULL, NULL, NULL),
(9, 'website', NULL, NULL, 'http://baotutruongsinh.com/', 0, NULL, NULL, NULL, NULL),
(10, 'socials', NULL, NULL, '{"facebook":"https:\\/\\/facebook.com\\/facebook","gplus":"https:\\/\\/google.com","youtube":"https:\\/\\/youtube.com"}', 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_field`
--

CREATE TABLE `tb_field` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `key` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1: Khóa chính 0:NT',
  `object_id` int(11) NOT NULL DEFAULT '0',
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `descriptions` varchar(255) DEFAULT NULL,
  `type` tinyint(2) NOT NULL DEFAULT '0',
  `length` int(11) DEFAULT NULL,
  `level` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Level của field',
  `width` int(100) DEFAULT NULL,
  `height` int(100) DEFAULT NULL,
  `style` varchar(255) DEFAULT NULL,
  `editor` tinyint(1) DEFAULT NULL COMMENT '0: textarea 1: nicEditor 2: Ckeditor',
  `process` tinyint(1) DEFAULT NULL COMMENT '1: Crop  - 2: Resize',
  `alert` varchar(255) DEFAULT '',
  `ord` float NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `create_time` timestamp NULL DEFAULT NULL,
  `create_by` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Field lưu trữ các trường của Object';

--
-- Dumping data for table `tb_field`
--

INSERT INTO `tb_field` (`id`, `parent_id`, `key`, `object_id`, `label`, `name`, `descriptions`, `type`, `length`, `level`, `width`, `height`, `style`, `editor`, `process`, `alert`, `ord`, `status`, `create_time`, `create_by`) VALUES
(1, 0, 1, 1, 'ID', 'id', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -9, 0, '2017-01-14 00:45:15', 1),
(2, 0, 0, 1, 'Parent ID', 'parent_id', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -8, 0, '2017-01-14 00:45:15', 1),
(3, 0, 0, 1, 'Name', 'name', NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, 'Hãy nhập tên', -7, 1, '2017-01-14 00:45:15', 1),
(4, 0, 0, 1, 'Thứ tự', 'ord', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 0, 0, '2017-01-14 00:45:15', 1),
(5, 0, 0, 1, 'Trạng thái', 'status', NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -10, 1, '2017-01-14 00:45:15', 1),
(6, 0, 0, 1, 'Ngày tạo', 'create_time', NULL, 5, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -7, 0, '2017-01-14 00:45:15', 1),
(7, 0, 0, 1, 'Người tạo', 'create_by', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -6, 0, '2017-01-14 00:45:15', 1),
(8, 0, 0, 1, 'Ngôn ngữ', 'lang', NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -5, 0, '2017-01-14 00:45:15', 1),
(9, 0, 0, 1, 'Meta Title', 'meta_title', NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -4, 1, '2017-01-14 00:45:15', 1),
(10, 0, 0, 1, 'Meta Description', 'meta_description', NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -3, 1, '2017-01-14 00:45:15', 1),
(11, 0, 0, 1, 'Meta Keyword', 'meta_keyword', NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -2, 1, '2017-01-14 00:45:15', 1),
(116, 0, 0, 8, 'Mã sản phẩm(mã bài viết)', 'product_id', NULL, 1, NULL, 0, NULL, 300, NULL, NULL, NULL, '', 5, 0, '2017-05-16 08:56:50', 1),
(115, 0, 0, 8, 'Nội dung', 'content', NULL, 4, NULL, 0, NULL, 300, NULL, 2, NULL, '', 4, 1, '2017-05-16 08:56:35', 1),
(114, 0, 0, 8, 'Số điện thoại', 'phone', NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 3, 1, '2017-05-16 08:56:17', 1),
(113, 0, 0, 8, 'Địa chỉ', 'address', NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 2, 1, '2017-05-16 08:56:02', 1),
(22, 0, 0, 1, 'Favicon', 'favicon', NULL, 14, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 3, 1, '2017-01-14 00:47:27', 1),
(37, 0, 0, 1, 'Mã Web Master Tools', 'code_wmt', NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 1, 1, '2017-02-08 07:09:46', 1),
(38, 0, 0, 1, 'Mã Analytics', 'code_analytics', NULL, 4, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 2, 1, '2017-02-08 07:10:55', 1),
(39, 0, 1, 4, 'ID', 'id', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -9, 0, '2017-02-08 07:12:00', 1),
(40, 0, 0, 4, 'Parent ID', 'parent_id', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -8, 0, '2017-02-08 07:12:00', 1),
(41, 0, 0, 4, 'Name', 'name', NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, 'Hãy nhập tên', -7, 1, '2017-02-08 07:12:00', 1),
(42, 0, 0, 4, 'Thứ tự', 'ord', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 0, 0, '2017-02-08 07:12:00', 1),
(43, 0, 0, 4, 'Trạng thái', 'status', NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -10, 1, '2017-02-08 07:12:00', 1),
(44, 0, 0, 4, 'Ngày tạo', 'create_time', NULL, 5, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -7, 0, '2017-02-08 07:12:00', 1),
(45, 0, 0, 4, 'Người tạo', 'create_by', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -6, 0, '2017-02-08 07:12:00', 1),
(46, 0, 0, 4, 'Ngôn ngữ', 'lang', NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -5, 0, '2017-02-08 07:12:00', 1),
(47, 0, 0, 4, 'Meta Title', 'meta_title', NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -4, 0, '2017-02-08 07:12:00', 1),
(48, 0, 0, 4, 'Meta Description', 'meta_description', NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -3, 0, '2017-02-08 07:12:00', 1),
(49, 0, 0, 4, 'Meta Keyword', 'meta_keyword', NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -2, 0, '2017-02-08 07:12:00', 1),
(50, 0, 0, 4, 'Hình ảnh', 'image', NULL, 14, NULL, 0, NULL, 300, NULL, NULL, NULL, '', 1, 1, '2017-02-08 07:15:00', 1),
(51, 0, 1, 5, 'ID', 'id', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -9, 0, '2017-04-01 14:36:35', 1),
(52, 0, 0, 5, 'Parent ID', 'parent_id', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -8, 0, '2017-04-01 14:36:35', 1),
(53, 0, 0, 5, 'Name', 'name', NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, 'Hãy nhập tên', -7, 1, '2017-04-01 14:36:35', 1),
(54, 0, 0, 5, 'Thứ tự', 'ord', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 0, 0, '2017-04-01 14:36:35', 1),
(55, 0, 0, 5, 'Trạng thái', 'status', NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -10, 1, '2017-04-01 14:36:35', 1),
(56, 0, 0, 5, 'Ngày tạo', 'create_time', NULL, 5, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -7, 0, '2017-04-01 14:36:35', 1),
(57, 0, 0, 5, 'Người tạo', 'create_by', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -6, 0, '2017-04-01 14:36:35', 1),
(58, 0, 0, 5, 'Ngôn ngữ', 'lang', NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -5, 0, '2017-04-01 14:36:35', 1),
(59, 0, 0, 5, 'Đường Dẫn', 'alias', NULL, 12, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 1, 1, '2017-04-01 14:37:27', 1),
(60, 0, 0, 5, 'Hình Ảnh', 'image', NULL, 14, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 2, 1, '2017-04-01 14:46:13', 1),
(61, 0, 1, 6, 'ID', 'id', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -9, 0, '2017-04-01 14:51:46', 1),
(62, 0, 0, 6, 'Parent ID', 'parent_id', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -8, 0, '2017-04-01 14:51:46', 1),
(63, 0, 0, 6, 'Name', 'name', NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, 'Hãy nhập tên', -7, 1, '2017-04-01 14:51:46', 1),
(64, 0, 0, 6, 'Thứ tự', 'ord', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 0, 0, '2017-04-01 14:51:46', 1),
(65, 0, 0, 6, 'Trạng thái', 'status', NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -10, 1, '2017-04-01 14:51:46', 1),
(66, 0, 0, 6, 'Ngày tạo', 'create_time', NULL, 5, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -7, 0, '2017-04-01 14:51:46', 1),
(67, 0, 0, 6, 'Người tạo', 'create_by', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -6, 0, '2017-04-01 14:51:46', 1),
(68, 0, 0, 6, 'Ngôn ngữ', 'lang', NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -5, 0, '2017-04-01 14:51:46', 1),
(69, 0, 0, 6, 'Meta Title', 'meta_title', NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -4, 0, '2017-04-01 14:51:46', 1),
(70, 0, 0, 6, 'Meta Description', 'meta_description', NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -3, 0, '2017-04-01 14:51:46', 1),
(71, 0, 0, 6, 'Meta Keyword', 'meta_keyword', NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -2, 0, '2017-04-01 14:51:46', 1),
(72, 0, 0, 6, 'Đường Dẫn', 'alias', NULL, 12, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -6, 1, '2017-04-01 14:52:41', 1),
(73, 0, 0, 6, 'Mô tả', 'description', NULL, 3, NULL, 0, NULL, 150, NULL, NULL, NULL, '', 1, 1, '2017-04-01 14:54:50', 1),
(74, 0, 0, 6, 'Nội dung', 'content', NULL, 4, NULL, 0, NULL, 300, NULL, 2, NULL, '', 2, 1, '2017-04-01 14:55:17', 1),
(75, 0, 0, 6, 'Hình ảnh', 'image', NULL, 14, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -5, 1, '2017-04-01 14:55:40', 1),
(76, 0, 1, 7, 'ID', 'id', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -9, 0, '2017-04-01 15:04:16', 1),
(77, 0, 0, 7, 'Parent ID', 'parent_id', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -8, 0, '2017-04-01 15:04:16', 1),
(78, 0, 0, 7, 'Name', 'name', NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, 'Hãy nhập tên', -7, 1, '2017-04-01 15:04:16', 1),
(79, 0, 0, 7, 'Thứ tự', 'ord', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 0, 0, '2017-04-01 15:04:16', 1),
(80, 0, 0, 7, 'Trạng thái', 'status', NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -10, 1, '2017-04-01 15:04:16', 1),
(81, 0, 0, 7, 'Ngày tạo', 'create_time', NULL, 5, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -7, 0, '2017-04-01 15:04:16', 1),
(82, 0, 0, 7, 'Người tạo', 'create_by', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -6, 0, '2017-04-01 15:04:16', 1),
(83, 0, 0, 7, 'Ngôn ngữ', 'lang', NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -5, 0, '2017-04-01 15:04:16', 1),
(84, 0, 0, 7, 'Họ tên', 'full_name', NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 1, 1, '2017-04-01 15:04:52', 1),
(85, 0, 0, 7, 'Số điện thoại', 'phone', NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 2, 1, '2017-04-01 15:05:06', 1),
(86, 0, 1, 8, 'ID', 'id', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -9, 0, '2017-04-01 15:06:48', 1),
(87, 0, 0, 8, 'Parent ID', 'parent_id', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -8, 0, '2017-04-01 15:06:48', 1),
(88, 0, 0, 8, 'Name', 'name', NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, 'Hãy nhập tên', -7, 1, '2017-04-01 15:06:48', 1),
(89, 0, 0, 8, 'Thứ tự', 'ord', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 0, 0, '2017-04-01 15:06:48', 1),
(90, 0, 0, 8, 'Trạng thái', 'status', NULL, 2, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -10, 1, '2017-04-01 15:06:48', 1),
(91, 0, 0, 8, 'Ngày tạo', 'create_time', NULL, 5, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -7, 0, '2017-04-01 15:06:48', 1),
(92, 0, 0, 8, 'Người tạo', 'create_by', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -6, 0, '2017-04-01 15:06:48', 1),
(93, 0, 0, 8, 'Ngôn ngữ', 'lang', NULL, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', -5, 0, '2017-04-01 15:06:48', 1),
(111, 0, 0, 7, 'Tình trạng bệnh', 'question', NULL, 4, NULL, 0, NULL, 350, NULL, 2, NULL, '', 3, 1, '2017-05-16 08:53:12', 1),
(99, 0, 0, 5, 'Hiển thị ở đầu trang chủ', 'home', NULL, 9, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 3, 0, '2017-04-02 07:16:23', 1),
(100, 5, 0, 6, 'Danh mục', 'category', NULL, 8, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 3, 1, '2017-04-02 07:52:54', 1),
(101, 0, 0, 6, 'Giá (Bài viết sản phẩm)', 'price', NULL, 25, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 4, 1, '2017-04-02 08:46:30', 1),
(117, 0, 0, 8, 'Số lượng', 'qty', NULL, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 6, 1, '2017-07-12 03:31:16', 1),
(118, 0, 0, 8, 'Tổng tiền', 'total_money', NULL, 25, NULL, 0, NULL, NULL, NULL, NULL, NULL, '', 7, 1, '2017-07-12 03:32:06', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_file`
--

CREATE TABLE `tb_file` (
  `id` int(11) UNSIGNED NOT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_path` varchar(255) DEFAULT NULL,
  `file_size` float DEFAULT NULL,
  `mime_type` varchar(255) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `create_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Lưu trữ các file/image';

-- --------------------------------------------------------

--
-- Table structure for table `tb_file_mapper`
--

CREATE TABLE `tb_file_mapper` (
  `id` int(11) UNSIGNED NOT NULL,
  `file_id` int(11) NOT NULL DEFAULT '0' COMMENT 'ID files',
  `file_description` varchar(255) DEFAULT NULL COMMENT 'Description File or Image',
  `record_id` int(11) DEFAULT NULL COMMENT 'ID record',
  `field_id` int(11) NOT NULL DEFAULT '0' COMMENT 'ID field',
  `object_id` int(11) NOT NULL DEFAULT '0' COMMENT 'ID Object',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `create_time` timestamp NULL DEFAULT NULL,
  `create_by` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_lang`
--

CREATE TABLE `tb_lang` (
  `id` int(11) NOT NULL,
  `name_vi` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_en` varchar(255) DEFAULT NULL,
  `name_cn` varchar(255) DEFAULT NULL,
  `name_fr` varchar(255) DEFAULT NULL,
  `name_rs` varchar(255) DEFAULT NULL,
  `name_jp` varchar(255) DEFAULT NULL,
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `note` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ord` int(2) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `create_time` timestamp NULL DEFAULT NULL,
  `create_by` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_menus`
--

CREATE TABLE `tb_menus` (
  `id` int(10) NOT NULL,
  `type_id` int(10) DEFAULT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `danhmuc_id` int(10) DEFAULT NULL,
  `depth` int(10) DEFAULT NULL,
  `left` int(10) DEFAULT NULL,
  `right` int(10) DEFAULT NULL,
  `parent_id` int(10) DEFAULT NULL,
  `baiviet_id` int(10) DEFAULT NULL,
  `ord` int(10) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_menus`
--

INSERT INTO `tb_menus` (`id`, `type_id`, `name`, `url`, `danhmuc_id`, `depth`, `left`, `right`, `parent_id`, `baiviet_id`, `ord`, `date_created`, `date_updated`) VALUES
(1, 1, 'Trang chủ', '', 0, 0, 2, 3, 0, 0, 1, '2017-05-15 19:12:44', '2017-05-15 19:12:44'),
(2, 1, 'Giới thiệu', 'gioi-thieu', 0, 0, 4, 5, 0, 2, 2, '2017-05-15 19:54:08', '2017-05-15 19:54:08'),
(3, 1, 'Cẩm nang sức khỏe', 'cam-nang-suc-khoe', 1, 0, 6, 7, 0, 0, 3, '2017-05-15 19:55:05', '2017-05-15 19:55:05'),
(4, 1, 'Liên hệ', 'lien-he', 0, 0, 10, 11, 0, 0, 5, '2017-05-15 19:57:12', '2017-05-15 19:57:12'),
(5, 1, 'Đặc trị hôi lách', 'dac-tri-hoi-lach', 0, 0, 8, 9, 0, 3, 4, '2017-05-16 03:38:02', '2017-05-16 03:38:02');

-- --------------------------------------------------------

--
-- Table structure for table `tb_news`
--

CREATE TABLE `tb_news` (
  `id` int(11) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ord` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_by` int(11) NOT NULL,
  `lang` varchar(2) NOT NULL DEFAULT 'vi',
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `content` text,
  `image` varchar(255) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `price` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_news`
--

INSERT INTO `tb_news` (`id`, `parent_id`, `name`, `ord`, `status`, `create_time`, `create_by`, `lang`, `meta_title`, `meta_description`, `meta_keyword`, `alias`, `description`, `content`, `image`, `category`, `price`) VALUES
(2, 0, 'Đặc tính nổi trội 1', 2, 1, '2017-07-11 17:31:28', 1, 'vi', 'Chữa hôi nách hiệu quả bằng đông y gia truyền', 'Chữa hôi nách hiệu quả bằng đông y gia truyền', 'Chữa hôi nách hiệu quả bằng đông y gia truyền', 'dac-tinh-noi-troi-1', 'Thuốc đông y dạng viên nén Nano dễ uống, không phải đun, sắc. Không gây tác dụng phụ, an toàn đối với mọi đối tượng.', '<p>Thuốc đông y dạng viên nén Nano dễ uống, không phải đun, sắc. Không gây tác dụng phụ, an toàn đối với mọi đối tượng.</p>\r\n', 'images/ud5_59650b6a204ce_11_07_2017_19_31_22.png', 2, 0),
(1, 0, 'Bao tử trường sinh', 1, 1, '2017-07-11 17:19:48', 1, 'vi', 'Đông y Gia truyền Việt Nam kính chúc quý khách năm mới sức khỏe, hạnh phúc !', 'Đông y Gia truyền Việt Nam kính chúc quý khách năm mới sức khỏe, hạnh phúc !', 'Đông y Gia truyền Việt Nam kính chúc quý khách năm mới sức khỏe, hạnh phúc !', 'bao-tu-truong-sinh', 'LIỆU PHÁP ĐIỀU TRỊ DỨT ĐIỂM ĐAU DẠ DÀY, ĐẠI TRÀNG, TÁ TRÀNG.', '<p><strong>Bao Tử Trường Sinh</strong>&nbsp;là tâm huyết hơn&nbsp;<strong>10 năm nghiên cứu</strong>&nbsp;dựa trên sự kết hợp hoàn hảo của&nbsp;<strong>Đông Y gia truyền Việt Nam</strong>&nbsp;và nền tảng&nbsp;<strong>CÔNG NGHỆ NANO</strong>&nbsp;hiện đại của thế giới.</p>\r\n\r\n<p style="text-align:justify">Đây là sản phẩm được tinh chế&nbsp;<strong>100% từ thảo dược&nbsp;thiên nhiên quý hiếm</strong>. Được nghiên cứu bởi<strong>&nbsp;PGS-TS Phạm Gia Điền</strong>&nbsp;Viên Hàn Lâm Khoa Học và Công Nghệ Việt Nam.</p>\r\n\r\n<p style="text-align:justify">Sản phẩm được bào chế bằng phương pháp chiết xuất hiện đại nhất thế giới hiện tại là&nbsp;<strong>công nghệ</strong><strong>NANO</strong>&nbsp;để sản phẩm được giữ nguyên&nbsp;<strong>thủy tính</strong>&nbsp;dược liệu từ thiên nhiên.</p>\r\n\r\n<p style="text-align:justify"><strong>Công nghệ Nano</strong>&nbsp;sẽ loại bỏ tất cả các tạp chất và những thành phần có tác dụng không tốt cho sức khỏe và giữ lại những thành phần có tác dụng chữa trị tốt nhất cho dạ dày. Đảm bảo chữa trị dứt điểm, không lo tái phát và không gây tác dụng phụ.</p>\r\n\r\n<h2 style="text-align:justify"><span style="color:rgb(0, 0, 255)"><strong>Thành phần có trong&nbsp;Bao Tử Trường Sinh</strong></span></h2>\r\n\r\n<p><strong><em>Trà Dây diệp lục:&nbsp;</em></strong>Có trong Bao Tử Trường Sinh giúp&nbsp;làm sạch vi khuẩn HP (Helicobarter Pylori), đây là loại xoắn khuẩn, sống trên lớp nhày niêm mạc dạ dày và gây ra&nbsp;<strong>bệnh viêm loét dạ dày</strong>&nbsp;- tá tràng,&nbsp;<strong>viêm đại tràng</strong>. giảm viêm niêm mạc dạ dày, giúp&nbsp;an thần dễ ngủ, giải độc mát gan, tiêu hoá tốt, người uống chè dây thường cảm thấy thèm ăn, ăn ngon miệng hơn.</p>\r\n\r\n<p><strong><em>Dạ Cẩm:&nbsp;</em></strong>Được dùng điều trị các bệnh lở loét miệng lưỡi, loét dạ dày, viêm họng, lở loét ngoài da, chữa vết thương do làm chóng lên da non.</p>\r\n\r\n<p><strong><em>Lá Khôi:&nbsp;</em></strong>Có tác dụng trung hòa, làm giảm tiết acid dịch vị, làm se vết loét, làm lành dạ dày, tá tràng, chữa viêm dạ dày hiệu quả.</p>\r\n\r\n<p><strong><em>Cam thảo:&nbsp;</em></strong>Kích thích sự phòng thủ của cơ thể để ngăn chặn sự hình thành các vết loét, duy trì mức độ axit trong dạ dày.</p>\r\n\r\n<p><strong><em>Bình vôi:&nbsp;</em></strong>Alkaloid được chiết từ củ bình vôi rất hiệu quả trong việc giảm đau, chống co giật, an thần gây ngủ, ít độc do vậy rất tốt cho bệnh nhân bị &nbsp;viêm loét dạ dày - đại tràng.</p>\r\n\r\n<p><strong><em>Tinh nghệ Nano Curcumin:&nbsp;</em></strong>Phát huy hiệu quả gấp 50 -80 lần so với tinh nghệ thông thường và gấp tới hàng nghìn lần bột nghệ khi chưa loại tạp chất.</p>\r\n\r\n<h2 style="text-align:justify"><span style="color:rgb(0, 0, 255)"><strong>Công dụng của Bao Tử Trường Sinh</strong></span></h2>\r\n\r\n<p>Giúp bổ tỳ dưỡng vị</p>\r\n\r\n<p>Ngăn chặn sự hình thành của vết loét, chữa viêm dạ dày- tá tràng</p>\r\n\r\n<p>Giúp bổ tỳ dưỡng vị</p>\r\n\r\n<p>Ngăn chặn sự hình thành của vết loét, chữa viêm dạ dày- tá tràng</p>\r\n\r\n<p>Ngăn chặn trào ngược dạ dày vào thực quản</p>\r\n\r\n<p>Giảm ợ hơi ợ chua kích thích tiêu hóa</p>\r\n\r\n<p>Hỗ trợ điều trị viêm đại tràng cấp và mãn tính</p>\r\n\r\n<p>Đặc trị vi khuẩn HP (Helicobarter Pylori)...</p>\r\n\r\n<h2 style="text-align:justify"><span style="color:rgb(0, 0, 255)"><strong>Đối tượng sử dụng Bao Tử Trường Sinh</strong></span></h2>\r\n\r\n<p>Người bị đau dạ giày,viêm loét dạ dày</p>\r\n\r\n<p>Người bị đau đại tràng.</p>\r\n\r\n<p>Người dễ buồn nôn, trào thực quản, ợ hơi ợ chua.</p>\r\n\r\n<p>Người bị viêm dạ dày, cấp và mãn tính.</p>\r\n\r\n<p>Người bị dương tính với vi khuẩn HP.</p>\r\n\r\n<h2 style="text-align:justify"><span style="color:rgb(0, 0, 255)"><strong>Cách sử dụng Bao Tử Trường Sinh</strong></span></h2>\r\n\r\n<p>Sản phẩm được điều chế với công nghệ Nano dạng viên nén.</p>\r\n\r\n<p>Cách uống: Sáng/Trưa/Chiều mỗi lần 2 viên trước khi ăn 15 phút. Trẻ em dưới 12 tuổi uống bằng ½ liều người lớn.&nbsp;</p>\r\n\r\n<h2><span style="color:rgb(0, 0, 255)"><strong>CAM KẾT CỦA NHÀ THUỐC</strong></span></h2>\r\n\r\n<p><span style="color:rgb(0, 0, 255)">CAM KẾT khỏi DỨT ĐIỂM HOÀN TOÀN và KHÔNG TÁI PHÁT sau 1 liệu trình sử dụng.</span></p>\r\n\r\n<p><span style="color:rgb(0, 0, 255)">THEO DÕI và ĐỒNG HÀNH cùng bệnh nhân trong suốt quá trình điều trị đến khi khỏi triệt để.</span></p>\r\n\r\n<p><span style="color:rgb(0, 0, 255)">100% được bào chế&nbsp;THẢO DƯỢC THIÊN NHIÊN và CÔNG NGHỆ NANO hiện đại bậc nhất.</span></p>\r\n\r\n<p><span style="color:rgb(0, 0, 255)">CAM KẾT HOÀN TIỀN NẾU SỬ DỤNG THUỐC KHÔNG CÓ HIỆU QUẢ</span></p>\r\n', 'images/sanpham-baotutruongsinh_596505d665f69_11_07_2017_19_07_34.png', 1, 450000),
(3, 0, 'Đặc tính nổi trội 2', 3, 1, '2017-07-11 17:32:03', 1, 'vi', 'Đặc trị hôi lách', 'Đặc trị hôi lách', 'Đặc trị hôi lách', 'dac-tri-hoi-lach', 'Liệu trình chữa trị nhanh, hiệu quả tuyệt đối và không bị tái phát.', '<p>Liệu trình chữa trị nhanh, hiệu quả tuyệt đối và không bị tái phát.</p>\r\n', 'images/ud4_59650b918187b_11_07_2017_19_32_01.png', 2, 0),
(4, 0, 'Đặc tính nổi trội 3', 4, 1, '2017-07-11 17:31:04', 1, 'vi', '', '', '', 'dac-tinh-noi-troi-3', 'Được bào chế bằng công nghệ NANO từ các loại thảo dược quý hiếm cho sức khỏe.', '', 'images/ud3_59650b528a2c2_11_07_2017_19_30_58.png', 2, 0),
(5, 0, 'Đặc tính nổi trội 4', 5, 1, '2017-07-11 17:32:49', 1, 'vi', '', '', '', 'dac-tinh-noi-troi-4', 'Nhà thuốc đã được Bộ Y tế cấp chứng nhận lưu hành số: 033/2015/ATTP - XNCB', '<p>Nhà thuốc đã được Bộ Y tế cấp chứng nhận lưu hành số: 033/2015/ATTP - XNCB</p>\r\n', 'images/ud2_59650bbc1154c_11_07_2017_19_32_44.png', 2, 0),
(6, 0, 'Đặc tính nổi trội 5', 6, 1, '2017-07-11 17:33:26', 1, 'vi', '', '', '', 'dac-tinh-noi-troi-5', 'Đã được kiểm nghiệm lâm sàng trên thị trường và nhận được giấy chứng nhận của chi cục an toàn thực phẩm.', '<p>Đã được kiểm nghiệm lâm sàng trên thị trường và nhận được giấy chứng nhận của chi cục an toàn thực phẩm.</p>\r\n', 'images/ud1_59650bde35355_11_07_2017_19_33_18.png', 2, 0),
(7, 0, 'Chứng nhận của nhà thuốc 1', 7, 1, '2017-07-11 17:43:06', 1, 'vi', '', '', '', 'chung-nhan-cua-nha-thuoc-1', 'Chứng nhận của nhà thuốc 1', '<p>Chứng nhận của nhà thuốc 1</p>\r\n', 'images/trophy-1_59650ddf42b87_11_07_2017_19_41_51.jpg', 3, 0),
(8, 0, 'Chứng nhận của nhà thuốc 2', 8, 1, '2017-07-11 17:42:56', 1, 'vi', '', '', '', 'chung-nhan-cua-nha-thuoc-2', 'Chứng nhận của nhà thuốc 2', '<p>Chứng nhận của nhà thuốc 2</p>\r\n', 'images/trophy-2_59650e141aafd_11_07_2017_19_42_44.jpg', 3, 0),
(9, 0, 'Cảm nhận của khách hàng 1', 9, 1, '2017-07-11 17:50:33', 1, 'vi', '', '', '', 'cam-nhan-cua-khach-hang-1', 'Cô: Trần Thị Ngọc (56 tuổi – Hải Phòng)', '<p>Tôi sinh ra trong gia đình mà gần như tất cả những thành viên trong gia đình đều bị đau dạ dày, tuy mức độ khác nhau. Lúc đầu nghĩ là bệnh di truyền nhưng khi được sự tư vấn của các bác sĩ của Bao Tử Trường Sinh&nbsp;tôi mới biết đó bệnh dạ dày không di truyền mà do quá trình ăn uống, sinh hoạt không lành mạnh. Tôi mạnh dạn sử dụng một liệu trình được nhà thuốc tư vấn. Nay tôi gần như không còn cảm thấy bệnh đau dạ dày còn tồn tại trong mình như trước đây. Tôi cũng đã đặt thêm cho gia đình tôi và đang trong quá trình sử dụng Bao Tử Trường Sinh, bệnh tình đang tiến triển rất lạc quan. Xin gửi lời cảm ơn chân thành tới bác sĩ, tôi sẽ tiếp tục giới thiệu sản phẩm này cho bạn bè, người quen..</p>\r\n', 'images/tran-thi-ngoc_59650fc28f0ca_11_07_2017_19_49_54.jpg', 4, 0),
(10, 0, 'Cảm nhận của khách hàng 2', 10, 1, '2017-07-11 17:54:16', 1, 'vi', '', '', '', 'cam-nhan-cua-khach-hang-2', 'Anh: Lê Thanh Hải (40 tuổi – Thái Bình)', '<p>Tôi mắc chứng đau đại tràng kinh niên do quá trình sinh hoạt không điều độ trong suốt thời gian dài. Cũng từng phải nằm viện điều trị bệnh đại tràng, sử dụng nhiều loại thuốc khác nhau trong thời gian dài nhưng bệnh cũng chỉ thuyên giảm một thời gian. Tôi gần như tuyệt vọng và gần như không còn tin vào việc sử dụng thuốc. Nhưng sau đó sử dụng sản phẩm của Bao Tử Trường Sinh tôi thấy tình trạng có tiến triển rất tốt. Bao Tử Trường Sinh thực sự rất dễ uống. Hơn nữa, với sự tư vấn tận tình của bác sĩ trong suốt quá trình sử dụng nên bệnh tình của tôi có sự chuyển biến tốt. Tôi cảm thấy rất hài lòng về tác dụng của Bao Tử Trường Sinh cũng như sự tận tình của các bác sĩ.</p>\r\n', 'images/le-thanh-hai_596510c675233_11_07_2017_19_54_14.jpg', 4, 0),
(11, 0, 'Cảm nhận của khách hàng 3', 11, 1, '2017-07-11 17:52:05', 1, 'vi', '', '', '', 'cam-nhan-cua-khach-hang-3', 'Cô: Nguyễn Thị Hằng (50 tuổi – Tp.Vinh)', '<p>Tôi thực sự bất ngờ với công dụng của&nbsp;Bao Tử Trường Sinh, tôi đã khỏi hoàn toàn chứng bệnh Viêm hang vị dạ dày có nhiễm khuẩn Hp.&nbsp;Xin gửi lời cảm ơn chân thành các Dược sĩ rất tận tâm của nhà thuốc Bao Tử Trường Sinh. Hy vọng nhà thuốc có thể duy trì được sự tân tâm cũng như đưa được sản phẩm rộng rãi hơn nữa tới những người đang mắc bệnh&nbsp;về Dạ Dày.</p>\r\n', 'images/nguyen-thi-hang_59651042085ac_11_07_2017_19_52_02.jpg', 4, 0),
(12, 0, 'Cảm nhận của khách hàng 4', 12, 1, '2017-07-11 17:53:09', 1, 'vi', '', '', '', 'cam-nhan-cua-khach-hang-4', 'Anh: Vũ Xuân Trường (41tuổi – Quảng Ninh)', '<p>Em năm nay 41 tuổi, đang l&agrave; đang l&agrave; c&aacute;n bộ. Em biết m&igrave;nh mắc chứng đau dạ d&agrave;y khi đi kh&aacute;m tại bệnh viện năm 27 tuổi, nhưng l&uacute;c đ&oacute; bệnh được chuẩn đo&aacute;n ở mức độ nhẹ n&ecirc;n em t&acirc;m l&yacute; chủ quan bệnh ng&agrave;y c&agrave;ng nặng th&ecirc;m, những cơn đau đến thường xuy&ecirc;n, ăn uống kh&ocirc;ng c&oacute; cảm gi&aacute;c ngon. Em cũng đ&atilde; sử dụng nhiều loại thuốc nhưng kh&ocirc;ng khỏi. Được biết tới B&agrave;i thuốc gia truyền chữa đau dạ d&agrave;y Bao Tử Trường Sinh n&ecirc;n em ki&ecirc;n tr&igrave; sử dụng theo sự chỉ dẫn từ th&aacute;ng 2/2015 - 4/2015 th&igrave; bệnh gần như khỏi ho&agrave;n to&agrave;n. Em gần như tho&aacute;t được những cơn đau v&agrave; ăn uống, sinh hoạt tốt hơn hẳn.</p>', 'images/vu-xuan-truong_5965106ec768d_11_07_2017_19_52_46.jpg', 4, 0),
(13, 0, 'Cảm nhận của khách hàng 5', 13, 1, '2017-07-11 17:53:55', 1, 'vi', '', '', '', 'cam-nhan-cua-khach-hang-5', 'Ngô Thị Khánh (52 tuổi – Hà Nội)', '<p>Trước đ&acirc;y t&ocirc;i hay bị chứng ợ chua , bụng hay bị chướng, thỉnh thoảng c&oacute; những cơn đau ở v&ugrave;ng thượng vị rất kh&oacute; chịu.T&ocirc;i đ&atilde; sử dụng một số loại thuốc trong đ&oacute; c&oacute; cả những loại thuốc t&acirc;y nhưng chỉ đỡ được thời gian đầu, sau đ&oacute; t&igrave;nh trạng đ&oacute; vẫn tiếp tục t&ocirc;i cảm thấy những cơn đau đến thường xuy&ecirc;n hơn. T&igrave;nh cờ thấy được th&ocirc;ng tin về Bao Tử Trường Sinh&nbsp;tr&ecirc;n mạng x&atilde; hội t&ocirc;i đ&atilde; li&ecirc;n hệ với nh&agrave; thuốc v&agrave; c&oacute; được tư vấn về sản phẩm Bao Tử Trường Sinh, sử dụng theo liệu tr&igrave;nh của b&aacute;c sĩ đưa ra đến nay bệnh t&igrave;nh đ&atilde; thuy&ecirc;n giảm kh&aacute; nhiều. T&ocirc;i ăn uống ngon hơn hẳn, những cơn đau &iacute;t hơn nhiều v&agrave; cũng kh&ocirc;ng dai dẳng như trước.</p>', 'images/ngo-thi-khanh_596510a64e5bc_11_07_2017_19_53_42.jpg', 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_object`
--

CREATE TABLE `tb_object` (
  `id` int(11) NOT NULL,
  `role` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `level` tinyint(1) NOT NULL DEFAULT '0',
  `ord` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `create_time` timestamp NULL DEFAULT NULL,
  `create_by` int(11) DEFAULT NULL,
  `add_seo` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Object lưu trữ các bảng dữ liệu';

--
-- Dumping data for table `tb_object`
--

INSERT INTO `tb_object` (`id`, `role`, `name`, `label`, `level`, `ord`, `status`, `create_time`, `create_by`, `add_seo`) VALUES
(1, '["1","2"]', 'seo', 'Seo chung', 0, 0, 1, '2017-01-14 00:45:14', 1, 1),
(4, '["1","2"]', 'slider', 'Quản lý slider', 0, 0, 1, '2017-02-08 07:11:59', 1, 1),
(5, '["1","2","3"]', 'categories', 'Danh Mục', 1, 0, 1, '2017-04-01 14:36:35', 1, NULL),
(6, '["1","2","3"]', 'news', 'Bài Viết', 0, 0, 1, '2017-04-01 14:51:46', 1, 1),
(7, '["1","2","3"]', 'advisory', 'Quản lý đăng ký tư vấn', 0, 0, 1, '2017-04-01 15:04:16', 1, NULL),
(8, '["1","2","3"]', 'order', 'Quản lý đơn hàng', 0, 0, 1, '2017-04-01 15:06:47', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_object_mapper`
--

CREATE TABLE `tb_object_mapper` (
  `id` int(11) UNSIGNED NOT NULL,
  `record_id` int(11) DEFAULT NULL COMMENT 'ID record',
  `field_id` int(11) DEFAULT NULL,
  `object_id` int(11) NOT NULL DEFAULT '0' COMMENT 'ID Object own',
  `record_map_id` int(11) DEFAULT NULL,
  `object_map_id` int(11) NOT NULL DEFAULT '0' COMMENT 'ID Object mapper',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `create_time` timestamp NULL DEFAULT NULL,
  `create_by` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_object_mapper`
--

INSERT INTO `tb_object_mapper` (`id`, `record_id`, `field_id`, `object_id`, `record_map_id`, `object_map_id`, `status`, `create_time`, `create_by`) VALUES
(1, 9, 100, 6, 5, 5, 1, '2017-04-02 14:27:16', 1),
(2, 9, 100, 6, 6, 5, 1, '2017-04-02 14:27:16', 1),
(3, 10, 100, 6, 5, 5, 1, '2017-04-02 14:27:56', 1),
(4, 10, 100, 6, 6, 5, 1, '2017-04-02 14:27:56', 1),
(5, 1, 100, 6, 1, 5, 1, '2017-04-03 01:44:45', 1),
(6, 2, 100, 6, 1, 5, 1, '2017-04-03 01:45:03', 1),
(7, 3, 100, 6, 1, 5, 1, '2017-04-03 01:45:21', 1),
(8, 4, 100, 6, 3, 5, 1, '2017-04-03 01:45:41', 1),
(9, 5, 100, 6, 3, 5, 1, '2017-04-03 01:45:48', 1),
(10, 6, 100, 6, 4, 5, 1, '2017-04-03 01:45:58', 1),
(11, 7, 100, 6, 4, 5, 1, '2017-04-03 01:46:11', 1),
(12, 8, 100, 6, 4, 5, 1, '2017-04-03 01:46:51', 1),
(13, 11, 100, 6, 4, 5, 1, '2017-04-03 01:48:23', 1),
(14, 12, 100, 6, 5, 5, 1, '2017-04-03 07:34:30', 1),
(15, 12, 100, 6, 7, 5, 1, '2017-04-03 07:34:30', 1),
(16, 13, 100, 6, 4, 5, 1, '2017-04-04 09:15:53', 1),
(17, 14, 100, 6, 4, 5, 1, '2017-04-04 09:16:30', 1),
(18, 15, 100, 6, 4, 5, 1, '2017-04-04 09:17:18', 1),
(19, 16, 100, 6, 4, 5, 1, '2017-04-04 09:18:20', 1),
(20, 17, 100, 6, 4, 5, 1, '2017-04-04 09:19:06', 1),
(21, 18, 100, 6, 4, 5, 1, '2017-04-04 09:19:42', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_order`
--

CREATE TABLE `tb_order` (
  `id` int(11) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ord` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_by` int(11) NOT NULL,
  `lang` varchar(2) NOT NULL DEFAULT 'vi',
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `content` text,
  `product_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `total_money` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_order`
--

INSERT INTO `tb_order` (`id`, `parent_id`, `name`, `ord`, `status`, `create_time`, `create_by`, `lang`, `address`, `phone`, `content`, `product_id`, `qty`, `total_money`) VALUES
(1, 0, 'Giới thiệu', 0, 0, '2017-07-12 01:56:26', 0, 'vi', 'fdsdfs', '01685948594', 'contact@bachdieptan.com', 1, 6, 2700000),
(2, 0, 'Giới thiệu', 0, 0, '2017-07-12 01:57:06', 0, 'vi', 'fdsdfs', '01685948594', 'contact@bachdieptan.com', 1, 7, 3150000),
(3, 0, 'Ho va ten', 0, 0, '2017-07-12 02:01:58', 0, 'vi', 'test dia chi', '01684850384', 'test nd', 1, 9, 4050000);

-- --------------------------------------------------------

--
-- Table structure for table `tb_role`
--

CREATE TABLE `tb_role` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '''''',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` timestamp NULL DEFAULT NULL,
  `create_by` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_role`
--

INSERT INTO `tb_role` (`id`, `name`, `status`, `create_time`, `create_by`) VALUES
(1, 'Quản trị cao cấp', 1, '2016-12-14 16:29:51', 6),
(2, 'Quản trị hệ thống', 1, '2016-12-14 16:30:09', 6),
(3, 'Thành viên', 1, '2016-12-14 16:30:20', 6);

-- --------------------------------------------------------

--
-- Table structure for table `tb_seo`
--

CREATE TABLE `tb_seo` (
  `id` int(11) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ord` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_by` int(11) NOT NULL,
  `lang` varchar(2) NOT NULL DEFAULT 'vi',
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `favicon` varchar(255) DEFAULT NULL,
  `code_wmt` varchar(255) DEFAULT NULL,
  `code_analytics` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_seo`
--

INSERT INTO `tb_seo` (`id`, `parent_id`, `name`, `ord`, `status`, `create_time`, `create_by`, `lang`, `meta_title`, `meta_description`, `meta_keyword`, `favicon`, `code_wmt`, `code_analytics`) VALUES
(1, 0, 'Bao tử trường sinh', 1, 1, '2017-07-11 12:08:37', 1, 'vi', 'Bao tử trường sinh', 'Bao tử trường sinh', 'Bao tử trường sinh', 'images/logo-truongxuansen_5964bfc409931_11_07_2017_14_08_36.png', '', '<script>\r\n  (function(i,s,o,g,r,a,m){i[''GoogleAnalyticsObject'']=r;i[r]=i[r]||function(){\r\n  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\r\n  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\r\n  })(window,document,''script'',''https://www.google-analytics.com/analytics.js'',''ga'');\r\n\r\n  ga(''create'', ''UA-99976846-1'', ''auto'');\r\n  ga(''send'', ''pageview'');\r\n\r\n</script>');

-- --------------------------------------------------------

--
-- Table structure for table `tb_session`
--

CREATE TABLE `tb_session` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(50) NOT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_setting`
--

CREATE TABLE `tb_setting` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `title` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `value` text CHARACTER SET utf8 NOT NULL,
  `ord` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL,
  `create_by` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_setting`
--

INSERT INTO `tb_setting` (`id`, `name`, `title`, `value`, `ord`, `status`, `create_time`, `create_by`) VALUES
(1, 'on', 'Mở hệ thống ', '1', NULL, 0, 0, 0),
(2, 'title', 'Tiêu đề website ', '', NULL, 0, 0, 0),
(3, 'key', 'Meta Keywords ', '', NULL, 0, 0, 0),
(4, 'des', 'Meta Description ', 'Smile Ville', NULL, 0, 0, 0),
(5, 'email', 'Email hệ thống', 'no-reply@azsolution.vn', NULL, 0, 0, 0),
(6, 'usermail', 'User send mail', 'sales', NULL, 0, 0, 0),
(7, 'passmail', 'Pass send mail', '1qazxsw2', NULL, 0, 0, 0),
(8, 'hostmail', 'Host send mail', 'smtp.azsolution.vn', NULL, 0, 0, 0),
(9, 'portmail', 'Port send mail', '465', NULL, 0, 0, 0),
(10, 'offline', 'Thông báo offline hệ thống', '', NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_slider`
--

CREATE TABLE `tb_slider` (
  `id` int(11) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ord` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_by` int(11) NOT NULL,
  `lang` varchar(2) NOT NULL DEFAULT 'vi',
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_slider`
--

INSERT INTO `tb_slider` (`id`, `parent_id`, `name`, `ord`, `status`, `create_time`, `create_by`, `lang`, `meta_title`, `meta_description`, `meta_keyword`, `image`) VALUES
(1, 0, 'Slider 1', 1, 1, '2017-07-11 16:53:33', 1, 'vi', '', '', '', 'images/17522990-1197434327045574-9022595128972378854-n_5965028bc59e7_11_07_2017_18_53_31.jpg'),
(2, 0, 'Slider 2', 2, 1, '2017-07-11 16:54:00', 1, 'vi', '', '', '', 'images/slide-web_596502a73d0a0_11_07_2017_18_53_59.png');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `fullname` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `last_login_time` datetime DEFAULT '0000-00-00 00:00:00',
  `ord` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `create_time` timestamp NULL DEFAULT NULL,
  `create_by` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `role_id`, `fullname`, `username`, `password`, `mobile`, `email`, `address`, `avatar`, `last_login_time`, `ord`, `status`, `create_time`, `create_by`) VALUES
(1, 1, 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', '', 'admin@gmail.vn', 'Nam định', NULL, '0000-00-00 00:00:00', NULL, 1, '2016-12-14 04:11:39', 5),
(2, 2, 'quản trị viên', 'quantri', 'e10adc3949ba59abbe56e057f20f883e', '', 'contact@gmail.vn', 'Nam định', NULL, '2016-12-23 01:50:36', NULL, 1, '2016-12-14 20:00:25', 6),
(3, 3, 'Thành viên', 'member', 'aa08769cdcb26674c6706093503ff0a3', '01684958394', 'member@gmail.com', 'Hà Nội', NULL, '2016-12-23 01:52:14', NULL, 1, '2016-12-22 18:50:29', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_advisory`
--
ALTER TABLE `tb_advisory`
  ADD KEY `id` (`id`);

--
-- Indexes for table `tb_categories`
--
ALTER TABLE `tb_categories`
  ADD KEY `id` (`id`);

--
-- Indexes for table `tb_extension`
--
ALTER TABLE `tb_extension`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_field`
--
ALTER TABLE `tb_field`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_file`
--
ALTER TABLE `tb_file`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_file_mapper`
--
ALTER TABLE `tb_file_mapper`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_lang`
--
ALTER TABLE `tb_lang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_menus`
--
ALTER TABLE `tb_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_news`
--
ALTER TABLE `tb_news`
  ADD KEY `id` (`id`);

--
-- Indexes for table `tb_object`
--
ALTER TABLE `tb_object`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_object_mapper`
--
ALTER TABLE `tb_object_mapper`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_order`
--
ALTER TABLE `tb_order`
  ADD KEY `id` (`id`);

--
-- Indexes for table `tb_role`
--
ALTER TABLE `tb_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_seo`
--
ALTER TABLE `tb_seo`
  ADD KEY `id` (`id`);

--
-- Indexes for table `tb_session`
--
ALTER TABLE `tb_session`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `tb_setting`
--
ALTER TABLE `tb_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_slider`
--
ALTER TABLE `tb_slider`
  ADD KEY `id` (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_advisory`
--
ALTER TABLE `tb_advisory`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_categories`
--
ALTER TABLE `tb_categories`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_extension`
--
ALTER TABLE `tb_extension`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tb_field`
--
ALTER TABLE `tb_field`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;
--
-- AUTO_INCREMENT for table `tb_file`
--
ALTER TABLE `tb_file`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_file_mapper`
--
ALTER TABLE `tb_file_mapper`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_lang`
--
ALTER TABLE `tb_lang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_menus`
--
ALTER TABLE `tb_menus`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_news`
--
ALTER TABLE `tb_news`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tb_object`
--
ALTER TABLE `tb_object`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tb_object_mapper`
--
ALTER TABLE `tb_object_mapper`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `tb_order`
--
ALTER TABLE `tb_order`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_role`
--
ALTER TABLE `tb_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_seo`
--
ALTER TABLE `tb_seo`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_slider`
--
ALTER TABLE `tb_slider`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
