jQuery(document).ready(function(){

	if($('#modal-advisory').length)
	{
		var off_modal = false;
		var time_show_modal = $('#modal-advisory').data('show-modal');
		var time_show_modal_after_off = $('#modal-advisory').data('show-modal-after-off');

		setTimeout(function(){
			$('#modal-advisory').modal('show');
		}, time_show_modal);

		$('#modal-advisory').on('hidden.bs.modal', function () {
	        showModalTime(time_show_modal_after_off);
		});
	}

	function showModalTime(time)
	{
		setTimeout(function(){
			$('#modal-advisory').modal('show');
		}, time);
	}
	$('.advisory-form').on('click', 'button[type="submit"]', function(event) {
		event.preventDefault();
		var button_submit = $(this);
		//ajax send contact
		var data_form = {};
		var form = $(this).parent('form.advisory-form');
		var input_your_name = form.find('input[name="your-name"]');
		var input_your_phone = form.find('input[name="phone"]');

		data_form.your_name = input_your_name.val();
		data_form.your_phone = input_your_phone.val();

		if(!input_your_name.parent().find('i.form-control-feedback').length)
			input_your_name.parent().append('<i class="form-control-feedback bv-no-label glyphicon"></i>');
		if(data_form.your_name=="")
		{
			form.find('input[name="your-name"]').css('border-color', 'red');
			input_your_name.parent().removeClass('has-success').addClass('has-error');
			input_your_name.parent().find('i.form-control-feedback').removeClass('glyphicon-ok').addClass('glyphicon-remove');
			return false;
		}else{
			form.find('input[name="your-name"]').css('border-color', '#3c763d');
			input_your_name.parent().removeClass('has-error').addClass('has-success');
			input_your_name.parent().find('i.form-control-feedback').removeClass('glyphicon-remove').addClass('glyphicon-ok');
		}
		if(!input_your_phone.parent().find('i.form-control-feedback').length)
			input_your_phone.parent().append('<i class="form-control-feedback bv-no-label glyphicon"></i>');
		if(data_form.your_phone==""|| !$.isNumeric(data_form.your_phone) || data_form.your_phone.length < 10 || data_form.your_phone.length > 11 )
		{
			form.find('input[name="phone"]').css('border-color', 'red');
			input_your_phone.parent().removeClass('has-success').addClass('has-error');
			input_your_phone.parent().find('i.form-control-feedback').removeClass('glyphicon-ok').addClass('glyphicon-remove');
			return false;
		}else{
			form.find('input[name="phone"]').css('border-color', '#3c763d');
			input_your_phone.parent().removeClass('has-error').addClass('has-success');
			input_your_phone.parent().find('i.form-control-feedback').removeClass('glyphicon-remove').addClass('glyphicon-ok');
		}

		if(form.find('textarea[name="question"]').length)
		{
			data_form.question = form.find('textarea[name="question"]').val();
		}
		$.ajax({
			url: base_url + 'Bluesoft_c/sendMailAjax/',
			type: 'POST',
			dataType: 'json',
			data: data_form,
			beforeSend : function (){
				button_submit.text('Đang xử lý');
			},
		})
		.done(function(res) {
			if(res.send == true)
			{
				window.location.href = base_url + 'home';
			}
		})
		.fail(function(er1, er2, er3) {
			console.log(er3);
		});
	});
	
});