$(function() {
	$(document).on('click', '.add_cart', function(event) {
		event.preventDefault();
		var $id = $(this).attr('pro-id');
		$.ajax({
			url: 'Bluesoft_c/cart/'+$id,
			type: 'GET',
			dataType : 'json'
		})
		.done(function(data) {
			reloadCart(data);
		})
		.fail(function() {
			
		})
		.always(function() {
			
		});
		
	});


	$(document).on('click', '._cart_delete', function(event) {
		event.preventDefault();
		var $rowid = $(this).attr('pro-id');
		var data =  {'action':'delete_cart', 'rowid':$rowid};
		$.ajax({
			url: 'bluesoft_c/deleteCart/',
			type: 'POST',
			data: data,
			dataType : 'json'
		})
		.done(function(data) {
			reloadCart(data);
		})
		.fail(function() {
			console.log('error delete cart');	
		});
		
	});
	
	$(document).on('click', '._cart_updateall', function(event) {
		event.preventDefault();
		
		var _obj = {};
		_obj['cart'] = JSON.stringify(updateAllCart());
		$.ajax({
			url: 'bluesoft_c/cart',
			type: 'POST',
			data: _obj,
			dataType : 'json'
		})
		.done(function(data) {
			reloadCart(data);
		})
		.fail(function() {
			console.log('error cart update all');	
		});
		
		
	});

	// $('._cart_city').change(function(event) {
	// 	$.ajax({
	// 		url: 'Vindex/getCity',
	// 		type: 'POST',
	// 		data: {id:$(this).val()},
	// 	})
	// 	.done(function(data) {
	// 		try{
	// 			var json = JSON.parse(data);
	// 			if(json.code==200){
	// 				$.simplyToast(json.message, 'success');
	// 				var arr = json.data;
	// 				var str = "";
	// 				for(var i =0;i<arr.length;i++){
	// 					str +="<option value='"+arr[i].id+"'>"+arr[i].name+"</option>";
	// 				}
	// 				$('._cart_provide').html(str);
	// 			}
	// 			else{
	// 				$.simplyToast(json.message, 'danger');
	// 			}
	// 		}
	// 		catch(ex){

	// 		}
	// 	})
	// 	.fail(function() {
	// 	})
	// 	.always(function() {
	// 	});
		
	// });

	$(document).on('submit', 'form.form-cart', function(event) {
		event.preventDefault();
		if(validateFormCart(this)){
			$.ajax({
				url: 'Bluesoft_c/checkout',
				type: 'POST',
				data: $(this).serialize(),
				dataType : 'json'
			})
			.done(function(json) {
				if(json.code==200){
					$('#modalCart').modal('hide');
					$('#modalCartSuccess').modal('show');
				}
				else{
					$.simplyToast(json.message, 'danger');
				}
			})
			.fail(function() {
			});
			
		}
	});



});

function validateFormCart(frm){
	var inputs= $(frm).find('input');
	for (var i = 0; i < inputs.length; i++) {
		var item = $(inputs[i]);
		if(item!=undefined && item.val().trim()==""){
			$.simplyToast("Vui lòng nhập " + item.attr('placeholder'), 'danger');
			return false;
		}
	}
	return true;
}
function reloadCart(json){
	// try{

		if(json.code==200){
			$.simplyToast(json.message, 'success');
			$('#modalCart .modal-body').load('bluesoft_c/load_cart').parent().parent().parent().modal('show');
			
		}
		else{
			console.log(json.code);
			alert('ẻtre');
			$.simplyToast(json.message, 'danger');
		}
	// }
	// catch(ex){

	// }
}
function updateAllCart(){
	var trs = $('.cart-form tbody tr');
	var send =[];
	for (var i = 0; i < trs.length; i++) {
		var item = $(trs[i]);
		var rowid = item.find('input[name=rowid]').val();
		var qty = item.find('input.qty').val();
		var obj = new Object();
		obj.rowid = rowid;
		obj.qty = qty;
		send.push(obj);
	}
	return send;
}