<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2016, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['imglib_source_image_required'] = "Bạn phải chỉ ra ảnh nguồn trong bảng thiết lập.";
$lang['imglib_gd_required'] = "Tính năng này yêu cầu máy chủ phải có thư viện GD.";
$lang['imglib_gd_required_for_props'] = "Máy chủ phải hỗ trợ thư viện GD để có thể xác định các thuộc tính của ảnh.";
$lang['imglib_unsupported_imagecreate'] = "Máy chủ không hỗ trợ các thao tác của thư viện GD cho việc xử lý ảnh.";
$lang['imglib_gif_not_supported'] = "Không hỗ trợ ảnh GIF do vấn đề bản quyền.  Bạn phải sử dụng JPG hoặc PNG.";
$lang['imglib_jpg_not_supported'] = "Không hỗ trợ ảnh JPG.";
$lang['imglib_png_not_supported'] = "Không hỗ trợ ảnh PNG.";
$lang['imglib_jpg_or_png_required'] = "Phương thức thay đổi kích thước ảnh đã thiết lập chỉ hoạt động được với JPEG hoặc PNG.";
$lang['imglib_copy_error'] = "Lỗi xảy ra khi thay thế file.  Hãy chắc chắn thư mục đã được thiết lập quyền ghi.";
$lang['imglib_rotate_unsupported'] = "Thao tác quay ảnh không được máy chủ hỗ trợ.";
$lang['imglib_libpath_invalid'] = "Đường dẫn đến thư viện ảnh không đúng.  Hãy thiết lập lại cho đúng.";
$lang['imglib_image_process_failed'] = "Xử lý lỗi.  Hãy kiểm tra máy chủ hỗ trợ phương thức đã chọn và đường dẫn đến thư viện ảnh là đúng.";
$lang['imglib_rotation_angle_required'] = "Phải nhập góc quay để thực hiện thao tác quay ảnh.";
$lang['imglib_invalid_path'] = "Đường dẫn đến file ảnh không đúng.";
$lang['imglib_copy_failed'] = "Thao tác copy ảnh không thực hiện được.";
$lang['imglib_missing_font'] = "Không tìm thấy font để sử dụng.";
$lang['imglib_save_failed'] = "Không thể lưu ảnh.  Hãy chắc chắn rằng file & thư mục đã được thiết lập quyền ghi.";
