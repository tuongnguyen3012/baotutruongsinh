<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2016, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['cal_su'] = 'CN';
$lang['cal_mo'] = 'T2';
$lang['cal_tu'] = 'T3';
$lang['cal_we'] = 'T4';
$lang['cal_th'] = 'T5';
$lang['cal_fr'] = 'T6';
$lang['cal_sa'] = 'T7';
$lang['cal_sun'] = 'CN';
$lang['cal_mon'] = 'T2';
$lang['cal_tue'] = 'T3';
$lang['cal_wed'] = 'T4';
$lang['cal_thu'] = 'T5';
$lang['cal_fri'] = 'T6';
$lang['cal_sat'] = 'T7';
$lang['cal_sunday'] = 'Chủ Nhật';
$lang['cal_monday'] = 'Thứ 2';
$lang['cal_tuesday'] = 'Thứ 3';
$lang['cal_wednesday'] = 'Thứ 4';
$lang['cal_thursday'] = 'Thứ 5';
$lang['cal_friday'] = 'Thứ 6';
$lang['cal_saturday'] = 'Thứ 7';
$lang['cal_jan'] = 'Th1';
$lang['cal_feb'] = 'Th2';
$lang['cal_mar'] = 'Th3';
$lang['cal_apr'] = 'Th4';
$lang['cal_may'] = 'Th5';
$lang['cal_jun'] = 'Th6';
$lang['cal_jul'] = 'Th7';
$lang['cal_aug'] = 'Th8';
$lang['cal_sep'] = 'Th9';
$lang['cal_oct'] = 'Th10';
$lang['cal_nov'] = 'Th11';
$lang['cal_dec'] = 'Th12';
$lang['cal_january'] = 'Tháng Giêng';
$lang['cal_february'] = 'Tháng Hai';
$lang['cal_march'] = 'Tháng Ba';
$lang['cal_april'] = 'Tháng Tư';
$lang['cal_mayl'] = 'Tháng Năm';
$lang['cal_june'] = 'Tháng Sáu';
$lang['cal_july'] = 'Tháng Bảy';
$lang['cal_august'] = 'Tháng Tám';
$lang['cal_september'] = 'Tháng Chín';
$lang['cal_october'] = 'Tháng Mười';
$lang['cal_november'] = 'Tháng Mười Một';
$lang['cal_december'] = 'Tháng Mười Hai';
