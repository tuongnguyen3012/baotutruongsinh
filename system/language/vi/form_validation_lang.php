<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2016, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_required']		= '{field} cần được nhập.';
$lang['form_validation_isset']			= '{field} phải có giá trị.';
$lang['form_validation_valid_email']		= '{field} không phải địa chỉ hòm thư hợp lệ.';
$lang['form_validation_valid_emails']		= '{field} phải chứa tất cả các địa chỉ hòm thư hợp lệ.';
$lang['form_validation_valid_url']		= '{field} không phải đường dẫn hợp lệ.';
$lang['form_validation_valid_ip']		= '{field} không phải địa chỉ IP hợp lệ.';
$lang['form_validation_min_length']		= '{field} phải có ít nhất {param} ký tự.';
$lang['form_validation_max_length']		= '{field} không thể vượt quá {param} ký tự.';
$lang['form_validation_exact_length']		= '{field} phải có chính xác {param} ký tự.';
$lang['form_validation_alpha']			= '{field} chỉ chứa các ký tự chữ cái.';
$lang['form_validation_alpha_numeric']		= '{field} chỉ chứa các ký tự chữ-số.';
$lang['form_validation_alpha_numeric_spaces']	= '{field} chỉ chứa các ký tự chữ và số, khoảng trắng.';
$lang['form_validation_alpha_dash']		= '{field} chỉ có thể chứa các ký tự chữ số, gạch dưới, và dấu gạch ngang.';
$lang['form_validation_numeric']		= '{field} chỉ chứa số.';
$lang['form_validation_is_numeric']		= '{field} chỉ chứa các ký tự số.';
$lang['form_validation_integer']		= '{field} chỉ chứa số nguyên.';
$lang['form_validation_regex_match']		= '{field} không có định dạng đúng.';
$lang['form_validation_matches']		= '{field} không giống với {param}.';
$lang['form_validation_differs']		= '{field} phải khác với {param}.';
$lang['form_validation_is_unique'] 		= '{field} đã tồn tại.';
$lang['form_validation_is_natural']		= '{field} chỉ chứa các chữ số.';
$lang['form_validation_is_natural_no_zero']	= '{field} chỉ chứa các chữ số và lớn hơn 0.';
$lang['form_validation_decimal']		= '{field} chỉ chứa số thập phân.';
$lang['form_validation_less_than']		= '{field} phải nhỏ hơn {param}.';
$lang['form_validation_less_than_equal_to']	= '{field} phải nhỏ hơn hoặc bằng{param}.';
$lang['form_validation_greater_than']		= '{field} phải lớn hơn {param}.';
$lang['form_validation_greater_than_equal_to']	= '{field} phải lớn hơn hoặc bằng {param}.';
$lang['form_validation_error_message_not_set']	= '{field} gặp lỗi không xác định.';
$lang['form_validation_in_list']		= '{field} phải nằm trong: {param}.';
