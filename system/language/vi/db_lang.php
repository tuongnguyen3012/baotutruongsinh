<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2016, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['db_invalid_connection_str'] = 'Không thể xác định các thiết lập CSDL dựa vào chuỗi bạn đã gửi.';
$lang['db_unable_to_connect'] = 'Không thể kết nối đến CSDL của bạn bằng cách sử dụng các thiết lập được cung cấp.';
$lang['db_unable_to_select'] = 'Không thể chọn CSDL: %s';
$lang['db_unable_to_create'] = 'Không thể tạo CSDL: %s';
$lang['db_invalid_query'] = 'Các truy vấn không hợp lệ.';
$lang['db_must_set_table'] = 'Bạn phải thiết lập CSDL để thực hiện truy vấn.';
$lang['db_must_use_set'] = 'Bạn phải sử dụng phương pháp "set" để cập nhật một mục.';
$lang['db_must_use_index'] = 'Bạn phải xác định chỉ số phù hợp để cập nhật hàng loạt.';
$lang['db_batch_missing_index'] = 'Một hoặc nhiều hàng thiếu chỉ số quy định.';
$lang['db_must_use_where'] = 'Không được phép cập nhật trừ khi có mệnh đề "where".';
$lang['db_del_must_use_where'] = 'Không được phép xóa trừ khí có mệnh đề "where" hoặc "like".';
$lang['db_field_param_missing'] = 'Để tìm cần có tên bảng.';
$lang['db_unsupported_function'] = 'Tính năng này không được hỗ trợ trong CSDL bạn đang sử dụng.';
$lang['db_transaction_failure'] = 'Thao tác lỗi: Đã quay lại.';
$lang['db_unable_to_drop'] = 'Không thể xóa các CSDL.';
$lang['db_unsupported_feature'] = 'Tính năng không được hỗ trợ trong nền tảng CSDL bạn đang sử dụng.';
$lang['db_unsupported_compression'] = 'Định dạng tập tin không được hỗ trợ bỏi máy chủ của bạn.';
$lang['db_filepath_error'] = 'Không thể ghi dữ liệu vào đường dẫn bạn đã chọn.';
$lang['db_invalid_cache_path'] = 'Đường dẫn bộ nhớ cache bạn chọn không hợp lệ hoặc ghi được.';
$lang['db_table_name_required'] = 'Cần có tên bảng.';
$lang['db_column_name_required'] = 'Cần có tên cột.';
$lang['db_column_definition_required'] = 'Cần định nghĩa cột này.';
$lang['db_unable_to_set_charset'] = 'Unable to set client connection character set: %s';
$lang['db_error_heading'] = 'Xảy ra lỗi CSDL';
