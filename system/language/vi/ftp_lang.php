<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2016, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['ftp_no_connection']			= "Không tìm thấy ID kết nối. Hãy chắc chắn rằng bạn đã kết nối trước khi thực hiện các thao tác file.";
$lang['ftp_unable_to_connect']		= "Không thể kết nối đến máy chủ FTP.";
$lang['ftp_unable_to_login']		= "Không thể đăng nhập vào máy chủ FTP. Hãy kiểm tra lại tên và mật khẩu.";
$lang['ftp_unable_to_mkdir']		= "Không thể tạo thư mục.";
$lang['ftp_unable_to_changedir']	= "Không thể thay đổi thư mục.";
$lang['ftp_unable_to_chmod']		= "Không thể thiết lập quyền. Hãy kiểm tra lại đường dẫn. Chú ý: Tính năng này chỉ được hỗ trợ từ PHP 5 trở lên";
$lang['ftp_unable_to_upload']		= "Không thể tải lên file. Hãy kiểm tra lại đường dẫn.";
$lang['ftp_unable_to_download']		= "Không thể tải về file. Hãy kiểm tra lại đường dẫn.";
$lang['ftp_no_source_file']			= "Không tìm thấy file nguồn. Hãy kiểm tra lại đường dẫn.";
$lang['ftp_unable_to_rename']		= "Không thể đổi tên file.";
$lang['ftp_unable_to_delete']		= "Không thẻ xóa file.";
$lang['ftp_unable_to_move']			= "Không thể di chuyển file. Hãy chắc chắn rằng thư mục đích tồn tại.";
