<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2016, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['upload_userfile_not_set'] = 'Không thể tìm thấy tập tin của người dùng.';
$lang['upload_file_exceeds_limit'] = 'Tập tin tải lên vượt quá kích thước tối đa cho phép trong cấu hình PHP.';
$lang['upload_file_exceeds_form_limit'] = 'Tập tin tải lên vượt quá kích thước tối đa cho phép.';
$lang['upload_file_partial'] = 'Tập tin chỉ tải lên được một phần.';
$lang['upload_no_temp_directory'] = 'Không tìm thấy thư mục lưu tạm.';
$lang['upload_unable_to_write_file'] = 'Tập tin không thể lưu vào đĩa.';
$lang['upload_stopped_by_extension'] = 'Tập tin tải lên đã dừng bởi phần bổ sung.';
$lang['upload_no_file_selected'] = 'Bạn chưa chọn tập tin tải lên.';
$lang['upload_invalid_filetype'] = 'Kiểu tập tin tải lên không phù hợp.';
$lang['upload_invalid_filesize'] = 'Tập tin tải lên lớn hơn kích thước quy định.';
$lang['upload_invalid_dimensions'] = 'Hình ảnh tải lên lớn hơn kích thước quy định.';
$lang['upload_destination_error'] = 'Có lỗi khi tải tập tin lên máy chủ.';
$lang['upload_no_filepath'] = 'Đường dẫn tải lên không hợp lệ.';
$lang['upload_no_file_types'] = 'Không tập tin nào được phép.';
$lang['upload_bad_filename'] = 'Tập tin đã tồn tại trên máy chủ.';
$lang['upload_not_writable'] = 'Không có thư mục để tải lên.';
