<?php
/**
 * Description of profile
 * @author trungthuc
 * @date Jan 27, 2015
 */
?>
<div class="header" style="height: 50px;">
    <div class="logo" style="padding-top:10px;font-size:30px;color:#fff;">
        Admin
    </div>
    <div class="headerinner">
        <ul class="headmenu">
            <li class="right" style="height: 50px;">
                <div class="userloggedinfo">
                    <div class="dropdown userinfo" style="padding:15px 10px;">
                        <h5 data-toggle="dropdown" style="cursor:pointer;">
                        <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                        <?php echo $this->session->userdata('fullname'); ?> 
                        <small>- <?php echo $this->session->userdata('email'); ?></small></h5>
                        <ul class="dropdown-menu" style="background: #1075BB;left:auto;right:-1px;">
                            <li><?php echo anchor('admin/user/edit/'.$this->session->userdata('user_id'), 'Thông tin cá nhân') ?></li>
                            <!-- <li><?php echo anchor('admin/user/setting', 'Cài đặt tài khoản') ?></li> -->
                            <li><?php echo anchor('admin/logout', 'Thoát') ?></li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul><!--headmenu-->
    </div>
</div>