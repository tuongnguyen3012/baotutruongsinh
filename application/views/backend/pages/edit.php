<?php
/**
 * Description of edit
 * @author trungthuc
 * @date Feb 3, 2015
 */
?>
<?php $this->load->view('backend/breadcrumbs'); ?>
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-sitemap"></span></div>
    <div class="pagetitle">
        <h1><?php echo $title ?></h1>
    </div>
</div><!--pageheader-->
<div class="maincontent">
    <div class="maincontentinner">
		<?php echo form_open_multipart('', array('class' => 'objects stdform')); ?>

        <div class="widget">
            <h4 class="widgettitle"><?php echo $title ?></h4>
            <div class="widgetcontent">
				<p>
					<?php
					echo form_ckeditor(array(
						'name' => 'html',
						'id' => 'html',
						'width' => '100%',
						'height' => '450',
						'value' => $detail['html']
					));
					?>
				</p>
                <p>
					<button type="submit" class="btn btn-submit btn-primary btn-rounded"><i class="fa fa-save"></i> Lưu</button>
                </p>
            </div><!--widgetcontent-->
        </div>
		<?php echo form_close(); ?>
		<?php $this->load->view('backend/footer'); ?>
    </div>
</div>
