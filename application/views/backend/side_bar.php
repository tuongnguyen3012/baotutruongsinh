<?php
/**
 * Description of side_bar
 * @author trungthuc
 * @date Jan 26, 2015
 */
?>
<div class="leftpanel">

    <div class="leftmenu">        
        <ul class="nav nav-tabs nav-stacked">
            <li class="nav-header">Navigation</li>
            <li <?php if ($this->controller == 'admin') echo 'class="active"' ?>><?php echo anchor('admin', '<span class="fa fa-laptop"></span> Dashboard'); ?></a></li>
            <li class="dropdown" <?php if ($this->controller == 'user') echo 'class="active"' ?>><?php echo anchor('admin/user', '<span class=" fa fa-sitemap"></span> Quản lý thành viên'); ?>
                <ul>
                    <li><?php echo anchor('admin/group/index/', 'Quản lý nhóm'); ?></li>
                    <li><?php echo anchor('admin/user/index/', 'Quản lý tài khoản'); ?></li>
                </ul>
            </li>
			<!-- <li class="dropdown" <?php if ($this->controller == 'menu') echo 'class="active"' ?>><?php echo anchor('admin/menu', '<span class=" fa fa-sitemap"></span> Quản lý Menu'); ?>
				<ul>
					<li><?php echo anchor('admin/menu/1', 'Menu ngang'); ?></li>
					<li><?php echo anchor('admin/menu/2', 'Menu dọc'); ?></li>
				</ul>
			</li> -->
			<?php if ($this->session->userdata('role_id') == 1): ?>
				<!-- <li <?php if ($this->controller == 'object') echo 'class="active"' ?>><?php echo anchor('admin/object', '<span class="fa fa-lock"></span> Objects'); ?></li> -->
				<!-- <li class="dropdown" <?php if ($this->controller == 'field') echo 'class="active"' ?>><?php echo anchor('admin/field', '<span class=" fa fa-sitemap"></span> Fields'); ?>
					<ul>
						<?php
						$objects = $this->adm->__getObjects();
						foreach ($objects as $row):
							?>
							<li><?php echo anchor('admin/field/index/' . $row['id'], $row['label']); ?></li>
						<?php endforeach;
						?>
					</ul>
				</li> -->
			<?php endif; ?>
			<li class="dropdown" <?php if ($this->controller == 'extension') echo 'class="active"' ?>><?php echo anchor('admin/extension/adv', '<span class=" fa fa-sitemap"></span> Tùy chỉnh mở rộng'); ?>
                <ul <?php if ($this->controller == 'extension') echo 'style="display: block;"' ?>>
					<li <?php if ($this->controller == 'extension' && $this->action == 'logo') echo 'class="active"' ?>><?php echo anchor('admin/extension/logo', 'Quản lý Logo'); ?></li>
					<li <?php if ($this->controller == 'extension' && $this->action == 'hotline') echo 'class="active"' ?>><?php echo anchor('admin/extension/hotline', 'Quản lý Hotline'); ?></li>
					<li <?php if ($this->controller == 'extension' && $this->action == 'email') echo 'class="active"' ?>><?php echo anchor('admin/extension/email', 'Quản lý Email'); ?></li>
					<li <?php if ($this->controller == 'extension' && $this->action == 'email') echo 'class="active"' ?>><?php echo anchor('admin/extension/website', 'Quản lý website'); ?></li>
					<li <?php if ($this->controller == 'extension' && $this->action == 'footer') echo 'class="active"' ?>><?php echo anchor('admin/extension/footer', 'Quản lý Thông tin Footer'); ?></li>
					<li <?php if ($this->controller == 'extension' && $this->action == 'contacts') echo 'class="active"' ?>><?php echo anchor('admin/extension/contacts', 'Thông tin hiển thị thành công'); ?></li>
                	<li <?php if ($this->controller == 'extension' && $this->action == 'social') echo 'class="active"' ?>><?php echo anchor('admin/extension/social', 'Quản lý thời gian hiện popup'); ?></li>
                	<li <?php if ($this->controller == 'extension' && $this->action == 'maps') echo 'class="active"' ?>><?php echo anchor('admin/extension/maps', 'Quản lý copyright'); ?></li>
                	<li <?php if ($this->controller == 'extension' && $this->action == 'socials') echo 'class="active"' ?>><?php echo anchor('admin/extension/socials', 'Quản lý mạng xã hội'); ?></li>
                </ul>
            </li>
			<!-- <li class="dropdown" <?php if ($this->controller == 'menu') echo 'class="active"' ?>><?php echo anchor('admin/menu', '<span class=" fa fa-sitemap"></span>  Quản lý Menu'); ?>
                <ul <?php if ($this->controller == 'menu') echo 'style="display: block;"' ?>>
					<?php foreach ($menu as $key => $row) { ?>
						<li <?php if ($this->controller == 'menu') echo 'class="active"' ?>><?php echo anchor('admin/menu/index/' . $key, $row); ?></li>
					<?php } ?>
				</ul>
            </li>
			<li <?php if ($this->controller == 'module') echo 'class="active"' ?>><?php echo anchor('admin/module', '<span class="fa fa-briefcase"></span> Quản lý Module'); ?></li>
			<li <?php if ($this->controller == 'pages') echo 'class="active"' ?>><?php echo anchor('admin/pages', '<span class="fa fa-book"></span> Quản lý Pages'); ?></li>
			<li <?php if ($this->controller == 'order') echo 'class="active"' ?>><?php echo anchor('admin/order', '<span class="fa fa-lock"></span> Quản lý đơn hàng'); ?></li>
			<li <?php if ($this->controller == 'comment') echo 'class="active"' ?>><?php echo anchor('admin/comment', '<span class="fa fa-lock"></span> Quản lý bình luận'); ?></li> -->
			<?php
			$objects = $this->adm->__getObjects();
			foreach ($objects as $row):
				?>
				<li <?php if ($this->controller == 'record' && $object_id == $row['id']) echo 'class="active"' ?>><?php echo anchor('admin/record/index/' . $row['id'], '<span class="fa fa-briefcase"></span> ' . $row['label']); ?></li>
			<?php endforeach;
			?>
	<!-- <li <?php if ($this->controller == 'extension' && $this->action == 'contact') echo 'class="active"' ?>><?php echo anchor('admin/extension/contact', '<span class="fa fa-briefcase"></span> Quản lý Yêu cầu liên hệ'); ?></li> -->
        </ul>
    </div><!--leftmenu-->

</div><!-- leftpanel -->
