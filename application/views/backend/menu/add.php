<?php
/**
 * Description of edit
 * @author trungthuc
 * @date Jan 27, 2015
 */
?>
<?php $this->load->view('backend/breadcrumbs'); ?>
<div class="pageheader">
    <div class="pageicon"><span class="fa fa-lock"></span></div>
    <div class="pagetitle">
        <h5>Thêm mới</h5>
        <h1><?php echo $title ?></h1>
    </div>
</div><!--pageheader-->

<div class="maincontent">
    <div class="maincontentinner">
		<?php echo form_open_multipart('', array('class' => 'menu stdform')); ?>

        <div class="widget">
            <h4 class="widgettitle">Thêm mới</h4>
            <div class="widgetcontent">
                <p class="name">
					<?php echo form_label('Tên menu', 'label') ?>
                    <span class="field">
						<?php
						echo form_input(array(
							'name' => 'name',
							'id' => 'name',
							'class' => 'input-block-level',
							'required' => true,
							'placeholder' => 'Tên Menu'
						));
						?>
                    </span>
                </p>
				<p class="alias" style="display: none">
					<?php echo form_label('Alias', 'label') ?>
                    <span class="field">
						<?php
						echo form_input(array(
							'name' => 'alias',
							'id' => 'alias',
							'class' => 'input-block-level',
							'placeholder' => 'Alias'
						));
						?>
                    </span>
                </p>
                <p>
					<?php echo form_label('Menu cấp cha', 'label') ?>
                    <span class="field">
						<?php
						echo form_dropdown('parent_id', $menus);
						?>
                    </span>
                </p>
                <p>
					<?php echo form_label('Loại menu', 'label') ?>
                    <span class="field">
						<?php
						echo form_dropdown('type', $type, 0, 'id="menu_type"')
						?>
                    </span>
                </p>
				<p class="html_menu"></p>
				<p class="html_menu_category"></p>
				<p class="html_menu_single"></p>
                <p>
                    <span class="field">
                        <button type="text" class="btn btn-primary btn-rounded"><i class="fa fa-save"></i> Lưu</button>
						<?php echo anchor('admin/user', '<i class="fa fa-retweet"></i> Hủy', array('class' => 'btn btn-primary ')); ?>
                    </span>

                </p>

            </div><!--widgetcontent-->
        </div>


		<?php echo form_close(); ?>

		<?php $this->load->view('backend/footer'); ?>
    </div>
</div>