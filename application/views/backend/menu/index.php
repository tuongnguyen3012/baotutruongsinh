<?php
/**
 * Description of index
 * @author trungthuc
 * @date Jan 27, 2015
 */
?>
<?php $this->load->view('backend/breadcrumbs'); ?>
<div class="maincontent">

    <div class="maincontentinner">
		<div class="row">
			<div class="col-sm-4">
				<?php echo form_open('admin/menu/save/'.$type_id, array('class' => '')); ?>
				<b>Thêm Menu</b>
				<p>
					<label for="type">Tên hiển thị</label>
					<input required="required" type="text" name="name" placeholder="Tên hiển thị" /></p>
				<p>
					<label for="type">Đường dẫn</label>
					<input type="text" name="url" placeholder="Đường dẫn" />
				</p>
				<p>
					<label for="type">Danh mục</label>
					<?php echo form_dropdown('danhmuc_id', $categories, '', 'class="uniformselect" id="danhmuc_id"'); ?>
				</p>
				<p>
					<label for="type">Bài Viết</label>
					<?php echo form_dropdown('baiviet_id', $news, '', 'class="uniformselect" id="baiviet_id"'); ?>
				</p>
				<p>
<!--					<button class="btn" id="addmenu">Thêm</button>-->
					<button class="btn" id="addmenu">Lưu</button>
				</p>
				<input type="hidden" name="id" value="" />
				<?php echo form_close(); ?>
			</div>
			<div class="col-sm-8">
				<img id="loadingmenu" src="<?php echo site_url(); ?>/template/backend/images/loading.gif">
				<?php echo $menu_html; ?>
			</div>
		</div>
		<?php $this->load->view('backend/footer'); ?>
    </div>
</div>
<div id="myModalEditMenu" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Sửa menu</h4>
			</div>
			<div class="modal-body">

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Thoát</button>
			</div>
		</div>
	</div>
</div>
<?php echo script_tag('template/backend/js/jquery.mjs.nestedSortable.js'); ?>
<script type="text/javascript">
	jQuery(document).ready(function($){
		jQuery('#loadingmenu').hide();
		jQuery('ol.sortable').nestedSortable({
			forcePlaceholderSize: true,
			handle: 'div',
			helper:	'clone',
			items: 'li',
			opacity: .6,
			placeholder: 'placeholder',
			revert: 250,
			tabSize: 25,
			tolerance: 'pointer',
			toleranceElement: '> div',
			maxLevels: 3,
			isTree: true,
			expandOnHover: 700,
			startCollapsed: false,
			update: function () {
				var orderNew = jQuery('ol.sortable').nestedSortable('toArray', {startDepthCount: 0});
				console.log(orderNew);
				jQuery.ajax({
					type: 'post',
					beforeSend: function(){
						jQuery('#loadingmenu').show();
					},
					success: function(){
						jQuery('#loadingmenu').hide();
					},
					url: base_url+'admin/menu/update',
					data: {menus:orderNew}
				});
			},
		});
		jQuery('.disclose').on('click', function() {
			jQuery(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
		})
		jQuery('#serialize').click(function(){
			serialized = $('ol.sortable').nestedSortable('serialize');
			jQuery('#serializeOutput').text(serialized+'\n\n');
		})
		jQuery('#toHierarchy').click(function(e){
			hiered = jQuery('ol.sortable').nestedSortable('toHierarchy', {startDepthCount: 0});
			hiered = dump(hiered);
			(typeof($('#toHierarchyOutput')[0].textContent) != 'undefined') ?
					$('#toHierarchyOutput')[0].textContent = hiered : jQuery('#toHierarchyOutput')[0].innerText = hiered;
		})
		jQuery('.editmenu').click(function(e){
			var idmenu = jQuery(this).closest('li').data('id');
			jQuery.ajax({
				type: 'get',
				beforeSend: function(){
					jQuery('#loadingmenu').show();
				},
				complete: function(){
					jQuery('#loadingmenu').hide();
				},
				url: base_url+'admin/menu/get/'+idmenu,
				dataType: 'json',
				success: function(json){
					jQuery('input[name="name"]').val(json.name);
					jQuery('input[name="url"]').val(json.url);
					jQuery('select[name="danhmuc_id"]').val(json.danhmuc_id);
					jQuery('select[name="baiviet_id"]').val(json.baiviet_id);
					jQuery('input[name="id"]').val(json.id);
				}
			});
			return false;
		})

		//ajax baiviet -> url
		jQuery(document).on('change', '#baiviet_id', function(e){
			e.preventDefault();
			$('#danhmuc_id').val(0);
			var baiviet_id = $(this).val();
			$.ajax({
				url: base_url+'admin/menu/detail_news/'+baiviet_id,
				dataType: 'json'
			})
			.done(function(json) {
				$('.maincontentinner input[name="url"]').val('');
				if ($('.maincontentinner input[name="url"]').val()=="") {
					$('.maincontentinner input[name="url"]').val(json.alias);
				};
			})
			.fail(function() {
				console.log("error");
			});
			
		});

		jQuery(document).on('change', '#danhmuc_id', function(e){
			e.preventDefault();
			$('#baiviet_id').val(0);
			var danhmuc_id = $(this).val();
			$.ajax({
				url: base_url+'admin/menu/detail_category/'+danhmuc_id,
				dataType: 'json'
			})
			.done(function(json) {
				$('.maincontentinner input[name="url"]').val('');
				if ($('.maincontentinner input[name="url"]').val()=="") {
					$('.maincontentinner input[name="url"]').val(json.alias);
				};
			})
			.fail(function() {
				console.log("error");
			});
			
		});
	});

	function dump(arr,level) {
		var dumped_text = "";
		if(!level) level = 0;
		//The padding given at the beginning of the line.
		var level_padding = "";
		for(var j=0;j<level+1;j++) level_padding += "    ";

		if(typeof(arr) == 'object') { //Array/Hashes/Objects
			for(var item in arr) {
				var value = arr[item];

				if(typeof(value) == 'object') { //If it is an array,
					dumped_text += level_padding + "'" + item + "' ...\n";
					dumped_text += dump(value,level+1);
				} else {
					dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
				}
			}
		} else { //Strings/Chars/Numbers etc.
			dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
		}
		return dumped_text;
	}
</script>
