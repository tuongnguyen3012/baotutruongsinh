<?php
/**
 * Description of edit
 * @author trungthuc
 * @date Jan 27, 2015
 */
?>
<?php $this->load->view('backend/breadcrumbs'); ?>
<div class="pageheader">
    <form action="results.html" method="post" class="searchbar">
        <input type="text" name="keyword" placeholder="Tìm kiếm..." />
    </form>
    <div class="pageicon"><span class="fa fa-lock"></span></div>
    <div class="pagetitle">
        <h5></h5>
        <h1><?php echo $title ?></h1>
    </div>
</div><!--pageheader-->
<?php $roleId = $this->session->userdata('role_id'); ?>
<div class="maincontent">
    <div class="maincontentinner">
        <?php echo form_open_multipart('', array('class' => 'objects stdform')); ?>

        <div class="widget">
            <h4 class="widgettitle">Chỉnh sửa từ </h4>
            <div class="widgetcontent">
                <?php echo form_hidden('ID', $translate['id']); ?>
                <p>
                    <?php echo form_label('Tên Tiếng Việt', 'label') ?>
                    <span class="field">
                        <?php
                        echo form_input(array(
                            'name' => 'name_vi',
                            'id' => 'name_vi',
                            'value' => $translate['name_vi'],
                            'class' => 'input-block-level',
                            'required' => true,
                            'placeholder' => 'Tên Tiếng Việt'
                        ));
                        ?>
                    </span>
                </p>
                <p>
                    <?php echo form_label('Tên Tiếng Anh', 'label') ?>
                    <span class="field">
                        <?php
                        echo form_input(array(
                            'name' => 'name_en',
                            'id' => 'name_en',
                            'value' => $translate['name_en'],
                            'class' => 'input-block-level',
                            'required' => true,
                            'placeholder' => 'Tên Tiếng Anh'
                        ));
                        ?>
                    </span>
                </p>
                <p>
                    <?php echo form_label('Tên Tiếng Pháp', 'label') ?>
                    <span class="field">
                        <?php
                        echo form_input(array(
                            'name' => 'name_fr',
                            'id' => 'name_fr',
                            'value' => $translate['name_fr'],
                            'class' => 'input-block-level',
                            'required' => true,
                            'placeholder' => 'Tên Tiếng Pháp'
                        ));
                        ?>
                    </span>
                </p>
                <p>
                    <span class="field">
                        <button type="text" class="btn btn-primary btn-rounded"><i class="fa fa-save"></i> Lưu</button>
<!--                        <button type="reset" class="btn btn-primary btn-rounded"><i class="fa fa-refresh"></i> Reset</button>-->
                        <?php echo anchor('admin/user', '<i class="fa fa-retweet"></i> Hủy', array('class' => 'btn btn-primary ')); ?>
                    </span>

                </p>

            </div><!--widgetcontent-->
        </div>


        <?php echo form_close(); ?>

        <?php $this->load->view('backend/footer'); ?>
    </div>
</div>