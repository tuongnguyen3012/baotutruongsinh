<?php
/**
 * Description of index
 * @author trungthuc
 * @date Jan 27, 2015
 */
?>
<?php $this->load->view('backend/breadcrumbs'); ?>
<div class="pageheader">
    <form action="results.html" method="post" class="searchbar">
        <input type="text" name="keyword" placeholder="Tìm kiếm..." />
    </form>
    <div class="pageicon"><span class="fa fa-lock"></span></div>
    <div class="pagetitle">
        <h5></h5>
        <h1><?php echo $title ?></h1>
    </div>
</div><!--pageheader-->

<div class="maincontent">
    <div class="maincontentinner">
        <?php echo form_open('', array('class' => 'objects')); ?>
        <?php $roleId = $this->session->userdata('role_id'); ?>
        <?php if ($roleId < 3): ?>
            <?php echo anchor('admin/lang/add', '<i class="fa fa-plus"></i> Thêm từ mới', array('class' => 'btn btn-primary ')); ?>
                                <!--        <button type="button" id="btn_update" class="btn btn-primary btn-rounded"><i class="fa fa-pencil"></i> Cập nhật</button>-->
            <button type="button" id="btn_delete" class="btn btn-danger btn-rounded"><i class="fa fa-remove"></i> Xóa</button>
        <?php endif; ?>
        <table class="table table-bordered">
            <tr>
                <th class="head0">
                    <input type="checkbox" class="checkall" />
                </th>
                <th class="head1">ID</th>
                <th class="head1">Tiếng Việt</th>
                <th class="head0">Tiếng Anh</th>
                <th class="head0">Xóa</th>
            </tr>

            <?php foreach ($translate as $row): ?>
                <tr class="gradeX">
                    <td class="aligncenter" width="10">
                        <span class="center">
                            <input type="checkbox" name="chk_<?php echo $row['id']; ?>" />
                        </span>
                    </td>
                    <td width="30"><?php echo $row['id']; ?></td>
                    <td><?php echo anchor('admin/lang/edit/' . $row['id'], $row['name_vi']); ?></td>
                    <td><?php echo anchor('admin/lang/edit/' . $row['id'], $row['name_en']); ?></td>
                    <td class="center" width="20">
                        <?php echo anchor('admin/lang/delete/' . $row['id'], '<span class="icon-trash"></span>', array('class' => 'deleterow', 'rel' => $row['id'])) ?>
                    </td>
                </tr>
                <?php echo form_hidden('ID[]', $row['id']); ?>
            <?php endforeach; ?>

        </table>
        <input type="hidden" name="action" class="action" value="" />
        <?php echo form_close(); ?>
        <?php
        $this->pagination->initialize($config);
        echo $this->pagination->create_links();
        ?>

        <?php $this->load->view('backend/footer'); ?>
    </div>
</div>