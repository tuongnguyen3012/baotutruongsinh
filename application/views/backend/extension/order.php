<?php
/**
 * Description of index
 * @author trungthuc
 * @date Jan 27, 2015
 */
?>
<?php $this->load->view('backend/breadcrumbs'); ?>
<div class="pageheader">
    <form action="results.html" method="post" class="searchbar">
        <input type="text" name="keyword" placeholder="Nhập mã đơn hàng..." />
    </form>
    <div class="pageicon"><span class="iconfa-lock"></span></div>
    <div class="pagetitle">
        <h1><?php echo $title ?></h1>
    </div>
</div><!--pageheader-->

<div class="maincontent">
    <div class="maincontentinner">
		<?php echo form_open('', array('class' => 'objects')); ?>
        <button type="button" id="btn_update" class="btn btn-primary btn-rounded"><i class="iconfa-pencil"></i> Cập nhật đơn hàng</button>
        <button type="button" id="btn_delete" class="btn btn-danger btn-rounded"><i class="iconfa-remove"></i> Xóa</button>
		<a href="?status=1" class="btn btn-success alertsuccess"><i class="icon-ok"></i><small> Đơn hàng đã xử lý</small></a>
		<a href="?status=0" class="btn btn-warning alertwarning"><i class="icon-remove"></i><small> Đơn hàng chưa được xử lý</small></a>
		<br />

        <table class="table table-bordered">
            <tr>
                <th class="head0">
                    <input type="checkbox" class="checkall" />
                </th>
                <th class="head1">#Sku</th>
                <th class="head0">Khách hàng</th>
				<th class="head1">Điện thoại</th>
				<th class="head0">Địa chỉ</th>
                <th class="head1">Tổng tiền mua</th>
                <th class="head0">Ngày đặt hàng</th>
                <th class="head1">Trạng thái</th>
				<th class="head0">Xóa</th>
            </tr>

			<?php foreach ($rows as $row): ?>
				<tr class="gradeX">
					<td class="aligncenter" width="10">
						<span class="center">
							<input type="checkbox" name="chk_<?php echo $row['id']; ?>" />
						</span>
					</td>
					<td class="aligncenter" width="10">
						<?php echo anchor('admin/extension/detailorder/' . $row['id'], '#' . $row['id']); ?>
					</td>
					<td width="150"><?php echo anchor('admin/extension/detailorder/' . $row['id'], $row['fullname']); ?></td>
					<td width="100"><?php echo $row['mobile']; ?></td>
					<td width="200"><?php echo $row['address']; ?></td>
					<td width="100">
						<?php echo number_format($row['total'], 0, '', '.'); ?> (đ)
					</td>
					<td width="100"><?php echo $row['create_date']; ?></td>
					<td class="center" width="50">
						<?php
						if ($row['status'])
							echo '<span class="icon-ok"></span>';
						else
							echo anchor('admin/extension/process/' . $row['id'], '<span class="icon-remove"></span>');
						?>
					</td>
					<td class="center" width="20">
						<?php echo anchor('admin/extension/delete/' . $row['id'], '<span class="icon-trash"></span>', array('class' => 'deleterow', 'rel' => 'Đơn hàng #' . $row['id'])) ?>
					</td>
				</tr>
				<?php echo form_hidden('ID[]', $row['id']); ?>
			<?php endforeach; ?>

        </table>
        <input type="hidden" name="action" class="action" value="" />
		<?php echo form_close(); ?>
		<?php
		$this->pagination->initialize($config);
		echo $this->pagination->create_links();
		?>

		<?php $this->load->view('backend/footer'); ?>
    </div>
</div>