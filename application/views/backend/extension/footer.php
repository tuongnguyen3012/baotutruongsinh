<?php
/**
 * Description of edit
 * @author trungthuc
 * @date Feb 3, 2015
 */
?>
<?php $this->load->view('backend/breadcrumbs'); ?>
<div class="pageheader">
    <div class="pageicon"><span class="iconfa-sitemap"></span></div>
    <div class="pagetitle">
        <h1>Footer</h1>
    </div>
</div><!--pageheader-->
<div class="maincontent">
    <div class="maincontentinner">
		<?php echo form_open_multipart('', array('class' => 'objects stdform')); ?>

        <div class="widget">
            <h4 class="widgettitle">Module Hiển thị Footer</h4>
            <div class="widgetcontent">
				<p>
					<?php
					echo form_ckeditor(array(
						'name' => 'content',
						'id' => 'content',
						'width' => '100%',
						'height' => '300',
						'value' => $detail['content']
					));
					?>
				</p>
                <p>
					<button type="submit" class="btn btn-submit btn-primary btn-rounded"><i class="iconfa-save"></i> Lưu</button>
                </p>
            </div><!--widgetcontent-->
        </div>
		<?php echo form_close(); ?>
		<?php $this->load->view('backend/footer'); ?>
    </div>
</div>
