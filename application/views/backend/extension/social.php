<?php
/**
 * Description of edit
 * @author trungthuc
 * @date Feb 3, 2015
 */
?>
<?php $this->load->view('backend/breadcrumbs'); ?>
<div class="pageheader">
    <div class="pageicon"><span class="iconfa-sitemap"></span></div>
    <div class="pagetitle">
        <h1>Thời gian hiện popup</h1>
    </div>
</div><!--pageheader-->
<div class="maincontent">
    <div class="maincontentinner">
		<?php echo form_open_multipart('', array('class' => 'objects stdform')); ?>

        <div class="widget">
            <h4 class="widgettitle">Module Hiển thị Thời gian hiện popup</h4>
            <div class="widgetcontent">
				<p><label for="link">Thời gian hiện(giây)</label>
					<span class="field">
						<input type="text" name="content[time_show]" value="<?=isset($content['time_show']) ? $content['time_show']  : '' ?>" id="content" class="input-block-level" />
					</span>
				</p>
                <p><label for="link">Thời gian hiện sau khi tắt(giây)</label>
                    <span class="field">
                        <input type="text" name="content[time_show_after_off]" value="<?=isset($content['time_show_after_off']) ? $content['time_show_after_off']  : '' ?>" id="content" class="input-block-level" />
                    </span>
                </p>
                <p>
                    <span class="field">
                        <button type="submit" class="btn btn-submit btn-primary btn-rounded"><i class="iconfa-save"></i> Lưu</button>
                    </span>
                </p>
                
            </div><!--widgetcontent-->
        </div>
		<?php echo form_close(); ?>
		<?php $this->load->view('backend/footer'); ?>
    </div>
</div>
