<?php
/**
 * Description of admin
 * @author trungthuc
 * @date Jan 26, 2015
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title><?= $title ?></title>
        <?php echo link_tag('template/backend/css/style.default.css'); ?>
        <?php echo link_tag('template/backend/css/responsive-tables.css'); ?>
        <?php echo link_tag('template/backend/css/style.navyblue.css'); ?>
        <?php echo link_tag('template/backend/css/jquery.datetimepicker.css'); ?>
        <?php echo link_tag('template/backend/css/custom.css'); ?>
        <script type="text/javascript">
            var base_url = '<?php echo base_url(); ?>';
        </script>
        <?php echo script_tag('template/backend/js/jquery-1.9.1.min.js'); ?>
        <?php echo script_tag('template/backend/js/jquery-migrate-1.1.1.min.js'); ?>
        <?php echo script_tag('template/backend/js/jquery-ui-1.9.2.min.js'); ?>
        <?php echo script_tag('template/backend/js/bootstrap.min.js'); ?>
        <?php echo script_tag('template/backend/js/responsive-tables.js'); ?>
        <?php echo script_tag('template/backend/js/jquery.datetimepicker.js'); ?>
        <?php echo script_tag('template/backend/js/custom.js'); ?>
        <!--[if lte IE 8]><?php echo script_tag('template/backend/js/excanvas.min.js'); ?><![endif]-->

    </head>
    <body>
        <div class="mainwrapper">
            <?php $this->load->view('backend/header'); ?>
            <?php $this->load->view('backend/side_bar'); ?>
            <div class="rightpanel">
                <?php $this->load->view('backend/' . $this->controller . '/' . $this->action); ?>
            </div><!--rightpanel-->
        </div><!--mainwrapper-->
    </body>
</html>
