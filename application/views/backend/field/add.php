<?php
/**
 * Description of add
 * @author trungthuc
 * @date Jan 29, 2015
 */
?>
<script type="text/javascript">
    var object_id = '<?php echo $object['id']; ?>';
</script>
<?php $this->load->view('backend/breadcrumbs'); ?>
<div class="pageheader">
    <form action="results.html" method="post" class="searchbar">
        <input type="text" name="keyword" placeholder="To search type and hit enter..." />
    </form>
    <div class="pageicon"><span class="fa fa-sitemap"></span></div>
    <div class="pagetitle">
        <h5>Tạo Field</h5>
        <h1><?php echo $title ?></h1>
    </div>
</div><!--pageheader-->
<div class="maincontent">
    <div class="maincontentinner">
        <?php echo form_open_multipart('', array('class' => 'objects stdform')); ?>

        <div class="widget">
            <h4 class="widgettitle">Add Field</h4>
            <div class="widgetcontent">
                <p>
                    <?php echo form_label('Trạng thái', 'status') ?>
                    <span class="field">
                        <?php echo form_checkbox('status', 1, TRUE); ?>
                    </span>
                </p>
                <p>
                    <?php echo form_label('Tiêu đề Field', 'label') ?>
                    <span class="field">
                        <?php
                        echo form_input(array(
                            'name' => 'label',
                            'id' => 'label',
                            'class' => 'input-block-level',
                            'placeholder' => 'Field label'
                        ));
                        ?>
                    </span>
                </p>
                <p>
                    <?php echo form_label('Tên Field (*)', 'name') ?>
                    <span class="field">
                        <?php
                        echo form_input(array(
                            'name' => 'name',
                            'id' => 'name',
                            'class' => 'input-block-level',
                            'placeholder' => 'Field name'
                        ));
                        ?>
                    </span>
                </p>
                <p>
                    <?php echo form_label('Kiểu dữ liệu', 'type') ?>
                    <span class="field">
                        <?php echo form_dropdown('type', $type, '', 'class="uniformselect" id="data_type"'); ?>
                    </span>
                </p>
                <p class="html_type"></p>
                <p>
                    <label>Thông báo Alert</label>
                    <span class="field">
                        <?php
                        echo form_textarea(array(
                            'name' => 'alert',
                            'id' => 'alert',
                            'class' => 'span5',
                            'rows' => 5,
                            'placeholder' => 'Để trống nếu không cần thông báo xác nhận!'
                        ));
                        ?>
                    </span> 
                </p>
                <p>
                    <span class="field">
                        <button type="text" class="btn btn-primary btn-rounded"><i class="fa fa-save"></i> Lưu</button>
                        <button type="reset" class="btn btn-primary btn-rounded"><i class="fa fa-refresh"></i> Reset</button>
                        <?php echo anchor('admin/object', '<i class="fa fa-retweet"></i> Hủy', array('class' => 'btn btn-primary ')); ?>
                    </span>
                </p>
            </div><!--widgetcontent-->
        </div>
        <?php echo form_close(); ?>
        <?php $this->load->view('backend/footer'); ?>
    </div>
</div>