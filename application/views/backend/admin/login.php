<?php
/**
 * Description of login
 * @author trungthuc
 * @date Jan 26, 2015
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title><?= $title ?></title>
        <?php echo link_tag('template/backend/css/style.default.css'); ?>
        <?php echo link_tag('template/backend/css/style.navyblue.css'); ?>

        <?php echo script_tag('template/backend/js/jquery-1.9.1.min.js'); ?>
        <?php echo script_tag('template/backend/js/jquery-migrate-1.1.1.min.js'); ?>
        <?php echo script_tag('template/backend/js/jquery-ui-1.9.2.min.js'); ?>
        <?php //echo script_tag('template/backend/js/bootstrap.min.js'); ?>
        <?php //echo script_tag('template/backend/js/custom.js'); ?>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery('#login').submit(function () {
                    var u = jQuery('#username').val();
                    var p = jQuery('#password').val();
                    if (u == '' && p == '') {
                        jQuery('.login-alert').fadeIn();
                        return false;
                    }
                });
            });
        </script>
    </head>

    <body class="loginpage">

        <div class="loginpanel">
            <div class="loginpanelinner">
                <div class="logo animate0 bounceIn"><i style="font-size:100px;color:#fff;" class="fa fa-user-circle-o" aria-hidden="true"></i></div>
                <?php echo form_open('b_admin/login', array('id' => 'login')); ?>
                <div class="inputwrapper login-alert">
                    <div class="alert alert-error">Invalid username or password</div>
                </div>
                <div class="inputwrapper animate1 bounceIn">
                    <?php
                    echo form_input(array(
                        'name' => 'username',
                        'id' => 'username',
                        'placeholder' => 'Enter username',
                        'autofocus' => ''
                    ));
                    ?>
                </div>
                <div class="inputwrapper animate2 bounceIn">
                    <?php
                    echo form_input(array(
                        'type' => 'password',
                        'name' => 'password',
                        'id' => 'password',
                        'placeholder' => 'Enter password'
                    ));
                    ?>
                </div>
                <div class="inputwrapper animate3 bounceIn">
                    <button name="submit">Login</button>
                </div>
                <div class="inputwrapper animate4 bounceIn">
                    <label><input type="checkbox" class="remember" name="signin" checked="" /> Keep me sign in</label>
                </div>

                <?php echo form_close(); ?>
            </div><!--loginpanelinner-->
        </div><!--loginpanel-->

        <div class="loginfooter">
            <p>&copy; 2015. All Rights Reserved.</p>
        </div>

    </body>
</html>
