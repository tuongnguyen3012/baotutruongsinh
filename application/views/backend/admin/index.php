<?php
/**
 * Description of index
 * @author trungthuc
 * @date Jan 27, 2015
 */
?>
<?php $this->load->view('backend/breadcrumbs'); ?>

<div class="pageheader">
    <div class="pageicon"><span class="fa fa-laptop"></span></div>
    <div class="pagetitle">
        <h5>Thông tin quản trị</h5>
        <h1>Bảng điều khiển</h1>
    </div>
</div><!--pageheader-->

<div class="maincontent">
    <div class="maincontentinner">
        <?php if(isset($message) || $message != ''){ ?>
        <div class="alert alert-warning">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo $message; ?>
        </div> 
        <?php } ?>
        <div class="row-fluid">
            <div id="dashboard-left" class="span8">

                <ul class="shortcuts">
                    <li>
                        <!-- <a href="admin/record/index/4"> -->
                        <a href="/admin/record/index/8.html">
                            <span class="shortcuts-icon fa fa-credit-card fa-5x"></span>
                            <span class="shortcuts-label">QL đơn hàng</span>
                        </a>
                    </li>
                    <li>
                        <!-- <a href="admin/record/index/3"> -->
                        <a href="/admin/record/index/7.html">
                            <span class="shortcuts-icon fa fa-vcard fa-5x"></span>
                            <span class="shortcuts-label">QL đăng ký</span>
                        </a>
                    </li>
                    <li>
                        <!-- <a href="admin/record/index/2"> -->
                        <a href="/admin/record/index/6.html">
                            <span class="shortcuts-icon fa fa-newspaper-o fa-5x"></span>
                            <span class="shortcuts-label">QL bài viết</span>
                        </a>
                    </li>
                    <li>
                        <!-- <a href="admin/record/index/9"> -->
                        <a href="/admin/record/index/5.html">
                            <span class="shortcuts-icon fa fa-film fa-5x"></span>
                            <span class="shortcuts-label">QL danh mục</span>
                        </a>
                    </li>
                    <li>
                        <!-- <a href="admin/record/index/1"> -->
                        <a href="/admin/record/index/4.html">
                            <span class="shortcuts-icon fa fa-photo fa-5x"></span>
                            <span class="shortcuts-label">Slideshow</span>
                        </a>
                    </li>
                    <li>
                        <!-- <a href="admin/record/index/11"> -->
                        <a href="/admin/record/index/1.html">
                            <span class="shortcuts-icon fa fa-question-circle-o fa-5x"></span>
                            <span class="shortcuts-label">QL seo</span>
                        </a>
                    </li>
                     <li>
                        <!-- <a href="admin/record/index/12"> -->
                        <a href="">
                            <span class="shortcuts-icon fa fa-handshake-o fa-5x"></span>
                            <span class="shortcuts-label">Quảng cáo</span>
                        </a>
                    </li>
                    <li>
                        <!-- <a href="admin/record/index/14"> -->
                        <a href="">
                            <span class="shortcuts-icon fa fa-user-plus fa-5x"></span>
                            <span class="shortcuts-label">Tuyển dụng</span>
                        </a>
                    </li>
                </ul>

                <br />


            </div><!--span8-->

        </div><!--row-fluid-->


    </div><!--maincontentinner-->
</div><!--maincontent-->
<?php $this->load->view('backend/footer'); ?>