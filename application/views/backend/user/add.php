<?php
/**
 * Description of edit
 * @author trungthuc
 * @date Jan 27, 2015
 */
?>
<?php $this->load->view('backend/breadcrumbs'); ?>
<div class="pageheader">
    <form action="results.html" method="post" class="searchbar">
        <input type="text" name="keyword" placeholder="Tìm kiếm..." />
    </form>
    <div class="pageicon"><span class="fa fa-lock"></span></div>
    <div class="pagetitle">
        <h5>Thêm đối tượng Bảng</h5>
        <h1><?php echo $title ?></h1>
    </div>
</div><!--pageheader-->
<?php $roleId = $this->session->userdata('role_id');?>
<div class="maincontent">
    <div class="maincontentinner">
        <?php if(isset($message) || $message != ''){ ?>
        <div class="alert alert-warning">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo $message; ?>
        </div> 
        <?php } ?>
        <?php echo form_open_multipart('', array('class' => 'objects stdform')); ?>

        <div class="widget">
            <h4 class="widgettitle">Thêm tài khoản</h4>
            <div class="widgetcontent">
                <p>
                    <?php echo form_label('Tên thành viên', 'label') ?>
                    <span class="field">
                        <?php
                        echo form_input(array(
                            'name' => 'fullname',
                            'id' => 'fullname',
                            'class' => 'input-block-level',
                            'required' => true,
                            'placeholder' => 'Tên đầy đủ'
                        ));
                        ?>
                    </span>
                </p>
                <p>
                    <?php echo form_label('Tài khoản', 'label') ?>
                    <span class="field">
                        <?php
                        echo form_input(array(
                            'name' => 'username',
                            'id' => 'username',
                            'class' => 'input-block-level',
                            'required' => true,
                            'placeholder' => 'Tài khoản'
                        ));
                        ?>
                    </span>
                </p>
                <p>
                    <?php echo form_label('Mật khẩu', 'label') ?>
                    <span class="field">
                        <?php
                        echo form_password(array(
                            'name' => 'password',
                            'id' => 'password',
                            'class' => 'input-block-level',
                            'required' => true,
                            'placeholder' => 'Mật khẩu'
                        ));
                        ?>
                    </span>
                </p>
                <p>
                    <?php echo form_label('Nhóm', 'label') ?>
                    <span class="field">
                        <?php echo form_dropdown('role_id',$roleArray); ?>
                    </span>
                </p>
                <p>
                    <?php echo form_label('Email', 'label') ?>
                    <span class="field">
                        <?php
                        echo form_input(array(
                            'name' => 'email',
                            'id' => 'email',
                            'class' => 'input-block-level',
                            'required' => true,
                            'placeholder' => 'Email thành viên'
                        ));
                        ?>
                    </span>
                </p>
                <p>
                    <?php echo form_label('Số điện thoại', 'label') ?>
                    <span class="field">
                        <?php
                        echo form_input(array(
                            'name' => 'mobile',
                            'id' => 'mobile',
                            'class' => 'input-block-level',
                            'placeholder' => 'Số điện thoại'
                        ));
                        ?>
                    </span>
                </p>
                <p>
                    <?php echo form_label('Địa chỉ', 'label') ?>
                    <span class="field">
                        <?php
                        echo form_input(array(
                            'name' => 'address',
                            'id' => 'address',
                            'class' => 'input-block-level',
                            'required' => true,
                            'placeholder' => 'Địa chỉ'
                        ));
                        ?>
                    </span>
                </p>
                <p>
                    <span class="field">
                        <button type="text" class="btn btn-primary btn-rounded"><i class="fa fa-save"></i> Lưu</button>
<!--                        <button type="reset" class="btn btn-primary btn-rounded"><i class="fa fa-refresh"></i> Reset</button>-->
                        <?php echo anchor('admin/user', '<i class="fa fa-retweet"></i> Hủy', array('class' => 'btn btn-primary ')); ?>
                    </span>

                </p>

            </div><!--widgetcontent-->
        </div>


        <?php echo form_close(); ?>

        <?php $this->load->view('backend/footer'); ?>
    </div>
</div>