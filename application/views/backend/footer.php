<?php
/**
 * Description of footer
 * @author trungthuc
 * @date Jan 27, 2015
 */
?>
<div class="footer">
    <div class="footer-left">
        <span>&copy; <?php echo date('Y'); ?>. All Rights Reserved.</span>
    </div>
    <div class="footer-right">
        <span>Power by <?php echo anchor('http://ilovephpteam.vn', 'ILOVEPHP TEAM', array('target' => '_blank')) ?></span>
    </div>
</div><!--footer-->