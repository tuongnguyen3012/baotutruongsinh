<?php
/**
 * Description of edit
 * @author trungthuc
 * @date Jan 27, 2015
 */
?>
<?php $this->load->view('backend/breadcrumbs'); ?>
<div class="pageheader">
    <form action="results.html" method="post" class="searchbar">
        <input type="text" name="keyword" placeholder="To search type and hit enter..." />
    </form>
    <div class="pageicon"><span class="fa fa-lock"></span></div>
    <div class="pagetitle">
        <h5>Sửa đối tượng Table</h5>
        <h1><?php echo $title ?></h1>
    </div>
</div><!--pageheader-->

<div class="maincontent">
    <div class="maincontentinner">
        <?php echo form_open_multipart('', array('class' => 'objects stdform')); ?>

        <div class="widget">
            <h4 class="widgettitle">Add Object</h4>
            <div class="widgetcontent">

                <p>
                    <?php echo form_label('Tiêu đề Object', 'label') ?>
                    <span class="field">
                        <?php
                        echo form_input(array(
                            'name' => 'label',
                            'value' => $object['label'],
                            'id' => 'label',
                            'class' => 'input-block-level',
                            'placeholder' => 'Object label'
                        ));
                        ?>
                    </span>
                </p>
                <p>
                    <?php echo form_label('Tên Object (*)', 'name') ?>
                    <span class="field">
                        <?php
                        echo form_input(array(
                            'name' => 'name',
                            'value' => $object['name'],
                            'id' => 'name',
                            'class' => 'input-block-level',
                            'placeholder' => 'Object name'
                        ));
                        ?>
                    </span>
                </p>
                <p>
                    <?php echo form_label('Cấp Object', 'level') ?>
                    <span class="field">
                        <?php echo form_dropdown('level', $levels, $object['level'], 'class="uniformselect"'); ?>
                    </span>

                </p>

                <p>
                    <?php echo form_label('Phân quyền', 'role') ?>
                    <span class="field">
                        <?php echo form_multiselect('role[]', $role, json_decode($object['role']), 'class="uniformselect" size="10"'); ?>
                    </span>

                </p>
                <p>
                    <?php echo form_label('Thêm thẻ seo', 'add_seo') ?>
                    <span class="field">
                        <?php echo form_checkbox('add_seo', 1, $object['add_seo']); ?>
                    </span>

                </p>
                <p>
                    <?php echo form_label('Trạng thái', 'status') ?>
                    <span class="field">
                        <?php echo form_checkbox('status', 1, $object['status']); ?>
                    </span>

                </p>
                <p>
                    <span class="field">
                        <button type="text" class="btn btn-primary btn-rounded"><i class="fa fa-save"></i> Lưu</button>
                        <button type="reset" class="btn btn-primary btn-rounded"><i class="fa fa-refresh"></i> Reset</button>
                        <?php echo anchor('admin/object', '<i class="fa fa-retweet"></i> Hủy', array('class' => 'btn btn-primary ')); ?>
                    </span>

                </p>

            </div><!--widgetcontent-->
        </div>


        <?php echo form_close(); ?>

        <?php $this->load->view('backend/footer'); ?>
    </div>
</div>