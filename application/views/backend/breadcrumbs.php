<?php
/**
 * Description of breadcurm
 * @author trungthuc
 * @date Jan 27, 2015
 */
?>
<ul class="breadcrumbs">
    <li><?php echo anchor('admin', '<i class="fa fa-home"></i>') ?> <span class="separator"></span></li>
    <li><?php echo $title ?></li>
</ul>