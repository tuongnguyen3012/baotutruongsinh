<?php
/**
 * Description of index
 * @author trungthuc
 * @date Feb 5, 2015
 */
?>
<?php echo form_open('', array('class' => 'objects record')); ?>
<?php $this->load->view('backend/breadcrumbs'); ?>
<div class="pageheader">
    <div class="searchbar">
        <input type="text" name="keyword" placeholder="Tìm kiếm..." />
        <input type="submit" value="" style="position: absolute; left: -9999px" />
    </div>
    <div class="pageicon"><span class="fa fa-lock"></span></div>
    <div class="pagetitle">
        <h1><?php echo $title ?></h1>
    </div>
</div><!--pageheader-->

<div class="maincontent">
    <div class="maincontentinner">

        <?php echo anchor('admin/record/add/' . $object_id, '<i class="fa fa-plus"></i> Thêm mới', array('class' => 'btn btn-primary ')); ?>
        <button type="button" id="btn_update" class="btn btn-primary btn-rounded"><i class="fa fa-pencil"></i> Cập nhật</button>
        <button type="button" id="btn_delete" class="btn btn-danger btn-rounded"><i class="fa fa-remove"></i> Xóa</button>

        <table class="table table-bordered">
            <tr>
                <th class="head0">
                    <input type="checkbox" class="checkall" />
                </th>
                <th class="head1">ID</th>
                <th class="head0">Ord</th>
                <th class="head1">Name</th>
                <th class="head1">Status</th>
                <th class="head0"></th>
            </tr>

            <?php foreach ($rows as $row): ?>
                <tr class="gradeX">
                    <td class="aligncenter" width="10">
                        <span class="center">
                            <input type="checkbox" name="chk_<?php echo $row['id']; ?>" />
                        </span>
                    </td>
                    <td width="30"><?php echo $row['id']; ?></td>
                    <td width="20">
                        <?php echo form_input('ord_' . $row['id'], $row['ord'], 'class="input-small"'); ?>
                    </td>
                    <td><?php
                        echo anchor('admin/record/edit/' . $object_id . '/' . $page . '/' . $row['id'], $row['name']);
                        ?>
                    </td>
                    <td class="center" width="20">
                        <?php
                        if ($row['status'])
                            echo anchor('admin/record/disable/' . $object_id . '/' . $page . '/' . $row['id'], '<span class="icon-ok"></span>');
                        else
                            echo anchor('admin/record/enable/' . $object_id . '/' . $page . '/' . $row['id'], '<span class="icon-remove"></span>');
                        ?>
                    </td>
                    <td class="center" width="20">
                        <?php echo anchor('admin/record/delete/' . $object_id . '/' . $page . '/' . $row['id'], '<span class="icon-trash"></span>', array('class' => 'deleterow', 'rel' => $row['id'])) ?>
                    </td>
                </tr>
                <?php echo form_hidden('ID[]', $row['id']); ?>
            <?php endforeach; ?>

        </table>
        <input type="hidden" name="action" class="action" value="" />

        <?php
        if (isset($config) && $config):
            $this->pagination->initialize($config);
            echo $this->pagination->create_links();
        endif;
        ?>

        <?php $this->load->view('backend/footer'); ?>
    </div>
</div>

<?php echo form_close(); ?>