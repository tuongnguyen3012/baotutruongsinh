<?php
/**
 * Description of edit
 * @author trungthuc
 * @date Feb 3, 2015
 */
?>
<?php $this->load->view('backend/breadcrumbs'); ?>
<div class="pageheader">
    <form action="results.html" method="post" class="searchbar">
        <input type="text" name="keyword" placeholder="To search type and hit enter..." />
    </form>
    <div class="pageicon"><span class="fa fa-sitemap"></span></div>
    <div class="pagetitle">
        <h1><?php echo $title ?></h1>
    </div>
</div><!--pageheader-->
<div class="maincontent">
    <div class="maincontentinner">
        <?php echo form_open_multipart('', array('class' => 'objects stdform')); ?>

        <div class="widget">
            <h4 class="widgettitle">Edit Field</h4>
            <div class="widgetcontent">
                <?php
                $validate = '<script type="text/javascript">';
                $validate .= 'function check_form(){';
                foreach ($fields as $row):
                    $this->records->__parseForm($row, $record);
                    if ($row['alert']) {
                        $validate .="if(document.getElementById('" . $row['name'] . "').value==''){ alert('" . $row['alert'] . "');document.getElementById('" . $row['name'] . "').focus();return false;}";
                    }
                endforeach;
                $validate .= '}';
                $validate .= '</script>';
                ?>
                <p class="html_type"></p>
                <?php
                if (isset($parent) && $parent):
                    echo '<p>';
                    echo form_label('Level', 'parent');
                    echo '<span class="field">';
                    echo form_multiselect('parent_id', $parent, $record['parent_id'], '');
                    echo '</span></p>';
                endif;
                echo $validate;
                ?>
                <p>
                    <span class="field">
                        <button type="submit" onclick="return check_form()" class="btn btn-submit btn-primary btn-rounded"><i class="fa fa-save"></i> Lưu</button>
                        <button type="reset" class="btn btn-primary btn-rounded"><i class="fa fa-refresh"></i> Reset</button>
                        <?php echo anchor('admin/record/index/' . $object['id'], '<i class="fa fa-retweet"></i> Hủy', array('class' => 'btn btn-primary ')); ?>
                        <?php echo form_hidden('oID', $object['id']); ?>
                    </span>
                </p>
            </div><!--widgetcontent-->
        </div>
        <?php echo form_close(); ?>
        <?php $this->load->view('backend/footer'); ?>
    </div>
</div>
