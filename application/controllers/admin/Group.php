<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of setting
 * @author trungthuc
 * @date Jan 26, 2015
 */
class Group extends CI_Controller {

    public $per_page = 20;
    public $num_links = 4;
    public $uri_segment = 4;
	public $data;

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->library('javascript');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->model('adm');
        $this->load->model('groups');
        $this->load->model('gmail');
        $this->controller = $this->router->fetch_class();
        $this->action = $this->router->fetch_method();
        $this->view = 'backend/admin';
		$this->data['menu'] = $this->config->item('menu');
        if (!$this->session->userdata('user_id')) {
            redirect('admin');
        }
        $this->data['message'] = $this->session->flashdata('message');
    }

    public function index() {
        $groups = $this->groups->__getGroups();
        $config['total_rows'] = $this->groups->__totalGroups();
        $config['base_url'] = base_url() . 'admin/' . $this->controller . '/' . $this->action;
        $config['uri_segment'] = $this->uri_segment;
        $config['num_links'] = $this->num_links;
        $config['per_page'] = $this->per_page;
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['config'] = $config;
        $this->data['view'] = 'admin/group/';
        $this->data['title'] = 'Quản lý nhóm';
        $this->data['groups'] = $groups;
        if ($_POST && $_POST['action'] == 'delete') {
            foreach ($_POST['ID'] as $id) {
                if ($_POST['chk_' . $id] == 'on'){
                    $count_user = count($this->groups->__get_userByRole($id));
                    $group = $this->groups->__detailGroups($id);
                    $roleId = $this->session->userdata('role_id');
                    if($roleId == 1){
                        if($count_user > 0){
                            $this->session->set_flashdata('message', 'Vẫn còn thành viên trong nhóm <strong>'.$group['name'].'</strong>. Không thể xóa');
                            redirect('admin/group');
                        }elseif (in_array($id,[1,2])) {
                            $this->session->set_flashdata('message', 'Nhóm <strong>mặc định</strong> không thể xóa!');
                            redirect('admin/group');
                        }else{
                            $this->groups->__deleteGroups($id);
                            $this->session->set_flashdata('message', 'Đã xóa nhóm <strong>'.$group['name'].'</strong> thành công');
                            redirect('admin/group');
                        }
                    }else{
                        $this->session->set_flashdata('message', 'Không có quyền thực hiện chức năng này! Hãy liên hệ quản trị viên.');
                        redirect('admin/group');
                    }
                }      
            }
            redirect('admin/group');
        }
        $this->load->view($this->view, $this->data);
    }

    public function delete($id) {
        $count_user = count($this->groups->__get_userByRole($id));
        $group = $this->groups->__detailGroups($id);
        $roleId = $this->session->userdata('role_id');
        if($roleId == 1){
            if($count_user > 0){
                $this->session->set_flashdata('message', 'Vẫn còn thành viên trong nhóm <strong>'.$group['name'].'</strong>. Không thể xóa');
                redirect('admin/group');
            }elseif (in_array($id,[1,2])) {
                $this->session->set_flashdata('message', 'Nhóm <strong>mặc định</strong> không thể xóa!');
                redirect('admin/group');
            }else{
                $this->groups->__deleteGroups($id);
                $this->session->set_flashdata('message', 'Đã xóa nhóm <strong>'.$group['name'].'</strong> thành công');
                redirect('admin/group');
            }
        }else{
            $this->session->set_flashdata('message', 'Không có quyền thực hiện chức năng này! Hãy liên hệ quản trị viên.');
            redirect('admin/group');
        }
          
    }

    public function edit($id) {
        $group = $this->groups->__detailGroups($id);
        $roleId = $this->session->userdata('role_id');
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['view'] = 'admin/group/edit/';
        $this->data['title'] = 'Chỉnh sửa nhóm';
        $this->data['group'] = $group;
        if(in_array($roleId,[1,2])){
            if ($_POST) {
                $this->groups->__updateGroups($_POST['ID']);
                $this->session->set_flashdata('message', 'Đã cập nhật nhóm <strong>ID='.$group['id'].'</strong> thành công');
                redirect('admin/group/edit/'.$id);
            }
        }else{
            $this->session->set_flashdata('message', 'Không có quyền thực hiện chức năng này! Hãy liên hệ quản trị viên.');
            redirect('admin/group/edit/'.$id);
        }
        $this->load->view($this->view, $this->data);
    }

    public function add() {
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['title'] = "Thêm nhóm";
        $this->data['view'] = 'admin/group/add/';
        $roleId = $this->session->userdata('role_id');
        if(in_array($roleId,[1,2])){
            if ($_POST && $this->input->get_post('name')) {
                $groupId = $this->groups->__saveGroups();
                $this->session->set_flashdata('message', 'Đã thêm nhóm mới <strong>'.$this->input->get_post('username').'</strong>');
                redirect('admin/group');
            }
        }else{
            $this->session->set_flashdata('message', 'Không có quyền thực hiện chức năng này! Hãy liên hệ quản trị viên.');
            redirect('admin/group');
        }
        $this->load->view($this->view, $this->data);
    }

    public function disable($id) {
        //Update status
        if ($this->groups->__detailGroups($id)) {
            //Delete
            $this->groups->__updateObjects($id, 0);
        }
        redirect('admin/object');
    }

    public function enable($id) {
        //Update status
        if ($this->groups->__detailGroups($id)) {
            //Delete
            $this->groups->__updateObjects($id, 1);
        }
        redirect('admin/object');
    }

}
