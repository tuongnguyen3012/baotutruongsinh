<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of setting
 * @author trungthuc
 * @date Jan 26, 2015
 */
class User extends CI_Controller {

    public $per_page = 20;
    public $num_links = 4;
    public $uri_segment = 4;
	public $data;

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->library('javascript');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->library('form_validation');
        $this->load->model('adm');
        $this->load->model('users');
        $this->load->model('gmail');
        $this->controller = $this->router->fetch_class();
        $this->action = $this->router->fetch_method();
        $this->view = 'backend/admin';
		$this->data['menu'] = $this->config->item('menu');
        if (!$this->session->userdata('user_id')) {
            redirect('admin');
        }
        $this->data['message'] = $this->session->flashdata('message');
    }

    public function index() {
        $users = $this->users->__getUsers();
        $config['total_rows'] = $this->users->__totalUsers();
        $config['base_url'] = base_url() . 'admin/' . $this->controller . '/' . $this->action;
        $config['uri_segment'] = $this->uri_segment;
        $config['num_links'] = $this->num_links;
        $config['per_page'] = $this->per_page;
        $this->data['config'] = $config;
        $this->data['view'] = 'admin/user/';
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['title'] = 'Quản lý tài khoản';
        $this->data['users'] = $users;
        if ($_POST && $_POST['action'] == 'delete') {
            // echo "<pre>";
            // print_r($_POST);die();
            foreach ($_POST['ID'] as $id) {
                if ($_POST['chk_' . $id] && $_POST['chk_' . $id] == 'on'){
                    $user = $this->users->__detailUsers($id);
                    $roleId = $this->session->userdata('role_id');
                    $userId = $this->session->userdata('user_id');
                    if(in_array($roleId,[1,2]) || $userId == $id){
                        if($roleId >= 2 && $user['role_id'] == 1){
                            $this->session->set_flashdata('message', 'Không có quyền xóa thành viên cấp cao hơn. <strong>ID='.$id.'</strong>');
                            redirect('admin/user');
                        }elseif ($roleId == $user['role_id'] && $userId != $id) {
                            $this->session->set_flashdata('message', 'Không thể xóa thành viên cùng cấp với bạn. <strong>ID='.$id.'</strong>');
                            redirect('admin/user');
                        }elseif (in_array($userId,$_POST['ID'])) {
                            $this->users->__deleteUsers($id);
                            redirect('admin/logout');
                        }else{
                            $this->users->__deleteUsers($id);
                            $this->session->set_flashdata('message', 'Đã xóa thành viên <strong>'.$user['username'].'</strong>');
                            redirect('admin/user');
                        }   
                    }else{
                        $this->session->set_flashdata('message', 'Không có quyền thực hiện chức năng này! Hãy liên hệ quản trị viên.');
                    }
                }            
            }
            redirect('admin/user');
        }
        $this->load->view($this->view, $this->data);
    }

    public function delete($id) {
        $user = $this->users->__detailUsers($id);
        $roleId = $this->session->userdata('role_id');
        $userId = $this->session->userdata('user_id');
        if(in_array($roleId,[1,2])){
            if($roleId >= 2 && $user['role_id'] == 1){
                $this->session->set_flashdata('message', 'Không có quyền xóa thành viên cấp cao hơn.');
                redirect('admin/user');
            }elseif ($roleId == $user['role_id'] && $userId != $id) {
                $this->session->set_flashdata('message', 'Không thể xóa thành viên cùng cấp với bạn.');
                redirect('admin/user');
            }elseif ($userId == $id) {
                $this->users->__deleteUsers($id);
                redirect('admin/logout');
            }else{
                $this->users->__deleteUsers($id);
                $this->session->set_flashdata('message', 'Đã xóa thành viên <strong>'.$user['username'].'</strong>');
                redirect('admin/user');
            }   
        }else{
            $this->session->set_flashdata('message', 'Không có quyền thực hiện chức năng này! Hãy liên hệ quản trị viên.');
        }
    }

    public function edit($id) {
        $user = $this->users->__detailUsers($id);
        $roleId = $this->session->userdata('role_id');
        $userId = $this->session->userdata('user_id');
        $roleArray = $this->users->__getRoles();
        if($roleId == 1){
            $this->data['roleArray'] = $this->users->__getRoles();
        }elseif($roleId == 2){
            $this->data['roleArray'] = $this->users->__getRoles(array('id_other'=> [1]));
        }else{
            $this->data['roleArray'] = $this->users->__getRoles(array('id_other'=> [1,2]));
        }
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['view'] = 'admin/user/edit/';
        $this->data['title'] = 'Chỉnh sửa thông tin tài khoản';
        $this->data['user'] = $user;

        if($roleId > $user['role_id']){
            $this->session->set_flashdata('message', 'Không có quyền sửa thành viên cấp cao hơn.');
            redirect('admin/user');
        }elseif ($roleId == $user['role_id'] && $userId != $id) {
            $this->session->set_flashdata('message', 'Không có quyền sửa thành viên cùng cấp.');
            redirect('admin/user');
        }elseif ($roleId > 2 && $userId != $id) {
            $this->session->set_flashdata('message', 'Nhóm của bạn chỉ sửa được thông tin cá nhân mình.');
            redirect('admin/user');
        }else{
            if ($_POST) {
                // print_r($_POST);die();
                $this->users->__editUsers($_POST);
                $this->session->set_flashdata('message', 'Đã cập nhật thông tin thành công! <a href="admin/user">Trở lại</a>');
                redirect('admin/user/edit/'.$id); 
            }
        }   
        
        $this->load->view($this->view, $this->data);
    }
    
    public function add() {
        $roleId = $this->session->userdata('role_id');
        if($roleId == 1){
            $this->data['roleArray'] = $this->users->__getRoles();
        }elseif($roleId == 2){
            $this->data['roleArray'] = $this->users->__getRoles(array('id_other'=> [1]));
        }else{
            $this->data['roleArray'] = $this->users->__getRoles(array('id_other'=> [1,2]));
        }
        
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['title'] = "Thêm Tài Khoản";
        $this->data['view'] = 'admin/user/add/';
        if(!in_array($roleId,[1,2])){
            $this->session->set_flashdata('message', 'Không có quyền thực hiện chức năng này! Hãy liên hệ quản trị viên.');
            redirect('admin/user');
        }elseif($_POST && $this->input->get_post('username')){
            $this->form_validation->set_rules('username', 'Tài khoản', 'trim|required|is_unique[user.username]');
            if ($this->form_validation->run() == TRUE) {
                $userId = $this->users->__saveUsers();
                $this->session->set_flashdata('message', 'Đã thêm thành viên mới <strong>'.$this->input->get_post('username').'</strong>');
                redirect('admin/user');
            } else {
                $this->session->set_flashdata('message', 'Có lỗi khi thêm thành viên. Hãy thử lại!!!');
                redirect('admin/user/add');
            }        
        }
        $this->load->view($this->view, $this->data);
    }

}
