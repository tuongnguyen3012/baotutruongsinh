<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Description of extension
 * @author trungthuc
 * @date Jan 6, 2016
 */
class Extension extends CI_Controller {

	public $controller;
	public $action;
	public $view = 'backend/admin';
	public $per_page = 20;
	public $num_links = 4;
	public $uri_segment = 4;
	public $page = 0;
	public $levels = array(
		'0' => 'Đơn cấp',
		'1' => 'Đa cấp'
	);
	public $data;

	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('html');
		$this->load->helper('ckeditor');
		$this->load->library('javascript');
		$this->load->library('session');
		$this->load->library('pagination');
		$this->load->model('adm');
		$this->load->model('settings');
		$this->controller = $this->router->fetch_class();
		$this->action = $this->router->fetch_method();
		$this->view = 'backend/admin';
		$this->data['menu'] = $this->config->item('menu');
		if (!$this->session->userdata('user_id')) {
			redirect('admin');
		}
	}

	public function index() {
		$this->data['title'] = "Administrator";
		$this->load->view($this->view, $this->data);
	}

	public function order($page = null) {
		if ($_POST && $this->input->get_post('ID')) {

			foreach ($this->input->get_post('ID') as $row) {
				if ($this->input->get_post('action') == 'update') {
					//Update Status
					if ($this->input->get_post('chk_' . $row)) {
						$this->adm->__updateOrders($row);
					}
				}
				if ($this->input->get_post('action') == 'delete') {
					//Delete
					if ($this->input->get_post('chk_' . $row)) {
						//Delete data
						$this->adm->__deleteOrders($row);
					}
				}
			}
			//Delete object
		}
		$this->data['title'] = "Danh sách đơn đặt hàng";
		if ($page)
			$this->page = $page;
		$config['total_rows'] = $this->adm->__totalOrders();
		$config['base_url'] = base_url() . 'admin/' . $this->controller . '/' . $this->action;
		$config['uri_segment'] = $this->uri_segment;
		$config['num_links'] = $this->num_links;
		$config['per_page'] = $this->per_page;
		$this->data['config'] = $config;
		if (isset($_GET['status'])) {
			$status = $_GET['status'];
		} else {
			$status = NULL;
		}
		$this->data['rows'] = $this->adm->__getOrders($this->per_page, $this->page, $status);
		$this->load->view($this->view, $this->data);
	}

	public function delete($id) {
		if ($id) {
			//Delete data
			$this->adm->__deleteOrders($id);
		}
		redirect('admin/extension/order');
	}

	public function process($id) {
		//Update status
		if ($id) {
			//Delete
			$this->adm->__updateOrders($id);
		}
		redirect('admin/extension/order');
	}

	public function processorderdetail($id) {
		//Update status
		if ($id) {
			//Delete
			$this->adm->__updateOrders($id);
			redirect('admin/extension/detailorder/' . $id);
		} else {
			redirect('admin/extension/order');
		}
	}

	function detailorder($id) {
		$order = $this->adm->__detailOrder($id);
		$this->data['title'] = $order['fullname'];
		$this->data['role'] = $this->adm->__getRole();
		$this->data['levels'] = $this->levels;
		$this->data['order'] = $order;
		if ($_POST && $this->input->get_post('name')) {

			if ($object['name'] != $this->input->get_post('name')) {
				if (!$this->db->table_exists($this->input->get_post('name'))) {
					//Edit object
					$this->objects->__renameObject($object['name'], $this->input->get_post('name'));
				} else {
					redirect('admin/object/edit/' . $object_id);
				}
			}
			//Process data
			$this->objects->__editObject($object_id);
			redirect('admin/extension/order');
		}

		$orders = json_decode($order['orders']);
		if (!empty($orders)) {
			$ids = array();
			foreach ($orders as $key => $row) {
				$ids[] = $key;
			}

			$products = $this->adm->__cart_product($ids);
			$this->data['orders'] = $orders;
			$this->data['products'] = $products;
		}

		$this->load->view($this->view, $this->data);
	}

	public function contact($page = null) {
		$this->data['title'] = "Danh sách Liên hệ";
		if ($page)
			$this->page = $page;
		if ($_POST && $this->input->get_post('ID')) {

			foreach ($this->input->get_post('ID') as $row) {
				if ($this->input->get_post('action') == 'delete') {
					//Delete
					if ($this->input->get_post('chk_' . $row)) {
						//Delete data
						$this->adm->__deleteContacts($row);
					}
				}
			}
			//Delete object
		}
		$config['total_rows'] = $this->adm->__totalContacts();
		$config['base_url'] = base_url() . 'admin/' . $this->controller . '/' . $this->action;
		$config['uri_segment'] = $this->uri_segment;
		$config['num_links'] = $this->num_links;
		$config['per_page'] = $this->per_page;
		$this->data['config'] = $config;
		$this->data['rows'] = $this->adm->__getContacts($this->per_page, $this->page);
		$this->load->view($this->view, $this->data);
	}

	public function deletecontact($id) {
		if ($id) {
			//Delete data
			$this->adm->__deleteContacts($id);
		}
		redirect('admin/extension/contact');
	}

	public function logo() {
		$this->data['title'] = "Administrator";
		if ($_POST) {
			$this->settings->_save_logo();
		}
		$this->data['detail'] = $this->settings->__get_extension('logo');

		$this->load->view($this->view, $this->data);
	}
	
	public function hotline() {
		$this->data['title'] = "Administrator";
		if ($_POST) {
			$this->settings->_save_hotline();
		}
		$this->data['detail'] = $this->settings->__get_extension('hotline');

		$this->load->view($this->view, $this->data);
	}
	
	public function email() {
		$this->data['title'] = "Administrator";
		if ($_POST) {
			$this->settings->_save_email();
		}
		$this->data['detail'] = $this->settings->__get_extension('email');

		$this->load->view($this->view, $this->data);
	}
	
	public function footer() {
		$this->data['title'] = "Administrator";
		if ($_POST) {
			$this->settings->_save_footer();
		}
		$this->data['detail'] = $this->settings->__get_extension('footer');

		$this->load->view($this->view, $this->data);
	}
	
	public function contacts() {
		$this->data['title'] = "Administrator";
		if ($_POST) {
			$this->settings->_save_contacts();
		}
		$this->data['detail'] = $this->settings->__get_extension('contacts');

		$this->load->view($this->view, $this->data);
	}

	public function maps() {
		$this->data['title'] = "Administrator";
		if ($_POST) {
			$this->settings->_save_maps();
		}
		$this->data['detail'] = $this->settings->__get_extension('maps');

		$this->load->view($this->view, $this->data);
	}

	public function adv() {
		$this->data['title'] = "Administrator";
		if ($_POST) {
			$this->settings->_save_adv();
		}
		$this->data['detail'] = $this->settings->__get_extension('adv');

		$this->load->view($this->view, $this->data);
	}

	public function social()
	{
		$data['title'] = 'Administrator';
		if($_POST)
		{
			$this->settings->_save_social();
		}
		$data['detail'] = $this->settings->__get_extension('social');
		$data['content'] = json_decode($data['detail']['content'], true);
		$this->load->view($this->view, $data);
	}
	
	public function socials()
	{
		$data['title'] = 'Administrator';
		if($_POST)
		{
			$this->settings->_save_socials();
		}
		$data['detail'] = $this->settings->__get_extension('socials');
		$data['content'] = json_decode($data['detail']['content'], true);
		$this->load->view($this->view, $data);
	}
	public function website() {
        $this->data['title'] = "Administrator";
        if ($_POST) {
            $this->settings->_save_website();
        }
        $this->data['detail'] = $this->settings->__get_extension('website');

        $this->load->view($this->view, $this->data);
    }
}
