<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of lang
 * @author trungthuc
 * @date Jul 25, 2015
 */
class Lang extends CI_Controller {

    public $per_page = 20;
    public $num_links = 4;
    public $uri_segment = 4;
    public $page = 0;
	public $data;

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->library('javascript');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->model('adm');
        $this->load->model('users');
        $this->load->model('gmail');
        $this->controller = $this->router->fetch_class();
        $this->action = $this->router->fetch_method();
        $this->view = 'backend/admin';
		$this->data['menu'] = $this->config->item('menu');
        if (!$this->session->userdata('user_id')) {
            redirect('admin');
        }
    }

    public function update($lang) {
        if ($lang) {
            $this->session->set_userdata('lang', $lang);
        }
        redirect('admin');
        exit();
    }

    public function index($page = null) {
        if ($page)
            $this->page = $page;
        $config['total_rows'] = $this->adm->__totalTranslates();
        $config['base_url'] = base_url() . 'admin/' . $this->controller . '/' . $this->action;
        $config['uri_segment'] = $this->uri_segment;
        $config['num_links'] = $this->num_links;
        $config['per_page'] = $this->per_page;
        $this->data['config'] = $config;
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['view'] = 'admin/lang/';
        $this->data['title'] = 'Quản lý Translate';
        $this->data['translate'] = $this->adm->__getTranslates($this->per_page, $this->page);
        if ($_POST && $_POST['action'] == 'delete') {
            foreach ($_POST['ID'] as $id) {
                if ($_POST['chk_' . $id] == 'on')
                    $this->adm->__deleteTranslates($id);
            }
            redirect('admin/lang');
        }
        $this->load->view($this->view, $this->data);
    }

    public function delete($id) {
        $this->adm->__deleteTranslates($id);
        redirect('admin/lang');
    }

    public function edit($id) {
        $translate = $this->adm->__detailTranslates($id);
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['view'] = 'admin/lang/edit/';
        $this->data['title'] = 'Chỉnh sửa từ ' . $translate['name_vi'];
        $this->data['translate'] = $translate;
        if ($_POST) {
            $this->adm->__editTranslates($_POST['ID']);
            redirect('admin/lang');
        }
        $this->load->view($this->view, $this->data);
    }

    public function add() {
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['title'] = "Thêm Translate";
        $this->data['view'] = 'admin/lang/add/';
        if ($_POST && $this->input->get_post('name_vi')) {
            $this->adm->__saveTranslates();
            redirect('admin/lang');
        }
        $this->load->view($this->view, $this->data);
    }

}
