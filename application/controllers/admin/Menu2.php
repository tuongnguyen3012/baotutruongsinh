<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Description of setting
 * @author trungthuc
 * @date Jan 26, 2015
 */
class Menu extends CI_Controller {

	public $per_page = 20;
	public $num_links = 4;
	public $uri_segment = 4;
	public $data;

	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('html');
		$this->load->library('javascript');
		$this->load->library('session');
		$this->load->library('pagination');
		$this->load->model('adm');
		$this->load->model('users');
		$this->load->model('gmail');
		$this->load->model('menus');
		$this->controller = $this->router->fetch_class();
		$this->action = $this->router->fetch_method();
		$this->view = 'backend/admin';
		$this->data['menu'] = $this->config->item('menu');
		if (!$this->session->userdata('user_id')) {
			redirect('admin');
		}
	}
	
	function index($id) {
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['title'] = $this->data['menu'][$id];
		$this->data['id'] = $id;
		$this->data['menus'] = $this->menus->__menu(0, "", $id);
        $this->load->view($this->view, $this->data);
		$a = $this->menus->__menu(0, "", $id);
    }
	
	public function add($id) {
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['title'] = $this->data['menu'][$id];
        $this->data['view'] = 'admin/user/add/';
		$this->data['menus'] = $this->menus->__dataMenu(0, "", $id);
		$this->data['type'] = $this->config->item('type');
		$this->data['objects'] = $this->menus->__getObject();
		
		
        if ($_POST && $this->input->get_post('name')) {
                $this->menus->__saveMenu($id);
                redirect('admin/menu/index/'.$id);
        }
        $this->load->view($this->view, $this->data);

    }
	
	public function disable($object_id, $page, $id) {
        $object = $this->objects->__detailObjects($object_id);
        //Update status
        if ($object) {
            //Delete
            $this->records->__updateRecords($object['name'], $id, 0);
        }
        redirect('admin/record/index/' . $object_id . '/' . $page);
    }

    public function enable($object_id, $page, $id) {
        $object = $this->objects->__detailObjects($object_id);
        //Update status
        if ($object) {
            //Delete
            $this->records->__updateRecords($object['name'], $id, 1);
        }
        redirect('admin/record/index/' . $object_id . '/' . $page);
    }

}
