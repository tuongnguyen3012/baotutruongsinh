<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Description of ajax
 * @author trungthuc
 * @date Jan 29, 2015
 */
class Type extends CI_Controller {

	public $controller;
	public $action;
	public $view;
	public $field = 'field';
	public $object = 'object';
	public $data;

	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('html');
		$this->load->library('javascript');
		$this->load->library('session');
		$this->load->library('pagination');
		$this->load->library('upload');
		$this->load->model('adm');
		$this->load->model('objects');
		$this->load->model('records');
		$this->load->model('img');
		$this->controller = $this->router->fetch_class();
		$this->action = $this->router->fetch_method();
		$this->data['menu'] = $this->config->item('menu');
		$this->view = 'backend/' . $this->controller . '/' . $this->action;
		if (!$this->session->userdata('user_id')) {
			redirect('admin');
		}
	}

	function object() {
		$type = $this->input->get_post('type');
		$object_id = $this->input->get_post('object_id');

		$arr = array();
		$this->db->select('*');
		$this->db->where_not_in('id', $object_id);
		$query = $this->db->get('object');
		foreach ($query->result_array() as $row) {
			$arr[$row['id']] = $row['label'];
		}
		$this->data['type'] = $type;
		$this->data['object_id'] = $object_id;
		$this->data['data'] = $arr;
		$query->free_result();
		$this->load->view($this->view, $this->data);
	}

	function image() {
		$type = $this->input->get_post('type');
		$object_id = $this->input->get_post('object_id');
		$type_select = $this->input->get_post('type_select');
		if ($type == 15) {
			$arr = array();
			$this->db->select('*');
			$this->db->where('object_id', $object_id);
			$this->db->where('type', 14);
			$query = $this->db->get('field');
			foreach ($query->result_array() as $row) {
				$arr[$row['id']] = $row['label'];
			}
			$query->free_result();
			$this->data['data'] = $arr;
		}
		$this->data['type'] = $type;
		$this->data['object_id'] = $object_id;
		$this->data['type_select'] = $type_select;

		$this->load->view($this->view, $this->data);
	}

	/*
	 * Upload a File
	 */

	function upload() {
		$this->load->helper('url');
		$config['upload_path'] = './images/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$this->upload->initialize($config);

		if ($this->upload->do_upload('file')) {

			$this->data = $this->upload->data();
			if (isset($_POST['file_id']) && $_POST['file_id']) {
				//Update
				$file_id = $_POST['file_id'];
				$file_detail = $this->__detailTmp($file_id);
				//Delete file
				if (file_exists('./' . $file_detail['file_path']) && $file_detail['file_path']):
					unlink('./' . $file_detail['file_path']);
				endif;
				//Update file
				$this->records->__updateFile($this->data, $file_id);
			} else {
				//Create new
				$file_id = $this->records->__saveFile($this->data);
			}

			//Xu ly tao Thumb neu la Image
			if ($this->data['is_image']):
				$field_id = $this->input->get_post('field_id');
				$detail = $this->__detailField($field_id);
				if ($detail['process']):
					switch ($detail['process']) {
						case 1:
							$this->__cropImage($detail['width'], $detail['height'], $this->data);
							break;
						case 2:
							$this->__resizeImage($detail['width'], $detail['height'], $this->data);
							break;
						default:
							break;
					}
				endif;
			endif;
			$html = '<img class="thumbs" src="' . base_url() . 'images/' . $this->data['file_name'] . '" />';
			$json = array(
				'html' => $html,
				'value' => $file_id
			);
		} else {
			$error = $this->upload->display_errors();
			$json = array(
				'html' => strip_tags($error),
				'value' => null
			);
		}
		echo json_encode($json);
		exit();
	}

	/*
	 * Upload multi File
	 */

	function multiupload() {
		//Xu ly upload
		$path = './images/';
		$this->upload->initialize(array(
			"upload_path" => $path,
			"allowed_types" => "*"
		));
		//Xu ly upload multi
		$keys = array_keys($_FILES);
		foreach ($keys as $key):
			if ($this->upload->do_multi_upload($key)) {
				//get info key
				$detail_key = $this->fields->__infoKeybyName($object_id, $key);
				$uploads = $this->upload->get_multi_upload_data();
				//Save upload
				foreach ($uploads as $row):
					//Save file
					$file_id = $this->records->__saveFile($row);
					//Save mapper
					$this->records->__saveMapper($file_id, $record_id, $detail_key);
					//Crop

				endforeach;
				//
			}
		endforeach;
	}

	function delete() {
		$tmp_id = $this->input->get_post('file_id');
		if ($tmp_id):
			$detail = $this->__detailTmp($tmp_id);
			$field_id = $this->input->get_post('field_id');
			$field = $this->__fieldDetail($field_id);
			print_r($detail['file_path']);die();
			//Delete file
			if (file_exists('./' . $detail['file_path']) && $detail['file_path']):
				unlink('./' . $detail['file_path']);
			endif;

			//Delete thumb
			if ($field['process']):
				$thumb = $field['width'] . '_' . $field['height'] . '_' . $detail['file_name'];
				if (file_exists('./_thumbs/' . $thumb) && $detail['file_name']):
					unlink('./_thumbs/' . $thumb);
				endif;
			endif;
			//Delete data
			$this->db->where('id', $tmp_id);
			$this->db->delete('file');
			$mapper_id = $this->input->get_post('mapper_id');
			if ($mapper_id):
				$this->db->where('id', $mapper_id);
				$this->db->delete('file_mapper');
			endif;
		endif;
		exit();
	}

	function __detailTmp($tmp_id) {
		$this->db->select('*');
		$this->db->where('id', $tmp_id);
		$query = $this->db->get('file');
		foreach ($query->result_array() as $row)
			return $row;
		$query->free_result();
	}

	/*
	 * Single file
	 */

	function upload_single() {

		$this->load->helper('url');
		$type = $this->input->get_post('type');
		if ($type == 14):
			//Image
			$config['upload_path'] = './images/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
		elseif($type == 16):
			//Media
			$config['upload_path'] = './media/';
			$config['allowed_types'] = 'mp3|mp4|mov|flv';
		else:
			//File
			$config['upload_path'] = './files/';
			$config['allowed_types'] = 'zip|rar|doc|docx|xls|xlsx|csv|pdf';

		endif;
		$this->upload->initialize($config);
		if ($this->upload->do_upload('file')) {
			$this->data = $this->upload->data();
			$arr_thumb = array();
			//Xu ly tao Thumb neu la Image
			if ($this->data['is_image']):
				$field_id = $this->input->get_post('field_id');
				$details = $this->__ThumbField($field_id);
				if ($details):
					foreach ($details as $detail):
						switch ($detail['process']) {
							case 1:
								$arr_thumb[$detail['name']] = $this->__cropImage($detail['width'], $detail['height'], $this->data);
								break;
							case 2:
								$arr_thumb[$detail['name']] = $this->__resizeImage($detail['width'], $detail['height'], $this->data);
								break;
							default:
								break;
						}
					endforeach;
				endif;
			endif;
			if ($type == 14):
				//Image
				$file_name = 'images/' . $this->data['file_name'];
				$html = '<img class="thumbs" src="' . base_url() . 'images/' . $this->data['file_name'] . '" />';
			elseif($type == 16):
				//Media
				$html = '<b>' . $this->data['file_name'] . ' đã được tải lên.</b>';
				$file_name = 'media/' . $this->data['file_name'];
			else:
				//File
				$html = '<b>' . $this->data['file_name'] . ' đã được tải lên.</b>';
				$file_name = 'files/' . $this->data['file_name'];
			endif;
			$json = array(
				'html' => $html,
				'file_name' => $file_name,
				'arr_thumb' => $arr_thumb
			);
		} else {
			$error = $this->upload->display_errors();

			$json = array(
				'html' => strip_tags($error),
				'file_name' => null,
				'arr_thumb' => null
			);
		}
		echo json_encode($json);
		exit();
	}

	function __cropImage($width_thumb, $height_thumb, $file) {

		$image = './images/' . $file['file_name'];
		switch ($file['image_type']) {
			case 'jpg':
			case 'jpeg':
				$myImage = @imagecreatefromjpeg($image);
				break;
			case 'gif':
				$myImage = imagecreatefromgif($image);
				break;
			case 'png':
				$myImage = imagecreatefrompng($image);
				break;
			default:
				$myImage = imagecreatefrompng($image);
				break;
		}

		$x = $width_thumb;
		$y = $height_thumb;
		$ratio_thumb = $x / $y;

		list($xx, $yy) = getimagesize($image);
		$ratio_original = $xx / $yy;

		if ($ratio_original >= $ratio_thumb) {
			$yo = $yy;
			$xo = ceil(($yo * $x) / $y);
			$xo_ini = ceil(($xx - $xo) / 2);
			$xy_ini = 0;
		} else {
			$xo = $xx;
			$yo = ceil(($xo * $y) / $x);
			$xy_ini = ceil(($yy - $yo) / 2);
			$xo_ini = 0;
		}
		$myImageZoom = imagecreatetruecolor($width_thumb, $height_thumb);
		imagecopyresampled($myImageZoom, $myImage, 0, 0, $xo_ini, $xy_ini, $x, $y, $xo, $yo);

		$fileName = $x . '_' . $y . '_' . $file['file_name'];
		imagejpeg($myImageZoom, "./_thumbs/" . $fileName);
		return '_thumbs/' . $fileName;
	}

	function __resizeImage($width_thumb, $height_thumb, $file) {

		$image = './images/' . $file['file_name'];
		switch ($file['image_type']) {
			case 'jpg':
			case 'jpeg':
				$myImage = @imagecreatefromjpeg($image);
				break;
			case 'gif':
				$myImage = imagecreatefromgif($image);
				break;
			case 'png':
				$myImage = imagecreatefrompng($image);
				break;
			default:
				$myImage = imagecreatefrompng($image);
				break;
		}
		list($width_img, $height_img) = getimagesize($image);
		$myImageZoom = imagecreatetruecolor($width_thumb, $height_thumb);
		imagecopyresampled($myImageZoom, $myImage, 0, 0, 0, 0, $width_thumb, $height_thumb, $width_img, $height_img);
		$fileName = $width_thumb . '_' . $height_thumb . '_' . $file['file_name'];
		imagejpeg($myImageZoom, "./_thumbs/" . $fileName);
		return '_thumbs/' . $fileName;
	}

	function __detailField($field_id) {
		$this->db->select('*');
		$this->db->where('id', $field_id);
		$query = $this->db->get($this->field);
		if ($query->result_array()) {
			foreach ($query->result_array() as $result)
				return $result;
		} else {
			return false;
		}
		$query->free_result();
	}

	function __ThumbField($field_id) {
		$this->db->select('*');
		$this->db->where('parent_id', $field_id);
		$query = $this->db->get($this->field);
		return $query->result_array();
		$query->free_result();
	}

	function delete_single() {
		$file = $this->input->get_post('file');
		if ($file):
			//Empty db
			$record = $this->input->get_post('record');
			if ($record):
				$name = $this->input->get_post('name');
				$object = $this->input->get_post('object');
				$this->__emptyRecord($object, $name, $record);
			endif;
			//Delete file
			if (file_exists('./' . $file) && $file):
				unlink('./' . $file);
			endif;

		endif;
		exit();
	}

	function __emptyRecord($object_id, $name, $record_id) {
		$this->db->select('name');
		$this->db->where('id', $object_id);
		$result = $this->db->get($this->object);
		foreach ($result->result_array() as $row)
			$result->free_result();
		//Empty
		$this->db->where('id', $record_id);
		$this->db->update($row['name'], array(
			$name => ''
		));
	}

	function __fieldDetail($field_id) {
		$this->db->select('*');
		$this->db->where('id', $field_id);
		$result = $this->db->get($this->field);
		foreach ($result->result_array() as $row)
			return $row;
		$result->free_result();
	}

	function auto_generator() {
		$string = $this->input->get_post('string');
		$object_id = $this->input->get_post('object');

		$this->db->select('name');
		$this->db->where('id', $object_id);
		$result = $this->db->get($this->object);
		foreach ($result->result_array() as $row)
			$result->free_result();
		//
		$this->db->where('alias', $string);
		$this->db->from($row['name']);
		$count = $this->db->count_all_results();
		if ($count > 0) {
			$html = $string . '-' . $count;
		} else {
			$html = $string;
		}

		echo json_encode($html);
		exit();
	}

	function menu() {
		$type = $this->input->get_post('type');
		$return = array(
			'type' => $type
		);
		switch ($type) {
			case 1:


				break;
			case 2:
				$this->db->select(array('id', 'name', 'label'));
				$query = $this->db->get('page');
				$return['data'] = $query->result_array();
				$query->free_result();
				break;

			case 3:
				break;

			case 4:
			case 5:
				$this->db->select(array('id', 'name', 'label'));
				$query = $this->db->get('object');
				$return['data'] = $query->result_array();
				$query->free_result();
				break;

			default:
				$return['data'] = null;
				break;
		}
		echo json_encode($return);
		exit();
	}
	
	function menu_object() {
		$objectId = $this->input->get_post('object_id');
		$detailObject = $this->objects->__detailObjects($objectId);
		$rows = $this->menus->__getRowObject($detailObject['name']);
		
		$data = array(
			'object' => $detailObject,
			'data' => $rows
		);
		echo json_encode($data);
		
//		echo form_label('Tên danh mục', 'row_id');
//		echo '<span class="field">';
//		echo form_dropdown('row_id', $arr, '', 'class="uniformselect"');
//		echo '</span>';
//		echo '<input type="hidden" name="object_name" values="' . $detailObject['name'] . '" />';
		exit();
	}

	function menu_single_post() {
		$objectId = $this->input->get_post('object_id');
		$detailObject = $this->objects->__detailObjects($objectId);
		$rows = $this->menus->__getRowObject($detailObject['name']);
		$arr = array(
			0 => 'Chọn 1 bài viết'
		);
		foreach ($rows as $row) {
			$arr[$row['id']] = $row['name'];
		}
		echo form_label('Bài viết', 'row_id');
		echo '<span class="field">';
		echo form_dropdown('row_id', $arr, '', 'class="uniformselect"');
		echo '</span>';
		exit();
	}

	

}
