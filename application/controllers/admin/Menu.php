<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of setting
 * @author trungthuc
 * @date Jan 26, 2015
 */
class Menu extends CI_Controller {

    public $per_page = 20;
    public $view = 'backend/admin';
    public $num_links = 4;
    public $uri_segment = 4;
    public $data;

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->helper('ckeditor');
        $this->load->library('javascript');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->model('adm');
        $this->load->model('settings');
        $this->controller = $this->router->fetch_class();
        $this->action = $this->router->fetch_method();
        $this->view = 'backend/admin';
        $this->data['menu'] = $this->config->item('menu');
        if (!$this->session->userdata('user_id')) {
            redirect('admin');
        }
    }

    public function index($id = 1) {//defautl=1
        $this->data['title'] = "Quản Lý Menu";
        $this->data['type_id'] = $id;
        $menu_html = $this->genMenus(0, 0, $id);
        $this->data['menu_html'] = $menu_html;
        $cats = $this->db->where('status', 1)->get($this->db->dbprefix.'categories')->result_array();
        $categories[] = '--Chọn--';
        foreach($cats as $cat)
        {
            $categories[$cat['id']] = $cat['name'];
        }
        $this->data['categories'] = $categories;
        $news = $this->db->where('status', 1)->get($this->db->dbprefix.'news')->result_array();
        $baiviet[] = '--Chọn--';
        foreach($news as $new)
        {
            $baiviet[$new['id']] = $new['name'];
        }
        $this->data['news'] = $baiviet;
        $this->load->view($this->view, $this->data);
    }
    function genMenus($parent_id, $lever, $type_id)
    {
        $menu_html = '';
        $sql = "SELECT * FROM `".$this->db->dbprefix."menus` WHERE parent_id = ".$parent_id." AND type_id = ".$type_id." ORDER BY ord ASC";
        $menus = $this->db->query($sql)->result_array();
        if($menus)
        {
            if($parent_id == 0)
                $menu_html = '<ol class="sortable">';
            else
                $menu_html = '<ol>';
            foreach($menus as $menu)
            {
                $menu_html .= '<li data-id="'.$menu['id'].'" id="list_'.$menu['id'].'"><div><span class="disclose"><span></span></span>'.$menu['name'].'<span class="pull-right"><a data- class="editmenu" href="#">Sửa</a>|<a class="xoamenu" href="'.site_url('admin/menu/delete/'.$menu['id'].'/'.$menu['type_id']).'">Xóa</a></span></div>';
                $menu_html .= $this->genMenus($menu['id'], $menu['depth'], $type_id);
            }
            $menu_html .= '</ol>';
        }
        return $menu_html;
    }
    public function delete($id = null, $type_id = 1)
    {
        if($id)
        {
            $this->db->where('id', $id);
            $this->db->delete('menus');
        }
        redirect('admin/menu/'.$type_id);
    }
    public function update()
    {
        $menus = $this->input->get_post('menus');
        foreach($menus as $key => $menu)
        {
            if(isset($menu['id']))
            {
                $this->db->where('id', $menu['id']);
                unset($menu['id']);
                $menu['ord'] = $key;
                $this->db->update($this->db->dbprefix."menus", $menu);
            }
        }
    }
    public function get($id = null)
    {
        $this->db->where('id', $id);
        $menu = $this->db->get($this->db->dbprefix."menus")->row();
        echo json_encode($menu);
        exit();
    }
    public function save($type_id = null)
    {
        $data = array();
        $data['name'] = $this->input->get_post('name');
        $data['type_id'] = $type_id;
        $data['url'] = $this->input->get_post('url');
        $data['danhmuc_id'] = $this->input->get_post('danhmuc_id');
        $data['baiviet_id'] = $this->input->get_post('baiviet_id');
        if(empty($data['url']))
        {
            if(!empty($data['danhmuc_id'])&&empty($data['baiviet_id']))
            {
                $cate = $this->adm->__detail_categories($data['danhmuc_id']);
                $data['url'] = $cate['alias'];
            }
            else if(!empty($data['baiviet_id'])&&empty($data['danhmuc_id']))
            {
                $news = $this->adm->__detail_news($data['baiviet_id']);
                $data['url'] = $news['alias'];
            }
        }
        if(isset($_POST['id']) && $_POST['id'])
        {
            $data['date_updated'] = date('Y-m-d H:i:s');
            $this->db->where('id', $_POST['id']);
            $this->db->update('menus', $data);
        }
        else
        {
            $data['depth'] = 0;
            $data['left'] = 0;
            $data['right'] = 0;
            $data['parent_id'] = 0;
            $data['date_created'] = $data['date_updated'] = date('Y-m-d H:i:s');
            $this->db->insert('menus', $data);
        }
        redirect('admin/menu/'.$type_id);
    }

    public function detail_news($id)
    {
        $this->db->select('alias');
        $this->db->where(array('status'=>1, 'id'=>$id));
        $query = $this->db->get('news');
        echo json_encode($query->row_array());
        exit();
    }
    public function detail_category($id)
    {
        $this->db->select('alias');
        $this->db->where(array('status'=>1, 'id'=>$id));
        $query = $this->db->get('categories');
        echo json_encode($query->row_array());
        exit();
    }
}
