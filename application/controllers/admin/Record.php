<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of record
 * @author trungthuc
 * @date Jan 30, 2015
 */
class Record extends CI_Controller {

    public $controller;
    public $action;
    public $view = 'backend/admin';
    public $per_page = 30;
    public $num_links = 4;
    public $uri_segment = 5;
    public $page = 0;
	public $data;

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->library('javascript');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->model('adm');
        $this->load->model('objects');
        $this->load->model('fields');
        $this->load->model('records');
        $this->controller = $this->router->fetch_class();
        $this->action = $this->router->fetch_method();
		$this->data['menu'] = $this->config->item('menu');
        if (!$this->session->userdata('user_id')) {
            redirect('admin');
        }
    }

    public function index($object_id, $page = null) {
        $roleId = $this->session->userdata('role_id');
        $object = $this->objects->__detailObjects($object_id);
        if(!in_array($roleId, json_decode($object['role']))){
            $this->session->set_flashdata('message', 'Không có quyền truy cập chức năng này! Hãy liên hệ quản trị viên.');
            redirect('admin');
        }
        if ($_POST && $this->input->get_post('ID')) {

            foreach ($this->input->get_post('ID') as $row) {
                if ($this->input->get_post('action') == 'update') {
                    //Update Ord
                    if ($this->input->get_post('ord_' . $row)) {
                        $this->records->__updateOrdRecord($object['name'], $row, $this->input->get_post('ord_' . $row));
                    }
                }
                if ($this->input->get_post('action') == 'delete') {
                    //Delete
                    if ($this->input->get_post('chk_' . $row)) {
                        $this->__delete($object_id, $row);
                    }
                }
            }
            //Delete object
        }
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['title'] = $object['label'];
        if ($page)
            $this->page = $page;

        if ($object['level']):
            //Xu ly recurion
            $this->data['rows'] = $this->records->__getResurionRecords($object['name'], 0);
        else:
            $config['total_rows'] = $this->records->__totalRecords($object['name']);
            $config['base_url'] = base_url() . 'admin/' . $this->controller . '/' . $this->action . '/' . $object_id;
            $config['uri_segment'] = $this->uri_segment;
            $config['num_links'] = $this->num_links;
            $config['per_page'] = $this->per_page;
            $this->data['config'] = $config;
            $this->data['rows'] = $this->records->__getRecords($object['name'], $this->per_page, $this->page);
        endif;

        $this->data['object_id'] = $object_id;
        $this->data['object'] = $object;
        $this->data['page'] = ($page) ? $page : 0;
        $this->load->view($this->view, $this->data);
    }

    function add($object_id) {
        $roleId = $this->session->userdata('role_id');
        $object = $this->objects->__detailObjects($object_id);
        $fields = $this->fields->__getFieldObject($object_id);
        if(!in_array($roleId, json_decode($object['role']))){
            $this->session->set_flashdata('message', 'Không có quyền truy cập chức năng này! Hãy liên hệ quản trị viên.');
            redirect('admin');
        }
        if (isset($_POST) && $_POST) {
            //Save data simple fields 
            $record_id = $this->records->__saveRecord($object, $fields);
            redirect('admin/record/index/' . $object_id);
        }
        if ($object['level']):
            $levels = $this->records->__getLevelObject($object['name'], 0, '');
            $level = array();
            foreach ($levels as $row):
                $level[$row['id']] = $row['name'];
            endforeach;
            $this->data['parent'] = $level;
        endif;
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['title'] = 'Add ' . $object['label'];
        $this->data['object'] = $object;
        $this->data['fields'] = $this->records->__getAllField($object['id']);
		$this->data['object_id'] = $object_id;
        $this->load->view($this->view, $this->data);
    }

    function edit($object_id, $page, $record_id) {
        $roleId = $this->session->userdata('role_id');
        $object = $this->objects->__detailObjects($object_id);
        $fields = $this->fields->__getFieldObject($object_id);
        $record = $this->records->__detailRecord($object['name'], $record_id);

        if(!in_array($roleId, json_decode($object['role']))){
            $this->session->set_flashdata('message', 'Không có quyền truy cập chức năng này! Hãy liên hệ quản trị viên.');
            redirect('admin');
        }
        if (isset($_POST) && $_POST) {
            //kiểm tra checkbox không có thì đặt là 0
            foreach ($fields as $row){
                if($row['type'] == 9 && !isset($_POST[$row['name']])){
                    $_POST[$row['name']] = 0;
                }
            }
            //kiểm tra trạng thái không có thì đặt là 0
            if(!isset($_POST['status'])){
                $_POST['status'] = 0;
            }

            //Save data simple fields 
            $this->records->__updateRecord($object, $fields, $record_id);
            redirect('admin/record/index/' . $object_id . '/' . $page);
        }
        if ($object['level']):
            $levels = $this->records->__getLevelObject($object['name'], 0, '');
            $level = array();
            foreach ($levels as $row):
                $level[$row['id']] = $row['name'];
            endforeach;
            $this->data['parent'] = $level;
        endif;
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['record'] = $record;
        $this->data['title'] = 'Edit ' . $record['name'];
        $this->data['object'] = $object;
        $this->data['fields'] = $this->records->__getAllField($object_id);
		$this->data['object_id'] = $object_id;
        $this->load->view($this->view, $this->data);
    }

    public function delete($object_id, $page, $id) {
        $roleId = $this->session->userdata('role_id');
        $object = $this->objects->__detailObjects($object_id);
        $fields = $this->fields->__getFieldObject($object_id);
        if(!in_array($roleId, json_decode($object['role']))){
            $this->session->set_flashdata('message', 'Không có quyền truy cập chức năng này! Hãy liên hệ quản trị viên.');
            redirect('admin');
        }
        $this->__delete($object_id, $id);
        redirect('admin/record/index/' . $object_id . '/' . $page);
    }

    public function __delete($object_id, $record_id) {
        $roleId = $this->session->userdata('role_id');
        $object = $this->objects->__detailObjects($object_id);
        $fields = $this->fields->__getFieldObject($object_id);
        $record = $this->records->__detailRecord($object['name'], $record_id);
        if(!in_array($roleId, json_decode($object['role']))){
            $this->session->set_flashdata('message', 'Không có quyền truy cập chức năng này! Hãy liên hệ quản trị viên.');
            redirect('admin');
        }
        if ($object) {
            //Check image => Delete image
            foreach ($fields as $field):
                switch ($field['type']) {
                    case 17:
                        //Delete multi file
                        $this->datas = $this->records->__mapFile($record_id, $field['id']);
                        foreach ($this->datas as $row):
                            //Delete file upload
                            if (file_exists('./' . $row['file_path']) && $row['file_path']):
                                unlink('./' . $row['file_path']);
                            endif;
                            $this->db->where('id', $row['id']);
                            $this->db->delete('file');
                            //Delete thumb
                            if ($field['process']):
                                $thumb = $field['width'] . '_' . $field['height'] . '_' . $row['file_name'];
                                if (file_exists('./_thumbs/' . $thumb) && $row['file_name']):
                                    unlink('./_thumbs/' . $thumb);
                                endif;

                            endif;
                            //Delete file mapper.
                            $this->db->where('id', $row['mapper_id']);
                            $this->db->delete('file_mapper');

                        endforeach;

                        break;
                    case 13:
                    case 14:
                    case 15:
                        //Delete single file
                        if (file_exists('./' . $record[$field['name']]) && $record[$field['name']]):
                            unlink('./' . $record[$field['name']]);
                        endif;

                        break;

                    default:
                        break;
                }
            endforeach;
            //Delete data
            $this->records->__deleteRecords($object, $record);
        }
    }

    public function disable($object_id, $page, $id) {
        $object = $this->objects->__detailObjects($object_id);
        //Update status
        if ($object) {
            //Delete
            $this->records->__updateRecords($object['name'], $id, 0);
        }
        redirect('admin/record/index/' . $object_id . '/' . $page);
    }

    public function enable($object_id, $page, $id) {
        $object = $this->objects->__detailObjects($object_id);
        //Update status
        if ($object) {
            //Delete
            $this->records->__updateRecords($object['name'], $id, 1);
        }
        redirect('admin/record/index/' . $object_id . '/' . $page);
    }

}
