<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of object
 * @author trungthuc
 * @date Jan 27, 2015
 */
class Object extends CI_Controller {

    public $controller;
    public $action;
    public $view = 'backend/admin';
    public $per_page = 20;
    public $num_links = 4;
    public $uri_segment = 4;
    public $page = 0;
	public $data;
    public $levels = array(
        '0' => 'Đơn cấp',
        '1' => 'Đa cấp'
    );

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->library('javascript');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->model('adm');
        $this->load->model('objects');
        $this->load->model('fields');
        $this->controller = $this->router->fetch_class();
        $this->action = $this->router->fetch_method();
		$this->data['menu'] = $this->config->item('menu');
        if (!$this->session->userdata('user_id')) {
            redirect('admin');
        }
    }

    public function index($page = null) {
        if ($_POST && $this->input->get_post('ID')) {

            foreach ($this->input->get_post('ID') as $row) {
                if ($this->input->get_post('action') == 'update') {
                    //Update Ord
                    if ($this->input->get_post('ord_' . $row)) {
                        $this->objects->__updateOrdObject($row, $this->input->get_post('ord_' . $row));
                    }
                }
                if ($this->input->get_post('action') == 'delete') {
                    //Delete
                    if ($this->input->get_post('chk_' . $row)) {
                        $object = $this->objects->__detailObjects($row);
                        //Delete data
                        $this->objects->__deleteObjects($row);
                        //Delete Object
                        if ($this->db->table_exists($object['name'])) {
                            $this->load->dbforge();
                            $this->dbforge->drop_table($object['name']);
                        }
                        //Delete field
                        $this->fields->__deleteAllField($object['id']);
                    }
                }
            }
            //Delete object
        }
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['title'] = "Objects list";
        if ($page)
            $this->page = $page;
        $config['total_rows'] = $this->objects->__totalObjects();
        $config['base_url'] = base_url() . 'admin/' . $this->controller . '/' . $this->action;
        $config['uri_segment'] = $this->uri_segment;
        $config['num_links'] = $this->num_links;
        $config['per_page'] = $this->per_page;
        $this->data['config'] = $config;
        $this->data['rows'] = $this->objects->__getObjects($this->per_page, $this->page);
        $this->load->view($this->view, $this->data);
    }

    public function add() {
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['title'] = "Add Object";
        $this->data['role'] = $this->adm->__getRole();
        $this->data['levels'] = $this->levels;
        if ($_POST && $this->input->get_post('name')) {

            if (!$this->db->table_exists($this->input->get_post('name'))) {
                //Create data & insert
                $object_id = $this->objects->__saveObject();
                //Create object
                $this->objects->__createObject($this->input->get_post('name'), $this->input->get_post('add_seo'));//change
                //Save fields
                $this->fields->__createField($object_id, $this->input->get_post('add_seo'));
                // redirect('admin/object');
            }
        }
        $this->load->view($this->view, $this->data);
    }

    public function delete($id) {
        if ($this->objects->__detailObjects($id)) {
            $object = $this->objects->__detailObjects($id);
            //Delete data
            $this->objects->__deleteObjects($id);
            //Delete Object
            if ($this->db->table_exists($object['name'])) {
                $this->load->dbforge();
                $this->dbforge->drop_table($object['name']);
            }
            //Delete field
            $this->fields->__deleteAllField($object['id']);
        }
        redirect('admin/object');
    }

    public function disable($id) {
        //Update status
        if ($this->objects->__detailObjects($id)) {
            //Delete
            $this->objects->__updateObjects($id, 0);
        }
        redirect('admin/object');
    }

    public function enable($id) {
        //Update status
        if ($this->objects->__detailObjects($id)) {
            //Delete
            $this->objects->__updateObjects($id, 1);
        }
        redirect('admin/object');
    }

    function edit($object_id) {
        $object = $this->objects->__detailObjects($object_id);
        $this->data['lang'] = $this->session->userdata('lang');
        $this->data['title'] = $object['label'];
        $this->data['role'] = $this->adm->__getRole();
        $this->data['levels'] = $this->levels;
        $this->data['object'] = $object;
        if ($_POST && $this->input->get_post('name')) {

            if ($object['name'] != $this->input->get_post('name')) {
                if (!$this->db->table_exists($this->input->get_post('name'))) {
                    //Edit object
                    $this->objects->__renameObject($object['name'], $this->input->get_post('name'));
                } else {
                    redirect('admin/object/edit/' . $object_id);
                }
            }
            //Edit data
            $this->objects->__editObject($object_id);
            redirect('admin/object');
        }
        $this->load->view($this->view, $this->data);
    }

}
