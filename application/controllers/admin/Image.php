<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of image
 * @author trungthuc
 * @date Feb 24, 2015
 */
class Image extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('html');
        $this->load->library('javascript');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->model('adm');
        $this->load->model('objects');
        $this->load->model('fields');
        $this->controller = $this->router->fetch_class();
        $this->action = $this->router->fetch_method();
        if (!$this->session->userdata('user_id')) {
            redirect('admin');
        }
    }

    function crop() {
        $width_thumb = '2100';
        $height_thumb = '100';

        $name = "./images/a.jpg";
        $myImage = imagecreatefromjpeg($name);

        $x = $width_thumb;
        $y = $height_thumb;
        $ratio_thumb = $x / $y;

        list($xx, $yy) = getimagesize($name);
        $ratio_original = $xx / $yy;

        if ($ratio_original >= $ratio_thumb) {
            $yo = $yy;
            $xo = ceil(($yo * $x) / $y);
            $xo_ini = ceil(($xx - $xo) / 2);
            $xy_ini = 0;
        } else {
            $xo = $xx;
            $yo = ceil(($xo * $y) / $x);
            $xy_ini = ceil(($yy - $yo) / 2);
            $xo_ini = 0;
        }
        $myImageZoom = imagecreatetruecolor($width_thumb, $height_thumb);
        imagecopyresampled($myImageZoom, $myImage, 0, 0, $xo_ini, $xy_ini, $x, $y, $xo, $yo);
        
        $fileName = "img" . $x . '_' . $y;
        imagejpeg($myImageZoom, "./images/" . $fileName . "_crop.jpg");

        exit();
    }

    function resize() {

        $width_thumb = '500';
        $height_thumb = '500';
        $name = "./images/a.jpg";
        $myImage = imagecreatefromjpeg($name);
        list($width_img, $height_img) = getimagesize($name);
        $myImageZoom = imagecreatetruecolor($width_thumb, $height_thumb);
        imagecopyresampled($myImageZoom, $myImage, 0, 0, 0, 0, $width_thumb, $height_thumb, $width_img, $height_img);
        $fileName = "img";
        imagejpeg($myImageZoom, "./images/" . $fileName . "_resize.jpg");

        exit();
    }

}
