<?php

class Bluesoft_c extends CI_Controller {

    public $data = [];
    public $layout = 'layout';
    public $controller;
    public $action;
    public $per_page = 5;
    public $num_links = 1;
    public $uri_segment = 5;
    public $page = 0;
    public function __construct() {
        parent::__construct();
        $this->load->model('Bluesoft_m');
        $this->load->model('Gmail');
        $this->load->library('smartie');
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->helper('text');
        $this->load->helper('support');
        $this->load->library('session');
        $this->load->library('pagination');
        $this->load->library('user_agent');
        $this->load->library('table');
        $this->load->library('cart');
        $this->config->load('conf');
        $this->controller = $this->router->fetch_class();
        $this->action = $this->router->fetch_method();
        $this->data['view'] = $this->router->fetch_method();
        $this->data['ext'] = $this->Bluesoft_m->__get_extension();

        $seo = $this->Bluesoft_m->__get_seo();
        $this->data['seo'] = $seo;
        $this->data['favicon'] = $seo['favicon'];
        $this->data['meta_description'] = $seo['meta_description'];
        $this->data['meta_keyword'] = $seo['meta_keyword'];
        $this->data['meta_title'] = $seo['meta_title'];

        $this->data['social'] = json_decode($this->data['ext']['social']['content'], true);
        $this->data['socials'] = json_decode($this->data['ext']['socials']['content'], true);
        $this->data['h_menus'] = $this->Bluesoft_m->__get_menus(0, 1);
        $this->data['v_menus'] = $this->Bluesoft_m->__get_menus(0, 2);
        $this->data['list_news'] = $this->Bluesoft_m->__get_news(null, null, 5);

        $this->data['time_show_modal'] = 3000;
        $this->data['time_show_modal_after_off'] = 10000;
        if($this->data['social']['time_show'])
            $this->data['time_show_modal'] = $this->data['social']['time_show'] * 1000;
        
        if($this->data['social']['time_show_after_off'])
            $this->data['time_show_modal_after_off'] = $this->data['social']['time_show_after_off'] * 1000;

        
        $this->data['this'] = $this;
    }

    public function index() {
        $this->data['nav'] = 0;
        $this->data['sliders'] = $this->Bluesoft_m->__slider(3, 0);
        $this->data['outstanding_characteristics'] = $this->Bluesoft_m->__get_news(2, 5);
        $this->data['cate_outstanding_characteristics'] = $this->Bluesoft_m->__detail_category_id(2);
        $this->data['certifies'] = $this->Bluesoft_m->__get_news(3, 3);
        $this->data['product'] = $this->Bluesoft_m->__get_news(1, 1)[0];
        $this->data['feels'] = $this->Bluesoft_m->__get_news(4, 5);
        $this->data['view'] = 'home';
        $this->smarty->view($this->layout, $this->data);
    }
    //error
    public function error404()
    {
        $this->view = 'error404';
        $this->smarty->view($this->view, $this->data);
    }
    public function testMail()
    {
        $this->Gmail->send_mail('tuongnguyen3012@gmail.com', 'tuongnguyen3012@gmail.com', 'tuongnguyen3012@gmail.com', 'subject', 'body');
    }
    public function sendMailAjax()
    {
        if($_POST)
        {
            $data = array();
            $to =  $this->data['ext']['email']['content'];
            $from =  $this->data['ext']['email']['content'];
            $from_name = $this->config->item('website_name');
            $subject = 'Đăng ký tư vấn';
            $body = '<p>Thông tin người đăng ký : '.$this->input->get_post('your_name').'</p>';
            if($this->input->get_post('question')&&$this->input->get_post('question')!="")
            {
                $body .= '<p>Tình trạng bệnh : '.$this->input->get_post('question').'</p>';
                $data['question'] = $this->input->get_post('question');
            }
            $body .= '<p>Số điện thoại : '.$this->input->get_post('your_phone').'</p>';
            //save data
            $data['name'] = $data['full_name'] = $this->input->get_post('your_name');
            $data['phone'] = $this->input->get_post('your_phone');
            $this->Bluesoft_m->__save_advisory($data);
            $this->session->set_userdata(base64_encode('is_modal'), 1);
            //send mail
            if(!empty($to))
            {
                $this->Gmail->send_mail($to, $from, $from_name, $subject, $body);
            }
            echo json_encode(array('message'=>'success', 'send'=>true));
            exit();
        }
        exit();
    }
    function cart($id=null) {
        $id = $id !=null ? (int)$id : 0;
        if ($id > 0) {
            $query = $this->db->get_where('news', array('id' => $id));
            $detail = $query->row_array();
            if (isset($detail['id'])) {
                $data = array(
                    'id' => $id,
                    'qty' => 1,
                    'price' => $detail['price'],
                    'name' => $detail['name']
                );
                $this->cart->insert($data);
            }
        }
        if (isset($_POST['cart'])) {
            $detail_post_cart = json_decode($_POST['cart'], true);
            foreach ($detail_post_cart as $item) {
                $data = array(
                    'rowid' => $item['rowid'],
                    'qty' => $item['qty']
                );
                $this->cart->update($data);
            }
        }
        $this->data['cart'] = $this->cart->contents();
        if($this->data['cart'])
            echo json_encode(array('code'=>200, 'message'=>'Cập nhập giỏ hàng thành công'));
        else
            echo json_encode(array('code'=>403, 'message'=>'Không thêm được sản phẩm vào giỏ hàng'));
        exit();
    }
    function deleteCart()
    {
        if(isset($_POST['action']) && $_POST['action'] = 'delete_cart')
        {
            $rowid = $_POST['rowid'];
            $carts = $this->cart->contents();
            foreach($carts as $key=>$val)
            {
                if($rowid==$val['rowid'])
                {
                    $data = array(
                        'rowid' => $key,
                        'qty' => 0
                    );
                    $this->cart->update($data);
                    break;
                }
            }
        }
        if(!$this->cart->contents())
            echo json_encode(array('code'=>200, 'message'=>'Cập nhập giỏ hàng thành công'));
        else
            echo json_encode(array('code'=>403, 'message'=>'Cập nhập giỏ hàng thất bại'));
        exit();
    }
    function checkout() {
        if ($_POST) {
            
            array_splice($_POST,0,1);
            $_post = $_POST;
            foreach ($this->cart->contents() as $item) {
                $_post['qty'] = $item['qty'];
                $_post['total_money'] = $item['subtotal'];
                $_post['product_id'] = $item['id'];
                $_post['qty'] = $item['qty'];
                $query = $this->Bluesoft_m->__save_order($_post);
                if($query)
                {
                    $this->cart->destroy();
                    echo json_encode(array('code'=>200, 'message'=>'Đặt hàng thành công'));
                }
                else
                    echo json_encode(array('code'=>403, 'message'=>'Đặt hàng không thành công'));
                exit();
            }
        }
    }


    public function load_cart()
    {
        $html_order = '';
        if($this->cart->contents())
        {
            $html_order = '
                <form class="form-cart" action="" method="post">
                    <table class="cart-form">
                        <thead>
                            <tr>
                                <th>Hình ảnh</th>
                                <th>Tên sản phẩm</th>
                                <th>Đơn giá</th>
                                <th>Số lượng</th>
                                <th>Giá tiền</th>
                                <th>Xóa</th>
                            </tr>
                        </thead>
                        <tbody>
                    ';
                    $total = 0;
                    foreach ($this->cart->contents() as $key_cart => $value_cart) {
                        $total += $value_cart['subtotal'];
                        $query = $this->db->get_where('news', array('id' => $value_cart['id']));
                        $pro_detail = $query->row_array();
                        $html_order .= '
                            <tr>
                                <td>
                                    <a href="" title="Bao Tử Trường sinh"> <img src="'.base_url().$pro_detail['image'].'" alt="'.$pro_detail['name'].'"> </a>
                                </td>
                                <td><a class="smooth" href="" title="'.$pro_detail['name'].'">'.$pro_detail['name'].'</a></td>
                                <td>'.number_format($pro_detail['price'], 0, '.', '.').'</td>
                                <td>
                                    <input class="qty" type="number" min="1" value="'.$value_cart['qty'].'">
                                </td>
                                <td>450.000</td>
                                <td><a class="_cart_delete" pro-id="'.$value_cart['rowid'].'" href="#" title=""><i class="glyphicon glyphicon-trash"></i></a></td>
                                <input type="hidden" name="rowid" value="'.$value_cart['rowid'].'">
                            </tr>
                        ';
                    }
                    
                    $html_order .='
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-sm-6 col-sm-push-6 col-xs-12 text-right">
                            <button class="smooth _cart_updateall" type="button">Tổng thành tiền</button>
                        </div>
                        <div class="col-sm-6 col-sm-pull-6 col-xs-12"> <span>Tổng thành tiền: </span>
                            <label>'.number_format($total, 0, '.', '.').'</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-push-6 col-xs-12">
                            <input type="text" name="name" placeholder="Họ và tên">
                            <input type="number" name="phone" placeholder="Số điện thoại"> 
                        </div>
                        <div class="col-sm-6 col-sm-pull-6 col-xs-12">
                            <input type="text" name="address" placeholder="Địa chỉ">
                            <input type="text" name="content" placeholder="Nội dung"> 
                        </div>
                    </div>
                    <div class="tm-popup-btn">
                        <button class="smooth" type="submit">Đặt hàng</button>
                        <button class="smooth" type="button">Hủy bỏ</button>
                    </div>
                </form>
                ';
        }
        else
        {
            $html_order = '<p class="text-center">Không tồn tại sản phẩm nào trong giỏ hàng</p>';
        }
        
        echo $html_order;
    }

}
