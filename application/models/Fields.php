<?php

/**
 * Description of fields
 * @author trungthuc
 * @date Jan 28, 2015
 */
class fields extends CI_Model {
    /*
     * Constant
     * Int: int, float, tinyint, checkbox, object
     * String: char, varchar, media, file,alias
     * Time: date, datetime, timestamp
     * 
     */

    public $field = 'field';
    public $type = array(
        0 => 'Chọn kiểu dữ liệu',
        1 => 'INT',
        2 => 'TINYINT',
        3 => 'VARCHAR',
        4 => 'TEXT',
        5 => 'TIMESTAMP',
        6 => 'DATE',
        7 => 'DATETIME',
        8 => 'OBJECT',
        9 => 'CHECKBOX',
        10 => 'RADIO',
        11 => 'PASSWORD',
        12 => 'ALIAS',
        13 => 'UPLOAD FILE',
        14 => 'UPLOAD IMAGE',
        15 => 'CREATE THUMB',
        16 => 'UPLOAD MEDIA',
        17 => 'MULTI UPLOAD',
        18 => 'CREATE MULTI THUMB',
        19 => 'TAGS',
        20 => 'JSON',
        21 => 'MULTI SELECT',
        22 => 'COMMENT',
        23 => 'MULTI OBJECT',
        24 => 'IS OBJECT',//my code:add for menu
        25 => 'FLOAT'
    );
    public $arr_field = array(
        'id' => array(
            'type' => 'INT',
            'label' => 'ID',
            'status' => 0,
            'primary_key' => TRUE,
            'constraint' => 11,
            'unsigned' => TRUE,
            'auto_increment' => TRUE,
            'alert' => '',
            'ord' => '-9'
        ),
        'parent_id' => array(
            'type' => 'INT',
            'status' => 0,
            'label' => 'Parent ID',
            'constraint' => 11,
            'alert' => '',
            'ord' => '-8'
        ),
        'name' => array(
            'type' => 'VARCHAR',
            'label' => 'Name',
            'status' => 1,
            'constraint' => 255,
            'alert' => 'Hãy nhập tên',
            'ord' => '-7'
        ),
        'ord' => array(
            'type' => 'INT',
            'label' => 'Thứ tự',
            'status' => 0,
            'constraint' => 11,
            'alert' => '',
            'ord' => '0'
        ),
        'status' => array(
            'type' => 'TINYINT',
            'label' => 'Trạng thái',
            'status' => 1,
            'constraint' => 1,
            'default' => 0,
            'alert' => '',
            'ord' => '-10'
        ),
        'create_time' => array(
            'type' => 'TIMESTAMP',
            'status' => 0,
            'label' => 'Ngày tạo',
            'alert' => '',
            'ord' => '-7'
        ),
        'create_by' => array(
            'type' => 'INT',
            'label' => 'Người tạo',
            'status' => 0,
            'constraint' => 11,
            'alert' => '',
            'ord' => '-6'
        ),
        'lang' => array(
            'type' => 'VARCHAR',
            'label' => 'Ngôn ngữ',
            'status' => 0,
            'constraint' => 2,
            'default' => 'vi',
            'alert' => '',
            'ord' => '-5'
        ),
    );
    public $array_seo = array(
        'meta_title' => array(
            'type' => 'VARCHAR',
            'label' => 'Meta Title',
            'status' => 1,
            'constraint' => 255,
            'alert' => '',
            'ord' => '-4'
        ),
        'meta_description' => array(
            'type' => 'VARCHAR',
            'label' => 'Meta Description',
            'status' => 1,
            'constraint' => 255,
            'alert' => '',
            'ord' => '-3'
        ),
        'meta_keyword' => array(
            'type' => 'VARCHAR',
            'label' => 'Meta Keyword',
            'status' => 1,
            'constraint' => 255,
            'alert' => '',
            'ord' => '-2'
        )
    );
    function __construct() {
        parent::__construct();
    }

    function __getAllField($object_id, $limit = null, $offset = 0) {
        $this->db->select('*');
        $this->db->where('object_id', $object_id);
        $this->db->order_by('ord', 'ASC');
        $this->db->order_by('id', 'DESC');
        if ($limit) {
            $this->db->limit($limit, $offset);
        }
        $query = $this->db->get($this->field);
        return $query->result_array();
        $query->free_result();
    }

    function __detailField($field_id) {
        $this->db->select('*');
        $this->db->where('id', $field_id);
        $query = $this->db->get($this->field);
        if ($query->result_array()) {
            foreach ($query->result_array() as $result)
                return $result;
        } else {
            return false;
        }
        $query->free_result();
    }

    function __createField($object_id, $has_seo) {//add-3-add data object to table field
        if($has_seo==1)
        {
            $this->arr_field = array_merge($this->arr_field, $this->array_seo);
        }
        foreach ($this->arr_field as $key => $row) {
            $data = array(
                'parent_id' => 0,
                'object_id' => $object_id,
                'label' => $row['label'],
                'name' => $key,
                'status' => $row['status'],
                'type' => array_search($row['type'], $this->type),
                'key' => (isset($row['primary_key']) && $row['primary_key']) ? 1 : 0,
                'alert' => $row['alert'],
                'ord' => $row['ord']
            );
            $this->db->set('create_by', $this->session->userdata['user_id']);
            $this->db->set('create_time', 'NOW()', FALSE);
            $this->db->insert($this->field, $data);
        }
    }

    function __deleteAllField($object_id) {
        $this->db->where('object_id', $object_id);
        $this->db->delete($this->field);
    }

    function __updateFields($id, $status) {
        $update = array(
            'status' => $status
        );
        $this->db->where('id', $id);
        $this->db->update($this->field, $update);
    }

    function __detailFields($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get($this->field);
        if ($query->result_array()) {
            foreach ($query->result_array() as $result)
                return $result;
        } else {
            return false;
        }
        $query->free_result();
    }

    function __deleteField($field_id) {
        $this->db->where('id', $field_id);
        $this->db->delete($this->field);
    }

    function __updateOrdField($field_id, $ord) {
        $update = array(
            'ord' => $ord
        );
        $this->db->where('id', $field_id);
        $this->db->update($this->field, $update);
    }

    function __deleteFieldObject($object_name, $field_name) {
        $this->load->dbforge();
        $this->dbforge->drop_column($object_name, $field_name);
    }

    function __addField($object_name) {
        $arr = $this->__convertMysql($this->input->get_post('type'));
        $name = $this->input->get_post('name');
        $fields = array(
            $name => $arr
        );
        $this->load->dbforge();
        $this->dbforge->add_column($object_name, $fields);
    }

    function __editField($object_name, $field) {
        $arr = $this->__convertMysql($this->input->get_post('type'));
        $name = $this->input->get_post('name');
        $arr['name'] = $name;
        $fields = array(
            $field['name'] => $arr
        );
        $this->load->dbforge();
        $this->dbforge->modify_column($object_name, $fields);
    }

    function __convertMysql($type) {
        switch ($type) {
            case 1:
            case 8:
            case 10:
            case 17:
            case 20:
                $arr = array(
                    'type' => 'INT',
                    'constraint' => ($this->input->get_post('length')) ? $this->input->get_post('length') : 11
                );
                break;
            case 2:
            case 9:
                $arr = array(
                    'type' => 'TINYINT',
                    'constraint' => 1,
                    'default' => 0
                );
                break;
            case 3:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 18:
            case 19:
            case 24://my code:add for menu
                $arr = array(
                    'type' => 'VARCHAR',
                    'constraint' => 255
                );
                break;
            case 4:
                $arr = array(
                    'type' => 'TEXT'
                );
                break;
            case 5:
                $arr = array(
                    'type' => 'TIMESTAMP'
                );
                break;
            case 6:
                $arr = array(
                    'type' => 'DATE'
                );
                break;
            case 7:
                $arr = array(
                    'type' => 'DATETIME'
                );
                break;
            // change 21
            case 21:
                $arr = array(
                    'type' => 'VARCHAR',
                    'constraint' => 255
                );
                break;
            case 25:
                $arr = array(
                    'type' => 'FLOAT'
                );
                break;
            default:
                $arr = array(
                    'type' => 'VARCHAR',
                    'constraint' => 255
                );
                break;
        }
        return $arr;
    }

    function __saveField($object_id) {
        //get max ord
        $max = $this->__maxOrd($object_id);
        if ($max):
            $ord = $max['max_ord'] + 1;
        else:
            $ord = 0;
        endif;

        $data = array(
            'object_id' => $object_id,
            'label' => $this->input->get_post('label'),
            'name' => $this->input->get_post('name'),
            'status' => ($this->input->get_post('status')) ? $this->input->get_post('status') : 0,
            'type' => ($this->input->get_post('type')) ? $this->input->get_post('type') : 3,
            'parent_id' => ($this->input->get_post('parent_id')) ? $this->input->get_post('parent_id') : 0,
            'alert' => ($this->input->get_post('alert')) ? $this->input->get_post('alert') : '',
            'ord' => $ord
        );
        if ($this->input->get_post('descriptions'))
            $data['descriptions'] = $this->input->get_post('descriptions');
        if ($this->input->get_post('height'))
            $data['height'] = $this->input->get_post('height');
        if ($this->input->get_post('width'))
            $data['width'] = $this->input->get_post('width');
        if ($this->input->get_post('length'))
            $data['length'] = $this->input->get_post('length');
        if ($this->input->get_post('editor'))
            $data['editor'] = $this->input->get_post('editor');
        if ($this->input->get_post('alert'))
            $data['alert'] = $this->input->get_post('alert');
        if ($this->input->get_post('process'))
            $data['process'] = $this->input->get_post('process');
        $this->db->set('create_by', $this->session->userdata['user_id']);
        $this->db->set('create_time', 'NOW()', FALSE);
        $this->db->insert($this->field, $data);
        //Update order
    }

    function __updateField($object_id, $field, $field_id) {

        if ($this->input->get_post('name') && $this->input->get_post('name') != $field['name'])
            $data['name'] = $this->input->get_post('name');
        if ($this->input->get_post('label') && $this->input->get_post('label') != $field['label'])
            $data['label'] = $this->input->get_post('label');
        if ($this->input->get_post('status') && $this->input->get_post('status') != $field['status'])
            $data['status'] = $this->input->get_post('status');
        if ($this->input->get_post('type') && $this->input->get_post('type') != $field['type'])
            $data['type'] = $this->input->get_post('type');
        if ($this->input->get_post('parent_id') && $this->input->get_post('parent_id') != $field['parent_id'])
            $data['parent_id'] = $this->input->get_post('parent_id');
        if ($this->input->get_post('descriptions') && $this->input->get_post('descriptions') != $field['descriptions'])
            $data['descriptions'] = $this->input->get_post('descriptions');
        if ($this->input->get_post('width') && $this->input->get_post('width') != $field['width'])
            $data['width'] = $this->input->get_post('width');
        if ($this->input->get_post('height') && $this->input->get_post('height') != $field['height'])
            $data['height'] = $this->input->get_post('height');
        if ($this->input->get_post('length') && $this->input->get_post('length') != $field['length'])
            $data['length'] = $this->input->get_post('length');
        if ($this->input->get_post('editor') != $field['editor'])
            $data['editor'] = $this->input->get_post('editor');
        if ($this->input->get_post('alert') && $this->input->get_post('alert') != $field['alert'])
            $data['alert'] = $this->input->get_post('alert');
        if ($this->input->get_post('process') && $this->input->get_post('process') != $field['process'])
            $data['process'] = $this->input->get_post('process');

        if ($data):
            $this->db->where('id', $field_id);
            $this->db->update($this->field, $data);
        endif;
    }

    function __getFieldObject($object_id) {
        $this->db->select('*');
        $this->db->where('object_id', $object_id);
        $query = $this->db->get($this->field);
        return $query->result_array();
        $query->free_result();
    }

    function __getFieldDetail($field_id) {
        $this->db->select('*');
        $this->db->where('id', $field_id);
        $query = $this->db->get($this->field);
        foreach ($query->result_array() as $row)
            return $row;
        $query->free_result();
    }

    function __maxOrd($object_id) {
        $this->db->select('MAX(ord) as max_ord');
        $this->db->select('id');
        $this->db->where('object_id', $object_id);
        $query = $this->db->get($this->field);
        foreach ($query->result_array() as $row)
            return $row;
        $query->free_result();
    }

    function __infoKeybyName($object_id, $key) {
        $this->db->select('*');
        $this->db->where('object_id', $object_id);
        $this->db->where('name', $key);
        $query = $this->db->get($this->field);
        foreach ($query->result_array() as $row)
            return $row;
        $query->free_result();
    }

    function __checkField($object_id, $type) {
        $this->db->select('*');
        $this->db->where('object_id', $object_id);
        $this->db->where('name', $key);
        $query = $this->db->get($this->field);
        foreach ($query->result_array() as $row)
            return $row;
        $query->free_result();
    }

    function __totalFields($object_id) {
        $this->db->where('object_id', $object_id);
        $this->db->from($this->field);
        return $this->db->count_all_results();
    }

}
