<?php

class gmail extends CI_Model {

    function __construct() {

        parent::__construct();
        $this->config->load('conf');
    }

    function send_mail($to, $from, $from_name, $subject, $body) {

        global $error;
        $settings = $this->__getSettings();
        // $prama = array(
        //     "usermail" => $settings[4]['value'],
        //     "passmail" => $settings[6]['value'],
        //     "hostmail" => $settings[7]['value'],
        //     "portmail" => $settings[8]['value']
        // );
        //  $prama = array(
        //     "usermail" => $this->config->item('usermail'),
        //     "passmail" => $this->config->item('passmail'),
        //     "hostmail" => $this->config->item('hostmail'),
        //     "portmail" => $this->config->item('portmail')
        // );

        require_once('phpmailer/class.phpmailer.php');

        // define('GUSER', $prama['usermail']); // tài khoản đăng nhập Gmail

        // define('GPWD', $prama['passmail']); // mật khẩu cho cái mail này

        

        $mail = new PHPMailer();  // tạo một đối tượng mới từ class PHPMailer
        
        $mail->CharSet = "utf-8";

        $mail->ContentType = "text/html";

        // $mail->IsSMTP(); // bật chức năng SMTP

        // $mail->SMTPDebug = 1;  // kiểm tra lỗi : 1 là  hiển thị lỗi và thông báo cho ta biết, 2 = chỉ thông báo lỗi

        // $mail->SMTPAuth = true;  // bật chức năng đăng nhập vào SMTP này

        // $mail->SMTPSecure = 'tls'; // sử dụng giao thức SSL vì gmail bắt buộc dùng cái này//ssl

        // $mail->Host = $prama['hostmail'];

        // $mail->Port = $prama['portmail'];

        // $mail->Username = GUSER;

        // $mail->Password = GPWD;

        $mail->SetFrom($from, $from_name);

        $mail->Subject = $subject;

        $mail->Body = $body;

        $mail->AddAddress($to);

        if (!$mail->Send()) {

            $error = 'Gửi mail bị lỗi: ' . $mail->ErrorInfo;

            return false;
        } else {

            $error = 'Thư của bạn đã được gửi đi ';

            return true;
        }
    }
    
    function __getSettings() {
        $this->db->select('*');
        $this->db->order_by('ord', 'ASC');
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get('tb_setting');
        return $query->result_array();
        $query->free_result();
    }

}

?>