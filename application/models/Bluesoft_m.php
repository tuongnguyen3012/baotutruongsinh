<?php

class Bluesoft_m extends CI_Model {

    function __construct()
    {
        parent::__construct();
    } 

    // MY CODE
     function __get_extension() {
        $this->db->select('*');
        $query = $this->db->get('tb_extension');
        $data = array();
        foreach ($query->result_array() as $row) {
            $data[$row['name']] = array(
                'link' => $row['name'],
                'img' => $row['img'],
                'content' => $row['content'],
                'link_left' => $row['link_left'],
                'img_left' => $row['img_left'],
                'link_right' => $row['link_right'],
                'img_right' => $row['img_right']
                );
        }
        return $data;
        $query->free_result();
    }
    //get file multi
    function __get_multi_file($id_object, $id_record)
    {
        $this->db->select('file.id as _file_id,file.file_path as _file_path');
        $this->db->select('file_mapper.id as _file_mapper_id');
        $this->db->from('file');
        $this->db->join('file_mapper', 'file.id=file_mapper.file_id');
        $this->db->where('file_mapper.status', 1);
        $this->db->where(array('record_id'=>$id_record, 'object_id'=>$id_object));
        $sql = $this->db->get();
        return $sql->result_array();
        $sql->free_result();
    }
    // get multi object
    function __get_multi_object($object_id, $record_map_id, $limit=null, $start=0, $not_id=null)//$record_map_id = id categories
    {
        $this->db->select('news.name, news.alias, news.image, news.description, news.home');
        $this->db->select('object_mapper.record_map_id');
        $this->db->from('news');
        $this->db->join('object_mapper', 'object_mapper.record_id=news.id');
        $this->db->where('news.status', 1);
        $this->db->where('object_mapper.object_id', $object_id);
        $this->db->where_in('object_mapper.record_map_id', $record_map_id);
        if($not_id)
            $this->db->where_not_in('news.id', $not_id);
        if($limit)
            $this->db->limit($limit, $start);
        $sql = $this->db->get();
        return $sql->result_array();
        $sql->free_result();
    }
    function __get_seo()
    {
        $this->db->where('status', 1);
        $query=$this->db->get('seo');
        return $query->row_array();
        $query->free_result();
    }
    function __get_pagination_theme()
    {
        $config['prev_link'] = '<';
        $config['next_link'] = '>';
        $config['cur_tag_close'] = '</a></li>';
        $config['cur_tag_open'] = '<li class="active"><a class="current">';
        $config['num_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li class="pagin-first">';
        $config['last_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="pagin-last">';
        $config['first_link'] = '<<';
        $config['last_link'] = '>>';
        $config['use_page_numbers'] = true;
        $config['prev_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="pagin-prev">';
        $config['next_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li class="pagin-next">';
        return $config;
    }
    function __slider($limit = null, $start = 0, $by="ord", $order='ASC') {
        $this->db->select('id, name, image');
        $this->db->where('status', 1);
        $this->db->order_by($by, $order);
        if($limit)
            $this->db->limit($limit, $start);
        $sql = $this->db->get('slider');
        return $sql->result_array();
        $sql->free_result();
    }

    function __get_categories($params=array())
    {
        $this->db->select('id, name, alias, image');
        $this->db->where('status', 1);
        $this->db->order_by('ord', 'ASC');
        if (array_key_exists("start", $params) && array_key_exists("limit", $params)) {
            $this->db->limit($params['limit'], $params['start']);
        } elseif (!array_key_exists("start", $params) && array_key_exists("limit", $params)) {
            $this->db->limit($params['limit']);
        }
        if(array_key_exists('home', $params))
        {
            $this->db->where('home', $params['home']);
        }
        $query = $this->db->get('categories');
        return $query->result_array();
        $query->free_result();
    }
    function __detail_category($alias)
    {
        $this->db->select('id, name, alias, parent_id');
        $this->db->where('status', 1);
        $this->db->where('alias', $alias);
        $query = $this->db->get('categories');
        return $query->row_array();
        $query->free_result();
    }

    function __detail_category_id($id)
    {
        $this->db->select('id, name, image');
        $this->db->where('status', 1);
        $this->db->where('id', $id);
        $query = $this->db->get('categories');
        return $query->row_array();
        $query->free_result();
    }
    function __detail_news($alias)
    {
        $this->db->select('id, name, category, alias, image, content, create_time, home, meta_description, meta_keyword, meta_title');
        $this->db->where('news.alias', $alias);
        $sql = $this->db->get('news');
        return $sql->row_array();
        $sql->free_result();
    }

    function __get_menus($parent_id, $type_id)
    {
        $this->db->select('id, name, url, parent_id, danhmuc_id, baiviet_id');
        $this->db->order_by('ord', 'ASC');
        $this->db->where('parent_id', $parent_id);
        $this->db->where('type_id', $type_id);
        $query = $this->db->get('menus');
        return $query->result_array();
        $query->free_result();
    }
    function __get_cate_by_news($record_id)
    {
        $this->db->select('categories.*');
        $this->db->from('categories');
        $this->db->join('object_mapper', 'object_mapper.record_map_id=categories.id');
        $this->db->where('object_mapper.record_id', $record_id);
        $query = $this->db->get();
        return $query->result_array();
        $query->free_result();
    }

    function __update_news($id, $data)
    {
        $this->db->where('id', $id);
        $query = $this->db->update('news', $data);
    }
    function __get_news($cate = null, $limit=null, $start = 0, $by="ord", $order='ASC')
    {
        $this->db->select('*');
        $this->db->where('status', 1);
        $this->db->order_by($by, $order);
        if($cate)
            $this->db->where('category', $cate);
        if($limit)
            $this->db->limit($limit, $start);
        $sql = $this->db->get('news');
        return $sql->result_array();
        $sql->free_result();
    }

    function __save_order($data)
    {
        $this->db->set('create_time', Date('Y-m-d H:i:s'));
        $this->db->set('status', 0);
        $this->db->insert('order', $data);
        return $this->db->insert_id();
    }

    function __save_advisory($data)
    {
        $this->db->set('create_time', Date('Y-m-d H:i:s'));
        $this->db->set('status', 1);
        $this->db->insert('advisory', $data);
        return $this->db->insert_id();
    }
}

