<?php

/**
 * Description of b
 * @author trungthuc
 * @date Jul 10, 2016
 */
class b extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	function __adv($limit = null, $start = null) {
		$this->db->select('*');
		$this->db->where('status', 1);
		$this->db->order_by('ord', 'DESC');
		$this->db->limit($limit, $start);
		$sql = $this->db->get('banner');
		return $sql->result_array();
		$sql->free_result();
	}

	function __ve() {
		$this->db->select('*');
		$this->db->where('status', 1);
		$this->db->order_by('ord', 'ASC');
		$query = $this->db->get('ve');
		return $query->result_array();
		$query->free_result();
	}

	function __menu_news() {
		$this->db->select('*');
		$this->db->where('status', 1);
		$this->db->order_by('ord', 'ASC');
		$query = $this->db->get('category');
		return $query->result_array();
		$query->free_result();
	}

	function __category($cateID = 0, $limit = null) {
		$this->db->select('*');
		$this->db->where('status', 1);
		$this->db->where('parent_id', $cateID);
		if ($limit)
			$this->db->limit($limit);
		$this->db->order_by('id', 'ASC');
		$query = $this->db->get('category_product');
		return $query->result_array();
		$query->free_result();
	}

	function __detail_post($alias) {
		$this->db->select('post.*');
		$this->db->select('category.name as cate_name');
		$this->db->select('category.alias as cate_alias');
		$this->db->from('post');
		$this->db->join('category', 'category.id = post.cate');
		$this->db->where('post.alias', $alias);
		$sql = $this->db->get();
		return $sql->row_array();
		$sql->free_result();
	}

	function __detail_category($alias) {
		$this->db->select('*');
		$this->db->where('alias', $alias);
		$sql = $this->db->get('category');
		return $sql->row_array();
		$sql->free_result();
	}

	function __totalPostByCate($catId) {
		$this->db->where('status', 1);
		$this->db->where_in('cate', $catId);
		$this->db->from('post');
		return $this->db->count_all_results();
	}

	function __postByCate($catId, $limit = null, $start = null) {
		$this->db->select('post.*');
		$this->db->select('category.name as cate_name');
		$this->db->select('category.alias as cate_alias');
		$this->db->from('post');
		$this->db->join('category', 'category.id = post.cate');
		$this->db->where_in('post.cate', $catId);
		$this->db->where('post.status', 1);
		$this->db->order_by('post.id', 'DESC');
		if ($limit)
			$this->db->limit($limit, $start);
		$sql = $this->db->get();
		return $sql->result_array();
		$sql->free_result();
	}

	function __postSameCate($id, $catId, $limit = null) {
		$this->db->select('*');
		$this->db->where('status', 1);
		$this->db->where('cate', $catId);
		$this->db->where_not_in('id', $id);
		if ($limit)
			$this->db->limit($limit);
		$this->db->order_by('id', 'DESC');
		$sql = $this->db->get('post');
		return $sql->result_array();
		$sql->free_result();
	}

	function __get_extension() {
		$this->db->select('*');
		$query = $this->db->get('tb_extension');
		$data = array();
		foreach ($query->result_array() as $row) {
			$data[$row['name']] = array(
				'link' => $row['name'],
				'img' => $row['img'],
				'content' => $row['content'],
				'link_left' => $row['link_left'],
				'img_left' => $row['img_left'],
				'link_right' => $row['link_right'],
				'img_right' => $row['img_right']
			);
		}
		return $data;
		$query->free_result();
	}

	function __news($limit = null, $start = null) {
		$this->db->select('*');
		$this->db->where('status', 1);
		$this->db->order_by('id', 'DESC');
		$this->db->limit($limit, $start);
		$sql = $this->db->get('news');
		return $sql->result_array();
		$sql->free_result();
	}

	function __slider($limit = null, $start = null) {
		$this->db->select('*');
		$this->db->where('status', 1);
		$this->db->order_by('id', 'DESC');
		$this->db->limit($limit, $start);
		$sql = $this->db->get('slide');
		return $sql->result_array();
		$sql->free_result();
	}

	function __total_news() {
		return $this->db->count_all_results('news');
	}

	function __detail_news() {
		$id = $this->uri->segment(2);
		if ($id > 0) {
			$sql = $this->db->get_where('news', array('id' => $id));
			return $sql->row_array();
		}
	}

	function __detail_khuyenmai() {
		$id = $this->uri->segment(2);
		if ($id > 0) {
			$sql = $this->db->get_where('sellingpro', array('id' => $id));
			return $sql->row_array();
		}
	}

	function __newstop($start = 0, $limit = 1) {
		$this->db->select('*');
		$this->db->where('status', 1);
		if ($limit)
			$this->db->limit($limit, $start);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get('news');
		return $query->result_array();
		$query->free_result();
	}

	function __detail_ve_slug($alias) {
		$this->db->select('*');
		$this->db->where('alias', $alias);
		$sql = $this->db->get('ve');
		return $sql->row_array();
		$sql->free_result();
	}

	function __detail_ve_id($id) {
		$this->db->select('*');
		$this->db->where('id', $id);
		$sql = $this->db->get('ve');
		return $sql->row_array();
		$sql->free_result();
	}
	
	function __detail_ticket_slug($alias) {
		$this->db->select('*');
		$this->db->where('alias', $alias);
		$sql = $this->db->get('ticket');
		return $sql->row_array();
		$sql->free_result();
	}
	
	function __detail_ticket_id($id) {
		$this->db->select('*');
		$this->db->where('id', $id);
		$sql = $this->db->get('ticket');
		return $sql->row_array();
		$sql->free_result();
	}
	
	function __detail_category_id($id) {
		$this->db->select('*');
		$this->db->where('id', $id);
		$sql = $this->db->get('category');
		return $sql->row_array();
		$sql->free_result();
	}
	
	function __detail_category_slug($alias) {
		$this->db->select('*');
		$this->db->where('alias', $alias);
		$sql = $this->db->get('category');
		return $sql->row_array();
		$sql->free_result();
	}

	function __categoryVe($ids, $limit = null, $start = null) {
		$this->db->select('ticket.*');
		$this->db->select(array(
			've.alias as cate_alias',
			've.name as cate_name'
		));
		$this->db->from('ticket');
		$this->db->join('ve', 've.id = ticket.cate');
		$this->db->where_in('ticket.cate', $ids);
		$this->db->order_by('ticket.id', 'DESC');
		if ($limit)
			$this->db->limit($limit, $start);
		$sql = $this->db->get();
		return $sql->result_array();
		$sql->free_result();
	}

	function __arrayVe($id) {
		$this->db->select('id');
		$this->db->where('parent_id', $id);
		$sql = $this->db->get('ve');
		$ids = array($id);
		foreach ($sql->result_array() as $row) {
			$ids[] = $row['id'];
		}
		return $ids;
		$sql->free_result();
	}
	
	function __arrayCategory($id) {
		$this->db->select('id');
		$this->db->where('parent_id', $id);
		$sql = $this->db->get('category');
		$ids = array($id);
		foreach ($sql->result_array() as $row) {
			$ids[] = $row['id'];
		}
		return $ids;
		$sql->free_result();
	}

	function __review($limit = 3) {
		$this->db->select('*');
		$this->db->where('status', 1);
		$this->db->order_by('id', 'DESC');
		$this->db->limit($limit);
		$sql = $this->db->get('review');
		return $sql->result_array();
		$sql->free_result();
	}

	function __postsByCate($cateId, $limit = 3) {
		$this->db->select('post.*');
		$this->db->select('category.name as cate_name');
		$this->db->select('category.alias as cate_alias');
		$this->db->from('post');
		$this->db->join('category', 'category.id = post.cate');
		$this->db->where('post.status', 1);
		$this->db->where('post.cate', $cateId);
		$this->db->order_by('post.id', 'DESC');
		$this->db->limit($limit);
		$sql = $this->db->get();
		return $sql->result_array();
		$sql->free_result();
	}

	function __postsByCateFirst($cateId) {
		$this->db->select('post.*');
		$this->db->select('category.name as cate_name');
		$this->db->select('category.alias as cate_alias');
		$this->db->from('post');
		$this->db->join('category', 'category.id = post.cate');
		$this->db->where('post.status', 1);
		$this->db->where('post.cate', $cateId);
		$this->db->order_by('post.id', 'DESC');
		$this->db->limit(1);
		$sql = $this->db->get();
		return $sql->row_array();
		$sql->free_result();
	}

	function __loadModule($id) {
		$this->db->select('*');
		$this->db->where('id', $id);
		$this->db->limit(1);
		$sql = $this->db->get('module');
		return $sql->row_array();
		$sql->free_result();
	}

	function __loadMenu($position = 1, $limit = null) {
		$this->db->select('*');
		$this->db->where('position', $position);
		$this->db->order_by('ord', 'ASC');
		if ($limit)
			$this->db->limit($limit);
		$sql = $this->db->get('menu');
		return $sql->result_array();
		$sql->free_result();
	}

	function __menuBottom($cateId, $limit = 3) {
		$this->db->select('*');
		$this->db->where('status', 1);
		$this->db->where('parent_id', $cateId);
		$this->db->order_by('id', 'DESC');
		$this->db->limit($limit);
		$sql = $this->db->get('ve');
		return $sql->result_array();
		$sql->free_result();
	}
	
	function __detail_page_id($id) {
		$this->db->select('*');
		$this->db->where('id', $id);
		$sql = $this->db->get('page');
		return $sql->row_array();
		$sql->free_result();
	}

}
