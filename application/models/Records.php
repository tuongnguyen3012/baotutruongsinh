<?php

/**
 * Description of records
 * @author trungthuc
 * @date Jan 30, 2015
 */
class records extends CI_Model {

    public $field = 'field';
    public $object = 'object';
    public $file = 'file';
    public $fileMapper = 'file_mapper';
    public $objectMapper = 'object_mapper';
    public $type = array(
        0 => 'Chọn kiểu dữ liệu',
        1 => 'INT',
        2 => 'TINYINT',
        3 => 'VARCHAR',
        4 => 'TEXT',
        5 => 'TIMESTAMP',
        6 => 'DATE',
        7 => 'DATETIME',
        8 => 'OBJECT',
        9 => 'CHECKBOX',
        10 => 'RADIO',
        11 => 'PASSWORD',
        12 => 'ALIAS',
        13 => 'UPLOAD FILE',
        14 => 'UPLOAD IMAGE',
        15 => 'CREATE THUMB',
        16 => 'UPLOAD MEDIA',
        17 => 'MULTI UPLOAD',
        18 => 'CREATE MULTI THUMB',
        19 => 'TAGS',
        20 => 'JSON',
        21 => 'MULTI SELECT',
        22 => 'COMMENT',
        23 => 'MULTI OBJECT',
        24 => 'IS OBJECT',//my code:add for menu
        25 => 'FLOAT'
    );

    function __construct() {

        parent::__construct();
    }

    /*
     * get all field display
     */

    function __getAllField($object_id) {
        $this->db->select('*');
        $this->db->where('object_id', $object_id);
        $this->db->where('status', 1);
        $this->db->order_by('ord', 'ASC');
        $this->db->order_by('name', 'DESC');
        $query = $this->db->get($this->field);
        return $query->result_array();
        $query->free_result();
    }

    /*
     * Function parse Html
     */

    function __parseForm($field, $data = null) {
        switch ($field['type']) {
            case 1:
            case 10:
            case 20:
            case 25:
                $this->__htmlInput($field, $data);
                break;
            case 2:
            case 9:
                $this->__htmlCheckbox($field, $data);
                break;
            case 11:
                $this->__htmlPassword($field, $data);
                break;
            case 3:
            case 19:
                $this->__htmlInput($field, $data);
                break;
            case 4:
                switch ($field['editor']) {
                    case 1:
                        $this->__htmlNic($field, $data);
                        break;
                    case 2:
                        $this->__htmlFck($field, $data);
                        break;
                    default:
                        $this->__htmlTextArea($field, $data);
                        break;
                }
                break;
            case 5:
                $this->__htmlTimeStamp($field, $data);
                break;
            case 6:
                $this->__htmlDate($field, $data);
                break;
            case 7:
                $this->__htmlDateTime($field, $data);
                break;
            case 8:
                $this->__htmlObject($field, $data);
                break;
            case 12:
                $this->__htmlAlias($field, $data);
                break;
            case 13:
            case 14:
                $this->__htmlFile($field, $data);
                break;
            case 15:
            case 18:
                break;
            case 16:
                $this->__htmlMedia($field, $data);
                break;
            case 17:
                $this->__htmlMultiUpload($field, $data);
                break;
            case 21:
                $this->__htmlMultiSelect($field, $data);
                break;
            case 23:
                $this->__htmlMultiObject($field, $data);
                break;
            case 24:
                $this->__htmlShowObject($field, $data);
                break;
            default:
                $this->__htmlInput($field, $data);
                break;
        }
    }

    function __htmlInput($field, $value = null) {
        $html = '<p>';
        $html .= form_label($field['label'], $field['name']);
        $html .= '<span class="field">';
        $html .= form_input(array(
            'name' => $field['name'],
            'id' => $field['name'],
            'class' => 'input-block-level',
            'placeholder' => $field['label'],
            'value' => $value[$field['name']]
        ));
        $html .= '</span>';
        $html .= '</p>';
        echo $html;
    }

    function __htmlAlias($field, $value = null) {
        $html = '<p>';
        $html .= form_label($field['label'], $field['name']);
        $html .= '<span class="field">';
        $html .= form_input(array(
            'name' => $field['name'],
            'id' => 'alias',
            'class' => 'input-block-level',
            'placeholder' => $field['label'],
            'value' => $value[$field['name']]
        ));
        $html .= '<button type="button" class="generator">Generator</button>';
        $html .= '</span>';
        $html .= '</p>';
        echo $html;
    }

    function __htmlTextArea($field, $value = null) {
        $html = '<p>';
        $html .= form_label($field['label'], $field['name']);
        $html .= '<span class="field">';
        $html .= form_textarea(array(
            'name' => $field['name'],
            'id' => $field['name'],
            'class' => 'input-block-level',
            'placeholder' => $field['label'],
            'value' => $value[$field['name']]
        ));
        $html .= '</span>';
        $html .= '</p>';
        echo $html;
    }

    function __htmlCheckbox($field, $value = null) {
        $html = '<p>';
        $html .= form_label($field['label'], $field['name']);
        $html .= '<span class="field">';
        $html .= form_checkbox($field['name'], 1, $value[$field['name']] != null ? $value[$field['name']] : 1);
        $html .= '<span style="padding-left:10px">' . $field['descriptions'] . '</span>';
        $html .= '</span>';
        $html .= '</p>';
        echo $html;
    }

    function __htmlFile($field, $value = null) {
        $html = '<p>';
        $html .= form_label($field['label'], $field['name']);
        $html .= '<span class="field">';
        if ($value[$field['name']]):
            $html .= form_hidden($field['name'], $value[$field['name']]);
            //Display thumb
            if ($field['type'] == 14):
                $details = $this->__ThumbField($field['id']);
                if ($details):
                    foreach ($details as $detail):
                        $html .= form_hidden($detail['name'], $value[$detail['name']]);
                    endforeach;
                endif;
            endif;

            $html .= form_upload('single_file', '', 'class="clear single_file" f_type="' . $field['type'] . '" field_id="' . $field['id'] . '" rel="' . $field['name'] . '" style="display:none;"');
            $html .= '<span class="preView">';
            if ($field['type'] == 14):
                $html .= img(array(
                    'src' => $value[$field['name']],
                    'class' => 'thumbs'
                ));
            else:
                $html .= $value[$field['name']];
            endif;
            $html .= '</span>';
            $html .= '<a class="remove_single iconsweets-trashcan" name="' . $field['name'] . '" record="' . $value['id'] . '" object="1" title="Xóa"></a>';
        else:
            $html .= form_upload('single_file', '', 'class="clear single_file" f_type="' . $field['type'] . '" field_id="' . $field['id'] . '" rel="' . $field['name'] . '"');
            $html .= '<span class="preView"></span>';
            $html .= '<a style="display:none;" class="remove_single iconsweets-trashcan" name="' . $field['name'] . '" record="0" title="Xóa"></a>';
        endif;

        $html .= '<span class="clear"></span>';
        $html .= '</span>';
        $html .= '</p>';
        echo $html;
    }

    function __htmlFck($field, $value = null) {
        $this->load->helper('ckeditor');
        $html = '<p>';
        $html .= form_label($field['label'], $field['name']);
        $html .= '<span class="field">';
        $html .= form_ckeditor(array(
            'name' => $field['name'],
            'id' => $field['name'],
            'width' => ($field['width']) ? $field['width'] : '100%',
            'height' => ($field['height']) ? $field['height'] : '500',
            'value' => $value[$field['name']]
        ));
        $html .= '</span>';
        $html .= '</p>';
        echo $html;
    }

    function __htmlNic($field, $value = null) {
        $this->load->helper('niceditor');
        $html = '<p>';
        $html .= form_label($field['label'], $field['name']);
        $html .= '<span class="field">';
        $html .= form_niceditor(array(
            'name' => $field['name'],
            'id' => $field['name'],
            'width' => ($field['width']) ? $field['width'] : '100%',
            'height' => ($field['height']) ? $field['height'] : '300',
            'value' => $value[$field['name']]
        ));
        $html .= '</span>';
        $html .= '</p>';
        echo $html;
    }

    function __htmlPassword($field, $value = null) {
        $html = '<p>';
        $html .= form_label($field['label'], $field['name']);
        $html .= '<span class="field">';
        $html .= form_password(array(
            'name' => $field['name'],
            'id' => $field['name'],
            'class' => 'input-block-level',
            'placeholder' => $field['label'],
            'value' => $value[$field['name']]
        ));
        $html .= '</span>';
        $html .= '</p>';
        echo $html;
    }

    function __htmlMedia($field, $value = null) {
        $html = '<p>';
        $html .= form_label($field['label'], $field['name']);
        $html .= '<span class="field">';
        $html .= form_input(array(
            'name' => $field['name'],
            'id' => $field['name'],
            'class' => 'input-block-level',
            'placeholder' => $field['label'],
            'value' => $value[$field['name']]
        ));
        $html .= '</span>';
        $html .= '</p>';
        echo $html;
    }

    function __htmlDate($field, $value = null) {
        $html = '<p>';
        $html .= form_label($field['label'], $field['name']);
        $html .= '<span class="field">';
        $html .= form_input(array(
            'name' => $field['name'],
            'id' => $field['name'],
            'placeholder' => $field['label'],
            'value' => ($value[$field['name']]) ? $value[$field['name']] : date('Y-m-d')
        ));
        $html .= '</span>';
        $html .= '</p>';
        $html .= '<script>jQuery("input[name=' . $field['name'] . ']").datetimepicker({timepicker:false});</script>';
        echo $html;
    }

    function __htmlTimeStamp($field, $value = null) {
        $html = '<p>';
        $html .= form_label($field['label'], $field['name']);
        $html .= '<span class="field">';
        $html .= form_input(array(
            'name' => $field['name'],
            'id' => $field['name'],
            'placeholder' => $field['label'],
            'value' => date('Y-m-d')
        ));
        $html .= '</span>';
        $html .= '</p>';
        echo $html;
    }

    function __htmlDateTime($field, $value = null) {
        $html = '<p>';
        $html .= form_label($field['label'], $field['name']);
        $html .= '<span class="field">';
        $html .= form_input(array(
            'name' => $field['name'],
            'id' => $field['name'],
            'placeholder' => $field['label'],
            'value' => ($value[$field['name']]) ? $value[$field['name']] : date('Y-m-d H:i:s')
        ));
        $html .= '</span>';
        $html .= '</p>';
        echo $html;
    }

    function __htmlMultiUpload($field, $value = null) {

        $html = '<p>' . form_label($field['label'], $field['name']) . '</p>';
        if ($value):
            $datas = $this->__mapFile($value['id'], $field['id']);
            foreach ($datas as $row):
                $html .= '<p id="Edit-' . $field['name'] . $row['id'] . '">';
                $html .= '<span class="field">';
                $html .= '<input type="hidden" name="' . $field['name'] . '[]" value="' . $row['id'] . '" />';
                $html .= '<input type="hidden" name="map_' . $field['name'] . '[]" value="' . $row['mapper_id'] . '" />';
                $html .= '<span class="imagePrev">';
                $html .= img(array(
                    'src' => $row['file_path'],
                    'class' => 'thumbs'
                ));
                $html .= '</span>';
                $html .= form_upload('file', '', 'class="clear multiple" multiple="multiple" rel="' . $field['name'] . '[]" field_id="' . $field['id'] . '" style="display:none" ');

                $html .= form_input(array(
                    'name' => 'des_' . $field['name'] . '[]',
                    'class' => 'description_file',
                    'placeholder' => 'Mô tả...',
                    'value' => $row['description']
                ));

                $html .= '<a class="iconsweets-refresh3 change" title="Thay đổi"></a>';
                $html .= '<a class="remove iconsweets-trashcan" id="' . $row['id'] . '" name="Edit-' . $field['name'] . '" tmp_id="' . $row['id'] . '" mapper_id="' . $row['mapper_id'] . '" field_id="' . $field['id'] . '" title="Xóa"></a>';
                $html .= '<span class="clear"></span>';
                $html .= '</span>';
                $html .= '</p>';
            endforeach;
        else:
            $html .= '<p id="' . $field['name'] . '">';
            $html .= '<span class="field">';
            $html .= '<input type="hidden" name="map_' . $field['name'] . '[]" value="" />';
            $html .= '<span class="imagePrev"></span>';
            $html .= form_upload('file', '', 'class="clear multiple" multiple="multiple" rel="' . $field['name'] . '[]" field_id="' . $field['id'] . '" ');
            $html .= form_input(array(
                'name' => 'des_' . $field['name'] . '[]',
                'class' => 'description_file',
                'placeholder' => 'Mô tả...'
            ));
            $html .= '<a class="iconsweets-refresh3 change" title="Thay đổi"></a>';
            $html .= '<a class="remove iconsweets-trashcan" id="" name="' . $field['name'] . '" tmp_id="" mapper_id="0" field_id="' . $field['id'] . '" title="Xóa"></a>';
            $html .= '<span class="clear"></span>';
            $html .= '</span>';
            $html .= '</p>';
        endif;
        $html .= '<p class = "add-' . $field['name'] . '"><span class = "field">';
        $html .= '<a class = "btn btn-primary btn-rounded add_file" name = "' . $field['name'] . '" id="' . $field['id'] . '"> <i class = "iconsweets-create iconsweets-white"></i> &nbsp;
        Thêm ảnh</a>';
        $html .= '</span></p>';
        echo $html;
    }

    function __htmlObject_bk($field, $value = null) {
        $object = $this->__detailObjects($field['parent_id']);
		
        $data = array();
        $this->db->select('*');
        $this->db->where('lang', $this->session->userdata('lang'));
        $query = $this->db->get($object['name']);
        foreach ($query->result_array() as $row) {
            $data[$row['id']] = $row['name'];
        }
        $query->free_result();
        $html = '<p>';
        $html .= form_label($field['label'], $field['name']);
        $html .= '<span class = "field">';
        $html .= form_dropdown($field['name'], $data, $value[$field['name']], 'class = "uniformselect" id="' . $field['name'] . '"');
        $html .= '</span>';
        $html .= '</p>';
        echo $html;
    }
	
	function __htmlObject($field, $value = null) {
        $object = $this->__detailObjects($field['parent_id']);
		$data = $this->__getResurionObject($object['name'], 0);
        $html = '<p>';
        $html .= form_label($field['label'], $field['name']);
        $html .= '<span class = "field">';
        $html .= form_dropdown($field['name'], $data, $value[$field['name']], 'class = "uniformselect" id="' . $field['name'] . '"');
        $html .= '</span>';
        $html .= '</p>';
        echo $html;
    }
	
	function __getResurionObject($object, $parent_id = 0, $str = ' ') {
		$data = array();
		$this->db->select('*');
		$this->db->where('parent_id', $parent_id);
		$this->db->order_by('name', 'ASC');
		$query = $this->db->get($object);
		foreach ($query->result_array() as $row):
			$data[$row['id']] = $str . $row['name'];
			$rows = $this->__getResurionObject($object, $row['id'], '&rdsh; ');
			foreach ($rows as $key => $dt):
				$data[$key] = $str . $dt;
			endforeach;
		endforeach;
		$query->free_result();
		return $data;
	}
    // my code : add for menu
    function __htmlShowObject($field, $value = null) {
        //$value==$row của object đó
        // $value[$field['name']]//
        $data = $this->__getShowObject();
        $html = '<p>';
        $html .= form_label($field['label'], $field['name']);
        $html .= '<span class = "field">';
        $html .= form_dropdown($field['name'], $data, $value[$field['name']], 'class = "uniformselect" id="' . $field['name'] . '"');
        $html .= '</span>';
        $html .= '</p>';
        echo $html;
    }
    function __getShowObject() {
        $data = array();
        $this->db->select('*');
        $this->db->where('status', 1);
        $this->db->order_by('name', 'ASC');
        $query = $this->db->get($this->object);
        $data['NULL'] = 'Khác';
        foreach ($query->result_array() as $row):
            $data[$row['name']] = $row['label'];
        endforeach;
        return  $data;
        $query->free_result();
    }
    //end
    function __htmlMultiObject($field, $value = null) {
        $object = $this->__detailObjects($field['parent_id']);
        $html = form_label($field['label'], $field['name']);
        $html .= '<span class = "field">';
        if ($value):
            $category = $this->__ObjectMapper($value['id'], $field);
            $html .= $this->__category_edit($object['name'], $field['name'] . '[]', $category, 0);
        else:
            $html .= $this->__category($object['name'], $field['name'] . '[]', 0);
        endif;

        $html .= '</span>';
        echo $html;
    }
    // change
    // FIX ACRIVE HERE
    function __htmlMultiSelect($field, $value = null) {
        $object = $this->__detailObjects($field['parent_id']);
        $html = form_label($field['label'], $field['name']);
        $html .= '<span class = "field">';
        if ($value):
            $category = json_decode($value[$field['name']], true);//$this->__ObjectMapper($value['id'], $field);/
            $html .= $this->__category_edit($object['name'], $field['name'] . '[]', $category, 0);
        else:
            $html .= $this->__category($object['name'], $field['name'] . '[]', 0);
        endif;

        $html .= '</span>';
        echo $html;
    }
    //end change

    function __ObjectMapper($record_id, $field) {
        $object_name = $this->__nameObject($field['parent_id']);
        $this->db->select('object.id');
        $this->db->where('object_mapper.record_id', $record_id);
        $this->db->where('object_mapper.field_id', $field['id']);
        $this->db->from('object_mapper');
        $this->db->join($object_name . ' as object', 'object.id = object_mapper.record_map_id');
        $result = $this->db->get();
        $data = array();
        foreach ($result->result_array() as $row):
            $data[] = $row['id'];
        endforeach;
        $result->free_result();
        return $data;
    }

    function __category($object_name, $field_name, $parent_id = 0) {

        $this->db->select('*');
        $this->db->where('parent_id', $parent_id);
        $this->db->order_by('name', 'ASC');
        $sql = $this->db->get($object_name);
        $datas = $sql->result_array();
        $sql->free_result();
        if ($datas):
            if ($parent_id == 0):
                $html = '<ul class = "multi-object">';
            else:
                $html = '<ul>';
            endif;

            foreach ($datas as $data):
                $html .= '<li><input class = "category" value = "' . $data['id'] . '" type = "checkbox" name = "' . $field_name . '" id = "in-category-' . $data['id'] . '"> ' . $data['name'];
                $html .= $this->__category($object_name, $field_name, $data['id']);
                $html .= '</li>';
            endforeach;
            $html .= '</ul>';
            return $html;
        endif;
    }

    function __category_edit($object_name, $field_name, $category, $parent_id = 0) {

        $this->db->select('*');
        $this->db->where('parent_id', $parent_id);
        $this->db->order_by('name', 'ASC');
        $sql = $this->db->get($object_name);
        $datas = $sql->result_array();
        $sql->free_result();
        if ($datas):
            if ($parent_id == 0):
                $html = '<ul class = "multi-object">';
            else:
                $html = '<ul>';
            endif;

            foreach ($datas as $data):
                if (in_array($data['id'], $category)):
                    $html .= '<li><input class = "category" value = "' . $data['id'] . '" type = "checkbox" name = "' . $field_name . '" id = "in-category-' . $data['id'] . '" checked="checked"> ' . $data['name'];
                else:
                    $html .= '<li><input class = "category" value = "' . $data['id'] . '" type = "checkbox" name = "' . $field_name . '" id = "in-category-' . $data['id'] . '"> ' . $data['name'];
                endif;

                $html .= $this->__category_edit($object_name, $field_name, $category, $data['id']);
                $html .= '</li>';
            endforeach;
            $html .= '</ul>';
            return $html;
        endif;
    }

    /*
     * Save record
     */

    function __saveRecord($object, $fields) {
        //get max ord
        $max = $this->__maxOrd($object);
        if ($max):
            $ord = $max['max_ord'] + 1;
        else:
            $ord = 0;
        endif;
        $data_object = array();
        $data_file = array();
        foreach ($fields as $row):
            if (isset($_POST[$row['name']]) && $row['type'] == 23):
                //Save type multi object
                if ($this->input->get_post($row['name'])):
                    $data_object[$row['name']]['data'] = $this->input->get_post($row['name']);
                    $data_object[$row['name']]['field'] = $row;
                endif;

            elseif (isset($_POST[$row['name']]) && $row['type'] == 17):
                //Save type multi upload
                if ($this->input->get_post($row['name'])):
                    $data_file[$row['name']]['data'] = $this->input->get_post($row['name']);
                    $data_file[$row['name']]['des'] = $this->input->get_post('des_' . $row['name']);
                    $data_file[$row['name']]['field'] = $row;
                endif;
            // change
            elseif (isset($_POST[$row['name']]) && $row['type'] == 21):
                $this->db->set($row['name'], json_encode($this->input->get_post($row['name'])));
            //end change
            else:
                if (isset($_POST[$row['name']])):
                    $this->db->set($row['name'], $this->input->get_post($row['name']));
                endif;


            endif;
        endforeach;
        $this->db->set('lang', $this->session->userdata('lang'));
        $this->db->set('ord', $ord);
        $this->db->set('create_by', $this->session->userdata['user_id']);
        $this->db->set('create_time', 'NOW()', FALSE);
        $this->db->insert($object['name']);
        $record_id = $this->db->insert_id();
        if ($data_object):
            $this->__saveObjectMapper($record_id, $data_object);
        endif;

        if ($data_file):
            $this->__saveFileMapper($record_id, $data_file);
        endif;
        return $record_id;
    }

    function __checkThumb($object_id, $parent_id) {
        $this->db->select('*');
        $this->db->where('parent_id', $parent_id);
        $this->db->where('object_id', $object_id);
        $result = $this->db->get($this->field);
        return $result->result_array();
        $result->free_result();
    }

    function __updateRecord($object, $fields, $record_id) {
        $data = array();
        $data_file = array();
        foreach ($fields as $row):
            if (isset($_POST[$row['name']]) && $row['type'] == 23):
                //Save type multi object
                $data_delete_all = array();
                $data_del = array();
                $data_plus = array();
                $category = $this->__ObjectMapper($record_id, $row);
                if ($this->input->get_post($row['name'])):
                    //Xoa nhung ban ghi cu di.
                    $data_del['data'] = array_diff($category, $this->input->get_post($row['name']));
                    $data_del['field'] = $row;
                    //Them moi ban ghi moi.
                    $data_plus['data'] = array_diff($this->input->get_post($row['name']), $category);
                    $data_plus['field'] = $row;
                    if ($data_del['data']):
                        $this->__deleteObjectMapper($record_id, $data_del);
                    endif;
                    if ($data_plus['data']):
                        $this->__saveObjectMapperPlus($record_id, $data_plus);
                    endif;
                else:
                    $data_delete_all['data'] = $category;
                    $data_delete_all['field'] = $row;
                    $this->__deleteObjectMapper($record_id, $data_delete_all);

                endif;

            elseif (isset($_POST[$row['name']]) && $row['type'] == 17):
                //Save type multi upload
                $data_file[$row['name']]['data'] = $this->input->get_post($row['name']);
                $data_file[$row['name']]['mapper'] = $this->input->get_post('map_' . $row['name']);
                $data_file[$row['name']]['des'] = $this->input->get_post('des_' . $row['name']);
                $data_file[$row['name']]['field'] = $row;
            // change
            elseif(isset($_POST[$row['name']]) && $row['type'] == 21):
                $data[$row['name']] = json_encode($this->input->get_post($row['name']));
            // end change
            else:
                if (isset($_POST[$row['name']])):
                    $data[$row['name']] = $this->input->get_post($row['name']);
                endif;
            endif;
        endforeach;

        if ($data_file):
            $this->__updateFileMapper($record_id, $data_file);
        endif;
        $this->db->where('id', $record_id);
        $this->db->update($object['name'], $data);
    }

    function __deleteObjectMapper($record_id, $data_object) {
        foreach ($data_object['data'] as $row):
            $this->db->where('record_id', $record_id);
            $this->db->where('field_id', $data_object['field']['id']);
            $this->db->where('object_id', $data_object['field']['object_id']);
            $this->db->where('object_map_id', $data_object['field']['parent_id']);
            $this->db->where('record_map_id', $row);
            $this->db->delete('object_mapper');
        endforeach;
    }

    function __saveObjectMapperPlus($record_id, $data_object) {
        foreach ($data_object['data'] as $row):
            $data = array(
                'record_id' => $record_id,
                'field_id' => $data_object['field']['id'],
                'object_id' => $data_object['field']['object_id'],
                'record_map_id' => $row,
                'object_map_id' => $data_object['field']['parent_id'],
            );
            $this->db->set('status', 1);
            $this->db->set('create_by', $this->session->userdata['user_id']);
            $this->db->set('create_time', 'NOW()', FALSE);
            $this->db->insert('object_mapper', $data);
        endforeach;
    }

    function __saveObjectMapper($record_id, $data_object) {
        foreach ($data_object as $rows):
            foreach ($rows['data'] as $row):
                $data = array(
                    'record_id' => $record_id,
                    'field_id' => $rows['field']['id'],
                    'object_id' => $rows['field']['object_id'],
                    'record_map_id' => $row,
                    'object_map_id' => $rows['field']['parent_id'],
                );
                $this->db->set('status', 1);
                $this->db->set('create_by', $this->session->userdata['user_id']);
                $this->db->set('create_time', 'NOW()', FALSE);
                $this->db->insert('object_mapper', $data);
            endforeach;
        endforeach;
    }

    function __saveFileMapper($record_id, $data_file) {
        foreach ($data_file as $rows):
            foreach ($rows['data'] as $key => $row):
                $data = array(
                    'record_id' => $record_id,
                    'field_id' => $rows['field']['id'],
                    'object_id' => $rows['field']['object_id'],
                    'file_id' => $row,
                    'file_description' => $rows['des'][$key],
                    'status' => 1
                );
                $this->db->set('status', 1);
                $this->db->set('create_by', $this->session->userdata['user_id']);
                $this->db->set('create_time', 'NOW()', FALSE);
                $this->db->insert('file_mapper', $data);

            endforeach;
        endforeach;
    }

    function __updateFileMapper($record_id, $data_file) {
        foreach ($data_file as $rows):
            foreach ($rows['data'] as $key => $row):
                $data = array(
                    'record_id' => $record_id,
                    'field_id' => $rows['field']['id'],
                    'object_id' => $rows['field']['object_id'],
                    'file_id' => $row,
                    'file_description' => $rows['des'][$key],
                    'status' => 1
                );
                if ($rows['mapper'][$key]) {
                    //Update
                    $this->db->where('id', $rows['mapper'][$key]);
                    $this->db->update('file_mapper', $data);
                } else {
                    //Add new
                    $this->db->set('status', 1);
                    $this->db->set('create_by', $this->session->userdata['user_id']);
                    $this->db->set('create_time', 'NOW()', FALSE);
                    $this->db->insert('file_mapper', $data);
                }

            endforeach;
        endforeach;
    }

    function __maxOrd($object) {
        $this->db->select('MAX(ord) as max_ord');
        $this->db->select('id');
        $query = $this->db->get($object['name']);
        foreach ($query->result_array() as $row)
            return $row;
        $query->free_result();
    }

    function __saveFile($row) {
        //Save file
        $data = array(
            'file_name' => $row['file_name'],
            'file_path' => 'images/' . $row['file_name'],
            'file_size' => $row['file_size'], //Kb
            'mime_type' => $row['file_type'],
            'width' => $row['image_width'],
            'height' => $row['image_height'],
        );
        $this->db->set('create_by', $this->session->userdata['user_id']);
        $this->db->set('create_time', 'NOW()', FALSE);
        $this->db->insert('file', $data);
        $file_id = $this->db->insert_id();
        return $file_id;
    }

    function __updateFile($row, $file_id) {
        $data = array(
            'file_name' => $row['file_name'],
            'file_path' => 'images/' . $row['file_name'],
            'file_size' => $row['file_size'], //Kb
            'mime_type' => $row['file_type'],
            'width' => $row['image_width'],
            'height' => $row['image_height'],
        );
        $this->db->where('id', $file_id);
        $this->db->update('file', $data);
    }

    function __saveMapper($file_id, $record_id, $detail_key) {
        $mapper = array(
            'file_id' => $file_id,
            'record_id' => $record_id,
            'field_id' => $detail_key['id'],
            'object_id' => $detail_key['object_id'],
            'status' => 1
        );
        $this->db->set('create_by', $this->session->userdata['user_id']);
        $this->db->set('create_time', 'NOW()', FALSE);
        $this->db->insert('file_mapper', $mapper);
    }

    function __detailObjects($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get($this->object);
        if ($query->result_array()) {
            foreach ($query->result_array() as $result)
                return $result;
        } else {
            return false;
        }
        $query->free_result();
    }

    function __totalRecords($object_name) {
        $this->db->where('lang', $this->session->userdata('lang'));
        return $this->db->count_all_results($object_name);
    }

    function __getRecords($object_name, $limit = null, $offset = 0) {

        $this->db->select('*');
        $this->db->where('lang', $this->session->userdata('lang'));
        if ($limit) {
            $this->db->limit($limit, $offset);
        }
        if (isset($_POST['keyword']) && $_POST['keyword']) {
            $this->db->like('name', $_POST['keyword']);
        }
        $this->db->order_by('ord', 'ASC');
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get($object_name);
        return $query->result_array();
        $query->free_result();
    }

    function __getResurionRecords($object_name, $parent_id = 0, $str = ' ') {
        $data = array();
        $this->db->select('*');
        $this->db->where('lang', $this->session->userdata('lang'));
        $this->db->where('parent_id', $parent_id);
        $this->db->order_by('ord', 'ASC');
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get($object_name);
        foreach ($query->result_array() as $row):
            $row['name'] = $str . $row['name'];
            $data[] = $row;
            $rows = $this->__getResurionRecords($object_name, $row['id'], '&rdsh; ');
            foreach ($rows as $dt):
                $dt['name'] = $str . $dt['name'];
                $data[] = $dt;
            endforeach;
        endforeach;
        $query->free_result();
        return $data;
    }

    function __checkParentID($object_name, $id) {
        $this->db->select('id');
        $query = $this->db->get($object_name);
        if ($query->result_array()):
            return true;
        else:
            return false;
        endif;
    }

    function __updateRecords($object_name, $id, $status) {
        $update = array(
            'status' => $status
        );
        $this->db->where('id', $id);
        $this->db->update($object_name, $update);
    }

    function __deleteRecords($object, $record) {
        //Delete Record.
        $this->db->where('id', $record['id']);
        $this->db->delete($object['name']);
        //Delete Object mapper
        $this->db->where('record_id', $record['id']);
        $this->db->where('object_id', $object['id']);
        $this->db->delete('object_mapper');
    }

    function __updateOrdRecord($object_name, $id, $ord) {
        $update = array(
            'ord' => $ord
        );
        $this->db->where('id ', $id);
        $this->db->update($object_name, $update);
    }

    function __detailRecord($object_name, $record_id) {
        $this->db->select('*');
        $this->db->where('id', $record_id);
        $query = $this->db->get($object_name);
        foreach ($query->result_array() as $row)
            return $row;
        $query->free_result();
    }

    function __getLevelObject($object_name, $parent_id = 0, $str = ' ') {
        $data = array();
        $this->db->select('*');
        $this->db->where('lang', $this->session->userdata('lang'));
        $this->db->where('parent_id', $parent_id);
        $this->db->order_by('ord', 'ASC');
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get($object_name);
        foreach ($query->result_array() as $row):
            $row['name'] = $str . $row['name'];
            $data[] = $row;
            $rows = $this->__getLevelObject($object_name, $row['id'], '-- ');
            foreach ($rows as $dt):
                $dt['name'] = $str . $dt['name'];
                $data[] = $dt;
            endforeach;
        endforeach;
        $query->free_result();
        return $data;
    }

    function __ThumbField($field_id) {
        $this->db->select('*');
        $this->db->where('parent_id', $field_id);
        $query = $this->db->get($this->field);
        return $query->result_array();
        $query->free_result();
    }

    function __mapFile($record_id, $field_id) {
        $this->db->select('file.*');
        $this->db->select('file_mapper.id as mapper_id');
        $this->db->select('file_mapper.file_description as description');
        $this->db->from('file_mapper');
        $this->db->join('file', 'file.id = file_mapper.file_id');
        $this->db->where('file_mapper.record_id', $record_id);
        $this->db->where('file_mapper.field_id', $field_id);
        $result = $this->db->get();
        return $result->result_array();
        $result->free_result();
    }

    function __nameObject($object_id) {
        $this->db->select('name');
        $this->db->where('id', $object_id);
        $result = $this->db->get($this->object);
        foreach ($result->result_array() as $row)
            return $row['name'];
        $result->free_result();
    }

}
