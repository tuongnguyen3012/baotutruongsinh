<?php

/**
 * Description of setting
 * @author trungthuc
 * @date Jan 28, 2015
 */
class settings extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public $setting = 'setting';
	public $field = 'field';
	public $fields = array(
		'id' => array(
			'type' => 'INT',
			'label' => 'ID',
			'status' => 0,
			'primary_key' => TRUE,
			'constraint' => 11,
			'unsigned' => TRUE,
			'auto_increment' => TRUE
		),
		'parent_id' => array(
			'type' => 'INT',
			'status' => 0,
			'label' => 'Parent ID',
			'constraint' => 11
		),
		'name' => array(
			'type' => 'VARCHAR',
			'label' => 'Name',
			'status' => 1,
			'constraint' => 255
		),
		'ord' => array(
			'type' => 'INT',
			'label' => 'Thứ tự',
			'status' => 0,
			'constraint' => 11
		),
		'status' => array(
			'type' => 'TINYINT',
			'label' => 'Trạng thái',
			'status' => 1,
			'constraint' => 1,
			'default' => 0
		),
		'create_time' => array(
			'type' => 'TIMESTAMP',
			'status' => 1,
			'label' => 'Ngày tạo'
		),
		'create_by' => array(
			'type' => 'INT',
			'label' => 'Người tạo',
			'status' => 0,
			'constraint' => 11
		),
		'lang' => array(
			'type' => 'VARCHAR',
			'label' => 'Ngôn ngữ',
			'status' => 0,
			'constraint' => 2,
			'default' => 'vi'
		),
	);

	function __getSettings($limit = null, $offset = 0) {
		$this->db->select('*');
		if ($limit) {
			$this->db->limit($limit, $offset);
		}
		$this->db->order_by('ord', 'ASC');
		$this->db->order_by('id', 'ASC');
		$query = $this->db->get($this->setting);
		return $query->result_array();
		$query->free_result();
	}

	function __saveSetting() {
		//Check setting
		$data = array(
			'label' => $this->input->get_post('label'),
			'name' => $this->input->get_post('name'),
			'level' => $this->input->get_post('level'),
			'role' => json_encode($this->input->get_post('role')),
			'status' => $this->input->get_post('status')
		);
		$this->db->set('create_by', $this->session->userdata['user_id']);
		$this->db->set('create_time', 'NOW()', FALSE);
		$this->db->insert($this->setting, $data);
		return $this->db->insert_id();
	}

	function __editSetting() {
		$on['value'] = $_POST['on'];
		$title['value'] = $_POST['title'];
		$key['value'] = $_POST['key'];
		$des['value'] = $_POST['des'];
		$email['value'] = $_POST['email'];
		$usermail['value'] = $_POST['usermail'];
		$passmail['value'] = $_POST['passmail'];
		$hostmail['value'] = $_POST['hostmail'];
		$portmail['value'] = $_POST['portmail'];
		$offline['value'] = $_POST['offline'];

		$id = $this->__get_id_setting('on');
		$this->db->where('id', $id);
		$this->db->update($this->setting, $on);

		$id = $this->__get_id_setting('title');
		$this->db->where('id', $id);
		$this->db->update($this->setting, $title);

		$id = $this->__get_id_setting('key');
		$this->db->where('id', $id);
		$this->db->update($this->setting, $key);

		$id = $this->__get_id_setting('des');
		$this->db->where('id', $id);
		$this->db->update($this->setting, $des);

		$id = $this->__get_id_setting('email');
		$this->db->where('id', $id);
		$this->db->update($this->setting, $email);

		$id = $this->__get_id_setting('usermail');
		$this->db->where('id', $id);
		$this->db->update($this->setting, $usermail);

		$id = $this->__get_id_setting('passmail');
		$this->db->where('id', $id);
		$this->db->update($this->setting, $passmail);

		$id = $this->__get_id_setting('hostmail');
		$this->db->where('id', $id);
		$this->db->update($this->setting, $hostmail);

		$id = $this->__get_id_setting('portmail');
		$this->db->where('id', $id);
		$this->db->update($this->setting, $portmail);

		$id = $this->__get_id_setting('offline');
		$this->db->where('id', $id);
		$this->db->update($this->setting, $offline);
	}

	function __get_id_setting($name) {
		$this->db->select('id');
		$this->db->where('name', $name);
		$query = $this->db->get($this->setting);
		return $query->row()->id;
		$query->free_result();
	}

	function __get_extension($module) {
		$this->db->select('*');
		$this->db->where('name', $module);
		$query = $this->db->get('tb_extension');
		foreach ($query->result_array() as $row)
			return $row;
		$query->free_result();
	}

	function _save_logo() {
		$this->db->where('name', 'logo');
		$this->db->update('tb_extension', array(
			'img' => isset($_POST['img']) ? $_POST['img'] : NULL
		));
	}
	
	function _save_hotline() {
		$this->db->where('name', 'hotline');
		$this->db->update('tb_extension', array(
			'content' => isset($_POST['content']) ? $_POST['content'] : NULL
		));
	}
	
	function _save_email() {
		$this->db->where('name', 'email');
		$this->db->update('tb_extension', array(
			'content' => isset($_POST['content']) ? $_POST['content'] : NULL
		));
	}
	
	function _save_footer() {
		$this->db->where('name', 'footer');
		$this->db->update('tb_extension', array(
			'content' => isset($_POST['content']) ? $_POST['content'] : NULL
		));
	}
	
	function _save_contacts() {
		$this->db->where('name', 'contacts');
		$this->db->update('tb_extension', array(
			'content' => isset($_POST['content']) ? $_POST['content'] : NULL
		));
	}
	
	function _save_adv() {
		$this->db->where('name', 'adv');
		$this->db->update('tb_extension', array(
			'link' => isset($_POST['link']) ? $_POST['link'] : NULL,
			'img' => isset($_POST['img']) ? $_POST['img'] : NULL
		));
	}
	// Show time
	function _save_social()
	{
		$this->db->where('name', 'social');
		$query = $this->db->get('tb_extension');
		if(count($query->row_array())>0)
		{
			$this->db->where('name', 'social');
			$this->db->update('tb_extension', array(
				'content' => json_encode($this->input->post('content'))
			));
		}
		else
		{
			$this->db->insert('tb_extension', array(
				'name' => 'social',
				'content' => json_encode($this->input->post('content'))
			));
		}
	}
	// SOCIALS
	function _save_socials()
	{
		$this->db->where('name', 'socials');
		$query = $this->db->get('tb_extension');
		if(count($query->row_array())>0)
		{
			$this->db->where('name', 'socials');
			$this->db->update('tb_extension', array(
				'content' => json_encode($this->input->post('content'))
			));
		}
		else
		{
			$this->db->insert('tb_extension', array(
				'name' => 'socials',
				'content' => json_encode($this->input->post('content'))
			));
		}
	}
	function _save_maps()
	{
		$this->db->where('name', 'maps');
		$query = $this->db->get('tb_extension');
		if(count($query->row_array())>0)
		{
			$this->db->where('name', 'maps');
			$this->db->update('tb_extension', array(
				'content' => isset($_POST['content']) ? $_POST['content'] : NULL
			));
		}
		else
		{
			$this->db->insert('tb_extension', array(
				'name' => 'maps',
				'content' => isset($_POST['content']) ? $_POST['content'] : NULL
			));
		}
	}
	function _save_website()
	{
		$this->db->where('name', 'website');
		$query = $this->db->get('tb_extension');
		if(count($query->row_array())>0)
		{
			$this->db->where('name', 'website');
			$this->db->update('tb_extension', array(
				'content' => isset($_POST['content']) ? $_POST['content'] : NULL
			));
		}
		else
		{
			$this->db->insert('tb_extension', array(
				'name' => 'website',
				'content' => isset($_POST['content']) ? $_POST['content'] : NULL
			));
		}
	}
}
