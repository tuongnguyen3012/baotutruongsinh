<?php

/**
 * Description of setting
 * @author trungthuc
 * @date Jan 28, 2015
 */
class users extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public $user = 'user';

    function __totalUsers() {
        return $this->db->count_all_results($this->user);
    }

    function __getUsers($limit = null, $offset = 0) {
        $roleId = $this->session->userdata('role_id');
        $this->db->select('*');
        if ($limit) {
            $this->db->limit($limit, $offset);
        }
        if ($roleId >= 3) {
            $userId = $this->session->userdata('user_id');
            $this->db->where('id', $userId);
        }
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get($this->user);
        return $query->result_array();
        $query->free_result();
    }

    function __saveUsers() {
        //Check object
        $data = array(
            'role_id' => $this->input->get_post('role_id'),
            'fullname' => $this->input->get_post('fullname'),
            'username' => $this->input->get_post('username'),
            'password' => md5($this->input->get_post('password')),
            'mobile' => $this->input->get_post('mobile'),
            'email' => $this->input->get_post('email'),
            'address' => $this->input->get_post('address'),
            'status' => 1,
            'create_by' => $this->session->userdata['user_id'],
            'create_time' => date('Y-m-d H:i:s')
        );
        $this->db->insert($this->user, $data);
        return $this->db->insert_id();
    }

    function __detailUsers($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get($this->user);
        foreach ($query->result_array() as $result)
            return $result;
        $query->free_result();
    }

    function __editUsers() {
        $data = array(
            'fullname' => $this->input->get_post('fullname') ? $this->input->get_post('fullname') : 'noName',
            'mobile' => $this->input->get_post('mobile') ? $this->input->get_post('mobile') : '',
            'address' => $this->input->get_post('address') ? $this->input->get_post('address') : '',
            'avatar' => $this->input->get_post('avatar'),
            'role_id' => $this->input->get_post('role_id')
        );

        if ($this->input->get_post('newpassword') != '' && $this->input->get_post('newpassword') != null)
            $data['password'] = md5($this->input->get_post('newpassword'));

        $this->db->where('id', $this->input->get_post('ID'));
        $this->db->update($this->user, $data);
    }

    function __deleteUsers($id) {
        $userID = $this->session->userdata('user_id');
        $roleId = $this->session->userdata('role_id');
        $user = $this->__detailUsers($id);
        if ($userID != $user['user_id'] && $roleId = 1) {
            $this->db->where('id', $id);
            $this->db->delete($this->user);
        }
    }

    function __getRoles($params=[]) {
        $this->db->select('*');
        $this->db->where('status', 1);

        if(array_key_exists('id_other', $params)){
            $this->db->where_not_in('id', $params['id_other']);
        }

        $query = $this->db->get('tb_role');
        $listRole = array();
        foreach ($query->result() as $role) {
            $listRole[$role->id] = $role->name;
        }
        return $listRole;
    }

    function __htmlFile($field, $value = null) {

        $html = null;
        if ($value):
            $html .= form_hidden($field, $value);
            $html .= form_upload('single_file', '', 'class="clear single_file" rel="' . $field . '" style="display:none;"');
            $html .= '<span class="preView">';
            $html .= img(array(
                'src' => $value,
                'class' => 'thumbs'
            ));
            $html .= '</span>';
            $html .= '<a class="remove_single iconsweets-trashcan" name="' . $field . '" record="' . $value . '" object="1"></a>';
        else:
            $html .= form_upload('single_file', '', 'class="clear single_file" f_type="14" rel="' . $field . '"');
            $html .= '<span class="preView"></span>';
            $html .= '<a style="display:none;" class="remove_single iconsweets-trashcan" name="' . $field . '" record="0"></a>';
        endif;
        echo $html;
    }

}
