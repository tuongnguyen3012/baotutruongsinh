<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of img_lib
 * @author trungthuc
 * @date Feb 24, 2015
 */
class img extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function crop($width_thumb, $height_thumb, $src, $new_src) {

        $name = "./images/a.jpg";
        $myImage = imagecreatefromjpeg($name);

        $x = $width_thumb;
        $y = $height_thumb;
        $ratio_thumb = $x / $y;

        list($xx, $yy) = getimagesize($name);
        $ratio_original = $xx / $yy;

        if ($ratio_original >= $ratio_thumb) {
            $yo = $yy;
            $xo = ceil(($yo * $x) / $y);
            $xo_ini = ceil(($xx - $xo) / 2);
            $xy_ini = 0;
        } else {
            $xo = $xx;
            $yo = ceil(($xo * $y) / $x);
            $xy_ini = ceil(($yy - $yo) / 2);
            $xo_ini = 0;
        }
        $myImageZoom = imagecreatetruecolor($width_thumb, $height_thumb);
        imagecopyresampled($myImageZoom, $myImage, 0, 0, $xo_ini, $xy_ini, $x, $y, $xo, $yo);
        $fileName = "img" . $x . '_' . $y;
        imagejpeg($myImageZoom, "./images/" . $fileName . "_crop.jpg");
    }

    function resize($width_thumb, $height_thumb, $src, $new_src) {

        $name = "./images/a.jpg";
        $myImage = imagecreatefromjpeg($name);
        list($width_img, $height_img) = getimagesize($name);
        $myImageZoom = imagecreatetruecolor($width_thumb, $height_thumb);
        imagecopyresampled($myImageZoom, $myImage, 0, 0, 0, 0, $width_thumb, $height_thumb, $width_img, $height_img);
        $fileName = "img";
        imagejpeg($myImageZoom, "./images/" . $fileName . "_resize.jpg");

        exit();
    }

}
