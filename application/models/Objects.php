<?php

/**
 * Description of objects
 * @author trungthuc
 * @date Jan 28, 2015
 */
class objects extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public $object = 'object';
    public $field = 'field';
    public $fields = array(
        'id' => array(
            'type' => 'INT',
            'label' => 'ID',
            'status' => 0,
            'primary_key' => TRUE,
            'constraint' => 11,
            'unsigned' => TRUE,
            'auto_increment' => TRUE
        ),
        'parent_id' => array(
            'type' => 'INT',
            'status' => 0,
            'label' => 'Parent ID',
            'constraint' => 11
        ),
        'name' => array(
            'type' => 'VARCHAR',
            'label' => 'Name',
            'status' => 1,
            'constraint' => 255
        ),
        'ord' => array(
            'type' => 'INT',
            'label' => 'Thứ tự',
            'status' => 0,
            'constraint' => 11
        ),
        'status' => array(
            'type' => 'TINYINT',
            'label' => 'Trạng thái',
            'status' => 1,
            'constraint' => 1,
            'default' => 0
        ),
        'create_time' => array(
            'type' => 'TIMESTAMP',
            'status' => 1,
            'label' => 'Ngày tạo'
        ),
        'create_by' => array(
            'type' => 'INT',
            'label' => 'Người tạo',
            'status' => 0,
            'constraint' => 11
        ),
        'lang' => array(
            'type' => 'VARCHAR',
            'label' => 'Ngôn ngữ',
            'status' => 0,
            'constraint' => 2,
            'default' => 'vi'
        ),
    );
    public $array_seo = array(
        'meta_title' => array(
            'type' => 'VARCHAR',
            'label' => 'Meta Title',
            'status' => 1,
            'constraint' => 255
        ),
        'meta_description' => array(
            'type' => 'VARCHAR',
            'label' => 'Meta Description',
            'status' => 1,
            'constraint' => 255
        ),
        'meta_keyword' => array(
            'type' => 'VARCHAR',
            'label' => 'Meta Keyword',
            'status' => 1,
            'constraint' => 255
        )
    );

    function __totalObjects() {
        return $this->db->count_all_results($this->object);
    }

    function __getObjects($limit = null, $offset = 0) {
        $this->db->select('*');
        if ($limit) {
            $this->db->limit($limit, $offset);
        }
        $this->db->order_by('ord', 'ASC');
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get($this->object);
        return $query->result_array();
        $query->free_result();
    }

    function __detailObjects($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get($this->object);
        if ($query->result_array()) {
            foreach ($query->result_array() as $result)
                return $result;
        } else {
            return false;
        }
        $query->free_result();
    }

    function __deleteObjects($id) {
        $this->db->where('id', $id);
        $this->db->delete($this->object);
    }

    function __updateObjects($id, $status) {
        $update = array(
            'status' => $status
        );
        $this->db->where('id', $id);
        $this->db->update($this->object, $update);
    }

    function __saveObject() {//add-1-save data object to table object
        //Check object
        $data = array(
            'label' => $this->input->get_post('label'),
            'name' => $this->input->get_post('name'),
            'level' => $this->input->get_post('level'),
            'role' => json_encode($this->input->get_post('role')),
            'add_seo' => $this->input->get_post('add_seo'),
            'status' => $this->input->get_post('status')
        );
        $this->db->set('create_by', $this->session->userdata['user_id']);
        $this->db->set('create_time', 'NOW()', FALSE);
        $this->db->insert($this->object, $data);
        return $this->db->insert_id();
    }

    function __editObject($object_id) {
        $data = array(
            'label' => $this->input->get_post('label'),
            'name' => $this->input->get_post('name'),
            'level' => $this->input->get_post('level'),
            'role' => json_encode($this->input->get_post('role')),
            'status' => $this->input->get_post('status')
        );
        $this->db->where('id', $object_id);
        $this->db->update($this->object, $data);
    }

    function __createObject($object, $has_seo) {//add-2-create table object
        $this->load->dbforge();
        // change
        if($has_seo==1)
        {
            $this->fields = array_merge($this->fields, $this->array_seo);
        }
        // end change
        $this->dbforge->add_field($this->fields);
        $this->dbforge->add_key('id');
        $this->dbforge->create_table($object, true);
    }

    function __renameObject($old_object, $new_object) {
        $this->load->dbforge();
        $this->dbforge->rename_table($old_object, $new_object);
    }

    function __updateOrdObject($object_id, $ord) {
        $update = array(
            'ord' => $ord
        );
        $this->db->where('id', $object_id);
        $this->db->update($this->object, $update);
    }

    function __checkFieldImageObject($object_id) {
        $this->db->select('*');
        $this->db->where('object_id', $object_id);
        $this->db->where('type', 14);
        $query = $this->db->get($this->field);
        if ($query->result_array())
            return $query->result_array();
        else
            return false;
        $query->free_result();
    }

    function __checkFieldMultiImageObject($object_id) {
        $this->db->select('*');
        $this->db->where('object_id', $object_id);
        $this->db->where('type', 17);
        $query = $this->db->get($this->field);
        if ($query->result_array())
            return $query->result_array();
        else
            return false;
        $query->free_result();
    }

}
