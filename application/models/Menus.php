<?php

/**
 * Description of menu
 * @author trungthuc
 * @date Oct 1, 2016
 */
class menus extends CI_Model {

	function __construct() {

		parent::__construct();
	}

	function __menu($parentId = 0, $str = "", $position) {
		$this->db->select('*');
		$this->db->where('position', $position);
		$this->db->where('parent_id', $parentId);
		$query = $this->db->get('menu');
		$data = $query->result_array();
		$return = array();
		if ($data) {

			foreach ($data as $key => $row) {
				if ($row['parent_id']) {
					$row['name'] = $str . $row['name'];
				}
				$return[$row['id']] = $row;
				if ($this->__menu($row['id'], $str . "&DoubleLongRightArrow; ", $position)) {
					$dt = $this->__menu($row['id'], $str . "&DoubleLongRightArrow; ", $position);
					$return = $return + $dt;
				}
			}
			return $return;
		} else {
			return false;
		}
	}

	function __dataMenu($parentId = 0, $str = "", $position) {
		$this->db->select('*');
		$this->db->where('position', $position);
		$this->db->where('parent_id', $parentId);
		$query = $this->db->get('menu');
		$data = $query->result_array();
		$return = array(
			0 => '== Root =='
		);
		foreach ($data as $row) {
			if ($row['parent_id'])
				$return[$row['id']] = $str . $row['name'];
			else
				$return[$row['id']] = $row['name'];

			if ($this->__dataMenu($row['id'], $str . "&DoubleLongRightArrow; ", $position)) {
				$dt = $this->__dataMenu($row['id'], $str . "&DoubleLongRightArrow; ", $position);
				$return = $return + $dt;
			}
		}

		return $return;
	}

	function __getObject() {
		$arr = array(
			0 => 'Chọn đối tượng'
		);
		$this->db->select('*');
		$query = $this->db->get('object');
		foreach ($query->result_array() as $row) {
			$arr[$row['id']] = $row['label'];
		}
		$query->free_result();
		return $arr;
	}

	function __getRowObject($object_name) {
		$this->db->select(array('id', 'name'));
		$this->db->where('lang', $this->session->userdata('lang'));
		$this->db->order_by('ord', 'ASC');
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($object_name);
		return $query->result_array();
		$query->free_result();
	}

	function __saveMenu($position) {
		//Check object
		$data = array(
			'name' => $this->input->get_post('name'),
			'parent_id' => $this->input->get_post('parent_id'),
			'position' => $position,
			'type' => $this->input->get_post('type'),
			'status' => 1
		);

		if (isset($_POST['alias']) && $_POST['alias'])
			$data['alias'] = $this->input->get_post('alias');

		if (isset($_POST['object_id']) && $_POST['object_id'])
			$data['object_id'] = $this->input->get_post('object_id');

		if (isset($_POST['object_name']) && $_POST['object_name'])
			$data['object_name'] = $this->input->get_post('object_name');

		if (isset($_POST['row_id']) && $_POST['row_id'])
			$data['row_id'] = $this->input->get_post('row_id');

		$this->db->set('create_by', $this->session->userdata['user_id']);
		$this->db->set('create_time', 'NOW()', FALSE);
		$this->db->insert('menu', $data);
		return $this->db->insert_id();
	}

}
