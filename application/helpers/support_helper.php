	<?php if (!defined('BASEPATH'))
	    exit('No direct script access allowed');
	    
	function curPage() {
	 return sprintf(
	    "%s://%s%s",
	    isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
	    $_SERVER['SERVER_NAME'],
	    $_SERVER['REQUEST_URI']
	  );
	}

	function youtube($string){
		if(strpos($string, 'youtube.com') != FALSE){
			if(strpos($string,'embed') != FALSE){
				$string = strstr($string, 'embed/');
				$string = substr($string, 6, 11);
			}else{
			    $string = strstr($string, 'watch?v=');
			    $string = substr($string, 8, 11);
			}
		}if(strpos($string, 'youtu.be') != FALSE){
				$string = strstr($string, 'youtu.be/');
				$string = substr($string, 9, 11);				
		}

		return $string;
	}

	function getRealIPAddress(){  
	    if(!empty($_SERVER['HTTP_CLIENT_IP'])){
	        //check ip from share internet
	        $ip = $_SERVER['HTTP_CLIENT_IP'];
	    }else if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
	        //to check ip is pass from proxy
	        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	    }else{
	        $ip = $_SERVER['REMOTE_ADDR'];
	    }
	    return $ip;
	}
	function convert_datetime($date)
	{
		// 01/10/2015 - 04:59
		return gmdate('d/m/Y - h:i', strtotime($date));
	}